<?php
	session_start();
/*
 * Страница добавления/редактирования компании
 * © Эрис
*/
	include "localization.php";
	include "connection.php";
	require_once "class/head.class.php";
	$head = new HeadPage(); //класс HEAD страницы

	if (isset($_SESSION['user'])){
		$id_user = $_SESSION['user'];
	}

	$table = $mysqli->query("SELECT * FROM admin_role WHERE id_admin = ".$id_user."");

	while($val = $table->fetch_assoc()){
		$id_role = $val['id_role'];
	}

	if (!isset($_SESSION['user']) || $id_role != 1)
	{
	  header('Location: /login.php');
	  exit;
	}

?>
<!DOCTYPE>
<html>
<head>
	<?php echo $head->getHead(LANG_CREATE_COMPANY); ?>
</head>
<body>
<?php include "headpanel.php"; ?>
<?php

	$edit_com = intval($_GET['editcom']);

	$res = $mysqli->query("SELECT * FROM company", MYSQLI_USE_RESULT);
	while ($value = $res->fetch_assoc())
	{

		if ($value['id_company'] == $edit_com)
		{
			$fname = $value['name'];
			$faddr = $value['adress'];
			$fdescript = $value['description'];
		}

	}

?>
	<section class="mbr-section form1 cid-r3l5xi9p39 mbr-parallax-background" id="form1-10">
    <div class="container">
		<div class="row justify-content-center">
			<div class="title col-12 col-lg-8">
				<br>
				<h2 class="mbr-section-title align-center mbr-fonts-style display-2">
				<?php echo $edit_com ? LANG_CHANGE_COMPANY : LANG_ADD_COMPANY; ?>
				</h2>
			</div>
		</div>
		<div class="mbr-block-title"></div>
		<form class="mbr-form company-form" id="company_form" action="company.php" method="post">
			<input type="hidden" name="id_admin" value="<?php echo $_SESSION['user']; ?>" id="сadm">
			<div class="row row-sm-offset">
				<div class="col-md-4 multi-horizontal" data-for="name">
					<div class="form-group">
						<label class="form-control-label mbr-fonts-style display-7"><?=LANG_NAME?></label>
						<input type="text" class="form-control" name="name" maxlength="150" id="cname" value="<?php echo $fname; ?>">
					</div>
				</div>
			</div>
			<div class="row row-sm-offset">
				<div class="col-md-4 multi-horizontal" data-for="address">
					<div class="form-group">
						<label class="form-control-label mbr-fonts-style display-7"><?=LANG_ADDR?></label>
						<input type="text" class="form-control" name="address"  id="caddr"  value="<?php echo $faddr; ?>">
					</div>
				</div>
			</div>
			<div class="form-group" data-for="message">
				<label class="form-control-label mbr-fonts-style display-7"><?=LANG_DESCRIPTION?></label>
				<textarea type="text" class="form-control" name="description" rows="7" maxlength="250" id="cdesc" ><?php echo $fdescript; ?></textarea>
			</div>
			<span class="input-group-btn">
				<button type="submit" class="btn btn-primary btn-form display-4"><?php echo $edit_com ? LANG_SAVE_CHANGE : LANG_CREATE; ?></button>
				<?php if($edit_com): ?><button type="button" id="button-delete" class="btn btn-md btn-secondary btn-form display-4" data-mess="<?=LANG_DEL_COMPANY_CONF?>"><?=LANG_DELETE?></button><?php endif ?>
				<button type="button" name="cancel" id="cancel_btn_comp" class="btn btn-warning btn-form display-4"><?=LANG_CANCEL?></button>
			</span>
			<input type="hidden" name="id" id="cid" value="<?php echo $edit_com; ?>">
			<?php if($edit_com): ?>
			<input type="hidden" name="key" id="ckey" value="199">
			<?php else: ?>
			<input type="hidden" name="key" id="ckey" value="1532">
			<?php endif ?>
		</form>
	</div>
	</section>
	<?php include "footer.php";?>
	<?php include "scripts.php"; ?>
	<script src="js/post_new_company.js"></script>
</body>
</html>
