<?php
	session_start();
/*
 * Формирвоание таблицы данных устройств
 * © Эрис
*/
	if (!isset($_SESSION['user'])) {
		header('Location: /login.php');
		exit;
	}
	
	include "localization.php";
	
	//$id_place = intval($_POST['id']);
	$id_place = $_POST['arr'];
	
	echo '
	    <div class="table-wrapper">
        <div class="container">
          <div class="row search">
            <div class="col-md-6"></div>
            <div class="col-md-6"></div>
          </div>
        </div>
        <div class="container scroll">
		<table class="table isSearch table-panel" cellspacing="0">
            <thead>
              <tr class="table-heads ">
				<th class="head-item mbr-fonts-style display-7 td-1">'.LANG_DESCRIPTION.'</th>
				<th class="head-item mbr-fonts-style display-7 td-2">'.LANG_FACTORY_NUMBER.'</th>
				<th class="head-item mbr-fonts-style display-7 td-3">'.LANG_TIME.'</th>
				<th class="head-item mbr-fonts-style display-7 td-4">'.LANG_CHANEL.' 1</th>
				<th class="head-item mbr-fonts-style display-7 td-5">'.LANG_CHANEL.' 2</th>
				<th class="head-item mbr-fonts-style display-7 td-6">'.LANG_CHANEL.' 3</th>
				<th class="head-item mbr-fonts-style display-7 td-7">'.LANG_CHANEL.' 4</th>
				<th class="head-item mbr-fonts-style display-7 td-8">'.LANG_CHARGE.'</th>
				<th class="head-item mbr-fonts-style display-7 td-9">'.LANG_STATUS.'</th>
				<th class="head-item mbr-fonts-style display-7 td-10">'.LANG_LOCATION.'</th>
			  </tr>
            </thead>
            <tbody>

	<div class="container">';
	
	include "connection.php";
	
	
	if(is_array($id_place)){
		//если массив
		$arr_place = implode(",",$id_place);
	}else{
		//если не является массивом
		$arr_place = $id_place;
	}
	
	//$id_place = 16;
						   
	$res_devices = $mysqli->query("SELECT d.*, ev1.*
								   FROM devices d
								   INNER JOIN eventlog ev1 ON d.id_device = ev1.id_device
								   AND ev1.id_place IN(".$arr_place.") GROUP BY ev1.id_device");

	$arr_device = array();
	
	while ($val_dev = $res_devices->fetch_assoc()){
		
		$arr_device[] = $val_dev['id_device'];
		
	}
	
	$arr_dev_str = implode(",",$arr_device);
	
	if(is_array($arr_device)){
	
		foreach($arr_device as $key=>$val){
			
			$res = $mysqli->query("SELECT d.*, DATE_FORMAT(e.date, '%d.%m.%y %H:%i:%s') as date, e.id_event, e.id_device, e.id_place, e.gps, e.channel1, e.channel2, e.channel3, e.channel4, e.field1, e.state
				FROM devices d INNER JOIN eventlog e ON e.id_device = d.id_device AND e.id_device = ".$val." AND e.gps != 'null' ORDER BY e.id_event DESC LIMIT 1", MYSQLI_USE_RESULT);
									
			while ($value = $res->fetch_assoc()) {

				echo '<tr> <td class="body-item mbr-fonts-style display-7 td-1" data-label="'.LANG_DESCRIPTION.'"><a href="/more.php?id='.$value['id_device'].'">'
				.$value['description'].'</a></td><td class="body-item mbr-fonts-style display-7 td-2" data-label="'.LANG_FACTORY_NUMBER.'">'.$value['znumber'].'</td>';
				echo '<td class="body-item mbr-fonts-style display-7 td-3" data-label="'.LANG_TIME.'">'.$value['date'].'</td><td class="body-item mbr-fonts-style display-7 td-4" data-label="'.LANG_CHANEL.' 1">'.$value['channel1'].'</td>';
				echo '<td class="body-item mbr-fonts-style display-7 td-5" data-label="'.LANG_CHANEL.' 2">'.$value['channel2'].'</td><td class="body-item mbr-fonts-style display-7 td-6" data-label="'.LANG_CHANEL.' 3">'.$value['channel3'].'</td>';
				echo '<td class="body-item mbr-fonts-style display-7 td-7" data-label="'.LANG_CHANEL.' 4">'.$value['channel4'].'</td><td class="body-item mbr-fonts-style display-7 td-8" data-label="'.LANG_CHARGE.'">'.$value['field1'].'</td>';
				echo '<td class="body-item mbr-fonts-style display-7 td-9" data-label="'.LANG_STATUS.'">'.$value['state'].'</td><td class="body-item mbr-fonts-style display-7 td-10" data-label="'.LANG_LOCATION.'">'.$value['gps'].'</td>';
				echo '</tr>';

			}
		}
	
	}else{
		
		$res = $mysqli->query("SELECT d.*, e.*
								FROM devices d INNER JOIN eventlog e ON e.id_device = d.id_device AND e.id_device = ".$id_place." ORDER BY e.id_event DESC LIMIT 1", MYSQLI_USE_RESULT);
								
		while ($value = $res->fetch_assoc()) {

			echo '<tr> <td class="body-item mbr-fonts-style display-7" data-label="'.LANG_DESCRIPTION.'"><a href="/more.php?id='.$value['id_device'].'">'
			.$value['description'].'</a></td><td class="body-item mbr-fonts-style display-7" data-label="'.LANG_FACTORY_NUMBER.'">'.$value['znumber'].'</td>';
			echo '<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_TIME.'">'.$value['date'].'</td><td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANEL.' 1">'.$value['channel1'].'</td>';
			echo '<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANEL.' 2">'.$value['channel2'].'</td><td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANEL.' 3">'.$value['channel3'].'</td>';
			echo '<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANEL.' 4">'.$value['channel4'].'</td><td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHARGE.'">'.$value['field1'].'</td>';
			echo '<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_STATUS.'">'.$value['state'].'</td><td class="body-item mbr-fonts-style display-7" data-label="'.LANG_LOCATION.'">'.$value['gps'].'</td>';
			echo '</tr>';

		}
		
	}

//file_put_contents($file, $current);	

echo '</table>
        </div>
        </div>
        <div class="container table-info-container">
          <div class="row info">
            <div class="col-md-6">

            </div>
            <div class="col-md-6"></div>
          </div>
        </div>
      </div>
    ';

	?>

 

