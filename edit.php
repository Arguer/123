<?php
session_start();
/*
 * Редактирование подразделения
 * © Эрис
*/
	
	// Подключение к базе данных
	include "connection.php";
	// Подключение языков
	include "localization.php";
	
	$key  = $_POST['key'];
	$desc = $_POST['description'];
	$id = intval($_POST['id']);
	$redir = intval($_GET['redir']);
	$result = array();
	
	if ($key == '1dev')
	{
		if ($mysqli->query('UPDATE devices SET description ="'.$desc.'" WHERE id_device = '.$id) == TRUE){
			$result['status'] = 'success';
			$result['info'] = LANG_SUCCESS_CHANGED;
		}else{
			$result['status'] = 'error';
			$result['info'] = LANG_ERR_CHANGE;
		}
		
		echo json_encode($result);
		
	}
	else if ($key == '193')
	{
		$name = trim($_POST['name']);
		$login =  trim($_POST['login']);
		$password = trim($_POST['password']);
		$password2 = trim($_POST['password2']);
		
		$login_ln = mb_strlen($login,'UTF-8');
		$password_ln = mb_strlen($password,'UTF-8');
		
		$pass_err = 0;
		
		if($password != $password2){
			$pass_err = 1;
		}
		
		if(!$name){
			$result['status'] = 'error';
			$result['name'] = 'name';
			$result['info'] = LANG_ERR_NAME_OBJ;
		}elseif(!$login){
			$result['status'] = 'error';
			$result['name'] = 'login';
			$result['info'] = LANG_ERR_LOGIN;
		}elseif($login_ln < 3){
			$result['status'] = 'error';
			$result['name'] = 'login';
			$result['info'] = LANG_ERR_LOGIN_MIN_LN;
		}elseif($login_ln > 50){
			$result['status'] = 'error';
			$result['name'] = 'login';
			$result['info'] = LANG_ERR_LOGIN_LN;
		}elseif(!$password){
			$result['status'] = 'error';
			$result['name'] = 'password';
			$result['info'] = LANG_ERR_PASS;
		}elseif($password_ln < 3){
			$result['status'] = 'error';
			$result['name'] = 'password';
			$result['info'] = LANG_ERR_PASS_MIN_LN;
		}elseif($password_ln > 50){
			$result['status'] = 'error';
			$result['name'] = 'password';
			$result['info'] = LANG_ERR_PASS_LN;
		}elseif(!$password2){
			$result['status'] = 'error';
			$result['name'] = 'password2';
			$result['info'] = LANG_ERR_PASS_CONFIRM;
		}elseif($pass_err == 1){
			$result['status'] = 'error';
			$result['name'] = 'password';
			$result['name2'] = 'password2';
			$result['info'] = LANG_ERR_PASS_DUBLICAT;
		}else{
			
			$curr_data = $mysqli->query("SELECT * FROM places WHERE id_place='".$id."'");
			
			while($val = $curr_data->fetch_assoc()){
				$curr_name = $val['name'];
				$curr_login = $val['login'];
			}
			
			if($curr_name != $name || $curr_login != $login){ //если текущее наименование не равно введенному наименованию и текущий логин не равен введенному логину
				$unique_name_result  = $mysqli->query("SELECT * FROM places WHERE name='".$name."'");
				$unique_login_result = $mysqli->query("SELECT * FROM places WHERE login='".$login."'");
				if($curr_name != $name && $curr_login != $login){
					if ($unique_name_result->num_rows == 0){ // если введенного наименвоания нет в базе
						if ($unique_login_result->num_rows == 0){ // если введеного логина нет в базе
							if ($mysqli->query('UPDATE places SET name = "'.$name.'", description ="'.$desc.'", login = "'.$login.'", password = "'.$password.'" 
								WHERE id_place = '.$id) == TRUE){
								$result['status'] = 'success';
								$result['info'] = LANG_SUCCESS_CHANGED;
							}else{
								$result['status'] = 'error';
								$result['info'] = LANG_ERR_CHANGE;
							}
						}else{
							$result['status'] = 'error';
							$result['name'] = 'login';
							$result['info'] = LANG_ERR_LOGIN_DUBLICAT;
						}
					}else{
						$result['status'] = 'error';
						$result['name'] = 'name';
						$result['info'] = LANG_ERR_NAME_DUBLICAT;
					}
				}elseif($curr_name != $name && $curr_login == $login){
					if ($unique_name_result->num_rows == 0){ // если введенного наименвоания нет в базе
						if ($mysqli->query('UPDATE places SET name = "'.$name.'", description ="'.$desc.'", login = "'.$login.'", password = "'.$password.'" 
							WHERE id_place = '.$id) == TRUE){
							$result['status'] = 'success';
							$result['info'] = LANG_SUCCESS_CHANGED;
						}else{
							$result['status'] = 'error';
							$result['info'] = LANG_ERR_CHANGE;
						}
					}else{
						$result['status'] = 'error';
						$result['name'] = 'name';
						$result['info'] = LANG_ERR_NAME_DUBLICAT;
					}
				}elseif($curr_name == $name && $curr_login != $login){
					if ($unique_login_result->num_rows == 0){ // если введеного логина нет в базе
						if ($mysqli->query('UPDATE places SET name = "'.$name.'", description ="'.$desc.'", login = "'.$login.'", password = "'.$password.'" 
							WHERE id_place = '.$id) == TRUE){
							$result['status'] = 'success';
							$result['info'] = LANG_SUCCESS_CHANGED;
						}else{
							$result['status'] = 'error';
							$result['info'] = LANG_ERR_CHANGE;
						}
					}else{
						$result['status'] = 'error';
						$result['name'] = 'login';
						$result['info'] = LANG_ERR_LOGIN_DUBLICAT;
					}
				}
			}else{
				if ($mysqli->query('UPDATE places SET name = "'.$name.'", description ="'.$desc.'", login = "'.$login.'", password = "'.$password.'" 
						    WHERE id_place = '.$id) == TRUE){
					$result['status'] = 'success';
					$result['info'] = LANG_SUCCESS_CHANGED;
				}else{
					$result['status'] = 'error';
					$result['info'] = LANG_ERR_CHANGE;
				}
			}
			
		}
		
		// преобразуем в JSON-формат
		echo json_encode($result);
		
	}
	
?>