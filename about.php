<?php
session_start();
/*
 * Страница описания ресурса
 * © Эрис
*/
include "localization.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы
?>
<!DOCTYPE html>
<html>
	<head>
	<?php echo $head->getHead(LANG_TITLE_SITE); ?>
	</head>
	<body>
	<?php include "headpanel.php"; ?>
	<section class="cid-qTkA127IK8 mbr-fullscreen mbr-parallax-background page-about" id="header2-1">
		<div class="mbr-fullscreen-back"></div>
		<div class="container align-center section-about">
			<h1 class="title-about mbr-white"><?=LANG_TITLE_HOME?></h1>
			<div class="row">
				<div class="mbr-white text-about">
					<p><?=LANG_ABOUT_1?> <a href="http://eriskip.com/" target="_blank">"<?=LANG_ERIS?>"</a>.</p>
					<p><?=LANG_ABOUT_2?>:</p>
					<ul>
						<li><?=LANG_ABOUT_3?>;</li>
						<li><?=LANG_ABOUT_4?>;</li>
						<li><?=LANG_ABOUT_5?>;</li>
						<li><?=LANG_ABOUT_6?>;</li>
						<li><?=LANG_ABOUT_7?>;</li>
						<li><?=LANG_ABOUT_8?>;</li>
						<li><?=LANG_ABOUT_9?>.</li>
					</ul>
					<p><?=LANG_ABOUT_10?>.</p>
					<p><?=LANG_ABOUT_11?>:</p>
					<ul>
						<li><?=LANG_ABOUT_12?>;</li>
						<li><?=LANG_ABOUT_13?>;</li>
						<li><?=LANG_ABOUT_14?>;</li>
						<li><?=LANG_ABOUT_15?>.</li>
					</ul>
					<p><?=LANG_ABOUT_16?>.</p>
					<p><?=LANG_ABOUT_17?>:</p>
					<ul>
						<li><?=LANG_ABOUT_18?>;</li>
						<li><?=LANG_ABOUT_19?>;</li>
						<li><?=LANG_ABOUT_20?>;</li>
						<li><?=LANG_ABOUT_21?>.</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<?php include "footer.php"; ?>
	<?php include "scripts.php"; ?>
	</body>
</html>
