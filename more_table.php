<?php
session_start();
/*
 * Формирование таблицы данных устройства
 * © Эрис
*/
if (!isset($_SESSION['user'])) {
	header('Location: /login.php');
	exit;
}

include "localization.php";
include "connection.php";

//$id_dev = $_POST['id_dev'];

//echo "id=".$id_dev;

	echo '<table class="table isSearch table-panel" cellspacing="0">
            <thead>
              <tr class="table-heads ">    
				<th class="head-item mbr-fonts-style display-7">'.LANG_DESCRIPTION.'</th>
				<th class="head-item mbr-fonts-style display-7">'.LANG_FACTORY_NUMBER.'</th>
				<th class="head-item mbr-fonts-style display-7">'.LANG_TIME.'</th>
				<th class="head-item mbr-fonts-style display-7">'.LANG_CHANEL.' 1</th>
				<th class="head-item mbr-fonts-style display-7">'.LANG_CHANEL.' 2</th>
				<th class="head-item mbr-fonts-style display-7">'.LANG_CHANEL.' 3</th>
				<th class="head-item mbr-fonts-style display-7">'.LANG_CHANEL.' 4</th>
				<th class="head-item mbr-fonts-style display-7">'.LANG_CHARGE.'</th>
				<th class="head-item mbr-fonts-style display-7">'.LANG_STATUS.'</th>
			  </tr>
            </thead>
            <tbody>';
	
	$id_place = 0;
	
	// функция получения концентрации
	function conc_select($valcan)
	{
		$start = strpos($valcan, "<b>");	//Ищем тег 
		$end = strpos($valcan, "</");	//Ищем начало следующего тега
		$res = substr($valcan, $start+3, $end - $start - 3 );	//Выводим числовую часть
		$res = str_replace(",",".", $res);
		return $res;
	}
	
	// функция получения подписи
	function title_select($valcan)
	{
        $result_f = substr(strrchr($valcan, ">"), 1);
		$result_f = str_replace("\00","", $result_f); 
		return $result_f;	    
	}
	
	$data_dev = urldecode($_POST['data_dev']);
	parse_str($data_dev, $arr_dev);
	
	if(!empty($arr_dev['idd']))
		$id_dev = $arr_dev['idd'];
	
	// время от
	if(!empty($arr_dev['from'])){
		$from = $arr_dev['from'];
		$_SESSION['filter']['from'] = $from;
		if(isset($_SESSION['filter']['from'])){
			$from = $_SESSION['filter']['from'];
		}
		$dev_query .= " AND date >= '".$from."'";
	}else{
		unset($_SESSION['filter']['from']);
	}
	
	// время до
	if(!empty($arr_dev['to'])){
		$to = $arr_dev['to'];
		$_SESSION['filter']['to'] = $to;
		if(isset($_SESSION['filter']['to'])){
			$to = $_SESSION['filter']['to'];
		}
		$dev_query .= " AND date <= '".$to."'";
	}else{
		unset($_SESSION['filter']['to']);
	}
	
	// заряд от
	if(!empty($arr_dev['battery_from'])){
		$battery_from = $arr_dev['battery_from'];
		$_SESSION['filter']['battery_from'] = $battery_from;
		if(isset($_SESSION['filter']['battery_from'])){
			$battery_from = $_SESSION['filter']['battery_from'];
		}
		$dev_query .= " AND field1 >= '".$battery_from."'";
	}else{
		unset($_SESSION['filter']['battery_from']);
	}
	
	// заряд до
	if(!empty($arr_dev['battery_to'])){
		$battery_to = $arr_dev['battery_to'];
		$_SESSION['filter']['battery_to'] = $battery_to;
		if(isset($_SESSION['filter']['battery_to'])){
			$battery_to = $_SESSION['filter']['battery_to'];
		}
		$dev_query .= " AND field1 <= '".$battery_to."'";
	}else{
		unset($_SESSION['filter']['battery_to']);
	}
	
	// статус
	if(!empty($arr_dev['status'])){
		$status = $arr_dev['status'];
		$_SESSION['filter']['status'] = $status;
		if(isset($_SESSION['filter']['status'])){
			$status = $_SESSION['filter']['status'];
		}
		$dev_query .= " AND state LIKE '%".$status."%'";
	}else{
		unset($_SESSION['filter']['status']);
	}
	
	
	$page = intval($_GET['page']);
	
	if(!empty($arr_dev['pagen']))
		$page = $arr_dev['pagen'];
	
	$res_table = $mysqli->query("SELECT * FROM eventlog WHERE id_device = ".$id_dev." AND gps != 'null' ".$dev_query." ORDER BY id_event DESC");
	$size_of_table = $res_table->num_rows;
	
	$res_title = $mysqli->query("SELECT * FROM eventlog WHERE id_device = ".$id_dev." ORDER BY id_event DESC");
	//$size_of_table = $res_title->num_rows;
	$title_resul_sql = $res_title->fetch_assoc();

	$title_th = "['".LANG_TIME."','".title_select($title_resul_sql['channel1'])."','".title_select($title_resul_sql['channel2'])."','".title_select($title_resul_sql['channel3'])."','".title_select($title_resul_sql['channel4'])
	."']\n" ;
	
	$res = $mysqli->query("SELECT d.*, DATE_FORMAT(e.date, '%d.%m.%y %H:%i:%s') as date, e.id_event, e.id_device, e.id_place, e.gps, e.channel1, e.channel2, e.channel3, e.channel4, e.field1, e.state 
							FROM eventlog e INNER JOIN devices d ON e.id_device = d.id_device WHERE d.id_device=".$id_dev." AND e.gps != 'null' ".$dev_query." ORDER BY e.id_event DESC LIMIT ".($page * 30).", 30 ", MYSQLI_USE_RESULT);
	$s = 0; $chart = "";
	$write_gps = 0;
	while ($value = $res->fetch_assoc()) {
		$strings[$s] .= ",['".substr($value['date'], 11)."',".conc_select($value['channel1']).",".conc_select($value['channel2']).",".conc_select($value['channel3']).",".conc_select($value['channel4'])."]\n";
		echo ' <td class="body-item mbr-fonts-style display-7" data-label="'.LANG_DESCRIPTION.'">'.$value['description'].'</td><td class="body-item mbr-fonts-style display-7" data-label="'.LANG_FACTORY_NUMBER.'">'.$value['znumber'].'</td> ';
		echo ' <td class="body-item mbr-fonts-style display-7" data-label="'.LANG_TIME.'">'.$value['date'].'</td><td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANEL.' 1">'.$value['channel1'].'</td>       ';
		echo ' <td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANEL.' 2">'.$value['channel2'].'</td><td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANEL.' 3">'.$value['channel3'].'</td>   ';
		echo ' <td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANEL.' 4">'.$value['channel4'].'</td><td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHARGE.'">'.$value['field1'].'</td>      ';
		echo ' <td class="body-item mbr-fonts-style display-7" data-label="'.LANG_STATUS.'">'.$value['state'].'</td>';
		echo ' </tr>';
		if ($write_gps == 0)
		{
		  $last_gps = $value['gps']; $desc = $value['description'];
		  $id_place = $value['id_place'];
		  $write_gps = 1;			  
		}
		$s++;
	}
	
	for ($x = $s; $x >= 0; $x--){
		$chart .= $strings[$x];
	}
	
	//$page = $_POST['data_page'];
	
    echo '</table>';
	
	
	/* ** Навигация ** */
	
	echo '<div class="navigation">';
	
	echo '<span class="nav_count">'.LANG_COUNT_RECORDS.': '.$size_of_table.'</span>';
	
	$count_pages = ($size_of_table / 30);
	echo LANG_PAGES.": ";
	if ($page != 0)
	{
		echo '<a href="/more.php?id='.$id_dev.'&page='.($page - 1).'#map_nav_btn" data-page="'.($page - 1).'"><< </a>';
		echo '<a href="/more.php?id='.$id_dev.'&page=0#map_nav_btn" data-page="0">1</a>';
	}
	else
		echo '<b>1</b>';
	
	if ($count_pages < 4)
	{
		for($z = 1; $z < 4; $z++)
		{
			if ($z <= $count_pages)
			{
				if ($z == $page) echo ', <b>'.($z + 1).'</b> ';
				else
				echo '<a href="/more.php?id='.$id_dev.'&page='.$z.'#map_nav_btn" data-page="'.$z.'">, '.($z + 1).'</a>';
			}
		}
	}
	else
	{
		if ($page > 4) echo '<a href="/more.php?id='.$id_dev.'&page=0#map_nav_btn" data-page="0">... </a>';
		for($z = -3; $z < 4; $z++)
		{
			if ((($z + $page) < $count_pages) && ($z + $page > 0))
			{
				if ($z == 0) echo ',<b> '.($z + $page + 1).'</b>';
				else
				echo '<a href="/more.php?id='.$id_dev.'&page='.($z + $page).'#map_nav_btn" data-page="'.($z + $page).'">, '.($z + $page + 1).'</a>';
			}
		}
		if ($count_pages - 4 > $page) echo '<a href="/more.php?id='.$id_dev.'&page='.(ceil($count_pages) - 1).'#map_nav_btn" data-page="'.(ceil($count_pages) - 1).'"> ... '.ceil($count_pages).'</a>';
		if ($page < ($count_pages - 1)) echo '<a href="/more.php?id='.$id_dev.'&page='.($page + 1).'#map_nav_btn" data-page="'.($page + 1).'"> >></a> ';
	}
	
	echo '</div>';
	
	echo '<div class="pagination"></div>';
	
	echo '<div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-7" href="/panel.php?id='.$id_place.'">&nbsp&nbsp&nbsp&nbsp'.LANG_BACK.'&nbsp&nbsp&nbsp&nbsp&nbsp<br></a></div>';
	
?>