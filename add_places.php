<?php
session_start();
/*
 * Добавление подразделения
 * © Эрис
*/
	
	include "localization.php";
	include "connection.php";
	
	$name = trim($_POST['name']);
	$login =  trim($_POST['login']);
	$password = trim($_POST['password']);
	$password2 = trim($_POST['password2']);
	$key = $_POST['key'];
	$desc = $_POST['description'];
	$id = intval($_POST['id']);
	$id_admin = $_POST['id_admin'];
	$result = array();
	
	$login_ln = mb_strlen($login,'UTF-8'); //длина логина
	$password_ln = mb_strlen($password,'UTF-8'); //длина пароля
	
	$pass_err = 0;
	
	// поверка на совпадение паролей
	if($password != $password2){
		$pass_err = 1;
	}
	
	if ($key == "3621")
	{
		
		if(!$name){
			$result['status'] = 'error';
			$result['name'] = 'name';
			$result['info'] = LANG_ERR_NAME_OBJ;
		}elseif(!$login){
			$result['status'] = 'error';
			$result['name'] = 'login';
			$result['info'] = LANG_ERR_LOGIN;
		}elseif($login_ln < 3){
			$result['status'] = 'error';
			$result['name'] = 'login';
			$result['info'] = LANG_ERR_LOGIN_MIN_LN;
		}elseif($login_ln > 50){
			$result['status'] = 'error';
			$result['name'] = 'login';
			$result['info'] = LANG_ERR_LOGIN_LN;
		}elseif(!$password){
			$result['status'] = 'error';
			$result['name'] = 'password';
			$result['info'] = LANG_ERR_PASS;
		}elseif($password_ln < 3){
			$result['status'] = 'error';
			$result['name'] = 'password';
			$result['info'] = LANG_ERR_PASS_MIN_LN;
		}elseif($password_ln > 50){
			$result['status'] = 'error';
			$result['name'] = 'password';
			$result['info'] = LANG_ERR_PASS_LN;
		}elseif(!$password2){
			$result['status'] = 'error';
			$result['name'] = 'password2';
			$result['info'] = LANG_ERR_PASS_CONFIRM;
		}elseif($pass_err == 1){
			$result['status'] = 'error';
			$result['name'] = 'password';
			$result['name2'] = 'password2';
			$result['info'] = LANG_ERR_PASS_DUBLICAT;
		}else{
		
			$unique_login_result = $mysqli->query("SELECT * FROM places WHERE login='".$login."'");
			$unique_name_result  = $mysqli->query("SELECT * FROM places WHERE name='".$name."'");
			
			if ($unique_login_result->num_rows == 0)	//если таких записей еще нет
			{
				if ($unique_name_result->num_rows == 0)
				{
					//Делаем запрос чтобы получить код компании
					$company  = $mysqli->query("SELECT id_company FROM admins WHERE id_admin=".$id_admin);	
					$value_company = $company->fetch_assoc();
						if ($mysqli->query(
							"INSERT INTO places(name, id_company, login, password, description)
							 VALUES	('".$_POST['name']."','".$value_company['id_company']."','".$login
							 ."','".$password."','".$desc
							 ."')" ) == TRUE)
							 {
								$last_id  = $mysqli->query("SELECT LAST_INSERT_ID()");
								$value_id = $last_id->fetch_assoc();
								if ($mysqli->query("INSERT INTO admin_places(id_admin, id_place) VALUES (".$id_admin.", ".$value_id['LAST_INSERT_ID()'].")")){
									$result['status'] = 'success';
									$result['info'] = LANG_DATA_SUCCESS;
								}else{
									$result['status'] = 'error';
									$result['info'] = LANG_ERR_FORMAT_DATA;
								}
								
							 }else{
								$result['status'] = 'error';
								$result['info'] = LANG_ERR_FORMAT_DATA;
							 }
				}else{
					$result['status'] = 'error';
					$result['name'] = 'name';
					$result['info'] = LANG_ERR_NAME_DUBLICAT;
				}
			}else{
				$result['status'] = 'error';
				$result['name'] = 'login';
				$result['info'] = LANG_ERR_LOGIN_DUBLICAT;
			}
		
		}
		
		// преобразуем в JSON-формат
		echo json_encode($result);
		
	}
	//else echo 'debug';
?>
