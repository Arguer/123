<?php
include "localization.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы
?>
<!doctype html>
<html>
	<head>
		<?php echo $head->getHead(LANG_PANEL_TITLE, LANG_CONTENT_1); ?>
		<link rel="stylesheet" href="assets/datatables/data-tables.bootstrap4.min.css">
		<script src="assets/web/assets/jquery/jquery.min.js"></script>
		<script src="js/jquery-migrate-3.0.0.min.js"></script>
		<!--[if lte IE 8]><link rel="stylesheet" href="/leaflet/leaflet.ie.css" /><![endif]-->
		<link rel="stylesheet" href="assets/theme/css/leaflet.css" />
		<script src="js/leaflet/leaflet.js"></script>
		<link rel="stylesheet" href="assets/theme/css/MarkerCluster.css" />
		<link rel="stylesheet" href="assets/theme/css/MarkerCluster.Default.css" />
		<script src="js/leaflet/leaflet.markercluster-src.js"></script>
		<script src="js/d3.v3.min.js" charset="utf-8"></script>
	</head>
	<style>
		.image-map {
			position:relative;
		}
		.hint-latlng {
			
		}
		.image-map:hover .hint-latlng.hint-show {
			opacity:1;
		}
		.image-content {
			display:inline-block;
			position:relative;
		}
	</style>
	<body onLoad="loadImage(); loadLatLng();">
		<div class="image-map">
			<div class="hint-latlng"></div>
			<div class="image-content"></div>
		</div>
		<div class="buttons-block">
			<div class="navbar-points">
				<div class="point-image">
					<input id="upimg" type="file" name="upimg" />
					<a id="upload_img" class="btn btn-sm btn-primary display-3">Загрузить</a>
				</div>
				<div class="point-item" >
					<span class="point-title" color-point="red">A</span>
					<input type="text" name="lat1" id="lat1" class="point-inp point-lat" color-point="red" title="широта" autocomplete="off" placeholder="широта">
					<input type="text" name="lng1" id="lng1" class="point-inp point-lng" color-point="red" title="долгота" autocomplete="off" placeholder="долгота">
					<span class="point-delete" title="Очистить"><i class="fa fa-trash" aria-hidden="true"></i></span>
				</div>
				<div class="point-item">
					<span class="point-title" color-point="blue">B</span>
					<input type="text" name="lat2" id="lat2" class="point-inp point-lat" color-point="blue" title="широта" autocomplete="off" placeholder="широта">
					<input type="text" name="lng2" id="lng2" class="point-inp point-lng" color-point="blue" title="долгота" autocomplete="off" placeholder="долгота">
					<span class="point-delete" title="Очистить"><i class="fa fa-trash" aria-hidden="true"></i></span>
				</div>
				<div class="point-item">
					<span class="point-title" color-point="green">C</span>
					<input type="text" name="lat3" id="lat3" class="point-inp point-lat" color-point="green" title="широта" autocomplete="off" placeholder="широта">
					<input type="text" name="lng3" id="lng3" class="point-inp point-lng" color-point="green" title="долгота" autocomplete="off" placeholder="долгота">
					<span class="point-delete" title="Очистить"><i class="fa fa-trash" aria-hidden="true"></i></span>
				</div>
				<a id="upload" class="btn btn-sm btn-primary display-3">Применить</a>
			</div>
		</div>
	</body>
	<script>
	var pinp = $('.point-inp'),
		image_map = $('.image-content'),
		ptitle = $('.point-title');
		/*img_map = $('.img-map'),
		img_width = img_map.width(),
		img_height = img_map.height();
		
	console.log(parseInt(img_width));*/
	
	pinp.on('mouseup', function(e){
		
		if(pinp.on('focus')){
			
			var color_point = $(this).attr('color-point');
			
			pinp.removeClass('focus');
			ptitle.removeClass('focus');
			//$(this).addClass('focus');
			$(this).parent('.point-item').find('.point-inp').addClass('focus');
			$(this).parent('.point-item').find('.point-title').addClass('focus');
			//$('#map').attr('cursor-map', color_point);
			$('.hint-latlng').addClass('hint-show');
			
		}
		
	});
	
	image_map.on('mousemove', function(e) {
		//console.log("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng);
		//$('.hint-latlng').show(500);
		var parentOffset = $(this).parent().position(),
		//var parentOffset = $('.img-map').position(),
			posX = e.pageX - parentOffset.left,
			posY = e.pageY - parentOffset.top;
		//console.log(posX+', '+posY);
		$('.hint-latlng').text(posX+', '+posY);
	});
	
	$('#upload_img').on('click', function() {
		var file_data = $('#upimg').prop('files')[0];
		var form_data = new FormData();
		form_data.append('file', file_data);
		//console.log(form_data);
		//alert(form_data);
		$.ajax({
			url: 'image_upload.php',
			dataType: 'text',
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function(e){
				//alert(php_script_response);
				//window.location.reload(true);
				//console.log('123');
				//init();
				loadImage();
			}
		});
	});
	
	image_map.on('click', function(e){
		if(pinp.hasClass('focus')){
			var parentOffset = $(this).parent().offset(),
			posX = e.pageX - parentOffset.left,
			posY = e.pageY - parentOffset.top;
			$('.point-inp.point-lat.focus').val(posY);
			$('.point-inp.point-lng.focus').val(posX);
			/*if($('.point-inp.focus').is('[color-point=red]')){
				if(new_red_marker) map.removeLayer(new_red_marker);
				new_red_marker = new L.marker(e.latlng, {icon: redIcon}).addTo(map);
			}
			if($('.point-inp.focus').is('[color-point=blue]')){
				if(new_blue_marker) map.removeLayer(new_blue_marker);
				new_blue_marker = new L.marker(e.latlng, {icon: blueIcon}).addTo(map);
			}
			if($('.point-inp.focus').is('[color-point=green]')){
				if(new_green_marker) map.removeLayer(new_green_marker);
				new_green_marker = new L.marker(e.latlng, {icon: greenIcon}).addTo(map);
			}*/
		}
	});
	
	$('#upload').on('click', function(){
		
		//loadImage();
		
		var img_map = $('.img-map'),
			lat1 = $('#lat1'),
			lat2 = $('#lat2'),
			lat3 = $('#lat3'),
			lng1 = $('#lng1'),
			lng2 = $('#lng2'),
			lng3 = $('#lng3'),
			img_width = parseInt(img_map.width()),
			img_height = parseInt(img_map.height());
			
		var lat_dist = lat1.val() - lat2.val();
		var lng_dist = lng1.val() - lng2.val();
			
		var img_data = new FormData();
		
		img_data.append('img_width', img_width);
		img_data.append('img_height', img_height);
		img_data.append('lat1', lat1.val());
		img_data.append('lat2', lat2.val());
		img_data.append('lat3', lat3.val());
		img_data.append('lng1', lng1.val());
		img_data.append('lng2', lng2.val());
		img_data.append('lng3', lng3.val());
		img_data.append('lat_dist', convertMinus(lat_dist));
		img_data.append('lng_dist', convertMinus(lng_dist));
		
		
		console.log(parseInt(img_width)+', '+parseInt(img_height));
		
		//var img_data = new Map([['img_width', img_width], ['img_height', img_height]]);
		
		//var img_data = {'img_width': img_width, 'img_height': img_height};
		
		/*var arr = Object.keys(img_data).map(function(key) {
			//return [Number(key), img_data[key]];
			//return img_data[key];
			return {[key]: img_data[key]};
		});*/
		
		//console.log(img_data);
		
		$.ajax({
			url: 'image_upload_param.php',
			dataType: 'text',
			cache: false,
			contentType: false,
			processData: false,
			data: img_data,
			type: 'post',
			success: function(e){
				alert('success');
			}
		});
		
	});
	
	$(document).click(function(e){
		
		//if(!$('#map').is(e.target)){ console.log('!!!!'); }
		
		if(!pinp.is(e.target) && pinp.has(e.target).length === 0){
			pinp.removeClass('focus');
			ptitle.removeClass('focus');
			$('.hint-latlng').removeClass('hint-show');
		}
		
	});
	
	function convertMinus(num){
		
		var num;
		
		if(num < 0){
			num = num*(-1);
			return num;
		}else{
			return num;
		}
		
	}
	
	//функция расчета процента разности
	function getNumPer(num, rnum) {
		
		var summPerNum, perSumm, nums, summ;
		
		nums = num - rnum;
		summPerNum = num / 100;
		perSumm = 1/summPerNum;
		summ = nums * perSumm;
		
		//проверка на отрицательное число
		if(summ < 0){
			summ = summ*(-1);
			return summ;
		}else{
			return summ;
		}
		
	}
	
	//функция округления
	var rounded = function(number){
		return +number.toFixed(5);
	}
	
	//console.log(rounded(getNumPer(831, 841)));
	
	//var img_data = [];
	
	//функция отображения изображения
	function loadImage(){
		
		$.ajax({
			url: '/uploads/data_image_file.json',
			dataType: 'json',
			success: function (data) {
				//console.log(data);
				
				var image_block = '<img class="img-map" src="/uploads/'+data.fileName+'">';
				$('.image-content').html(image_block);
				
			}
		});
		
	}
	
	//функция отображения точек на изображении
	function loadLatLng() {
		
		$.ajax({
			url: '/uploads/data_image_param.json',
			dataType: 'json',
			success: function (data) {
				
				$('#lat1').val(data.lat1);
				$('#lat2').val(data.lat2);
				$('#lat3').val(data.lat3);
				$('#lng1').val(data.lng1);
				$('#lng2').val(data.lng2);
				$('#lng3').val(data.lng3);
				
				var marker_image1 = '<div class="marker-image marker1"></div>';
				image_map.append(marker_image1);
				$('.marker-image.marker1').css({top:data.lat1+'px', left:data.lng1+'px'});
				
				var marker_image2 = '<div class="marker-image marker2"></div>';
				image_map.append(marker_image2);
				$('.marker-image.marker2').css({top:data.lat2+'px', left:data.lng2+'px'});
				
				var marker1_left = $('.marker-image.marker1').offset().left;
				console.log(marker1_left);
				var marker2_left = $('.marker-image.marker2').offset().left;
				console.log(marker2_left);
				console.log(marker1_left - marker2_left);
				
			}
		});
		
	}
	
	</script>
</html>