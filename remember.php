<?php
session_start();
/*
 * Восстановление пароля
 * © Эрис
*/
include "connection.php";
include "localization.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы

require_once 'PHPMailer/class.phpmailer.php';
require_once 'PHPMailer/class.smtp.php';

//echo (extension_loaded('openssl')?'SSL loaded':'SSL not loaded')."\n";

if (isset($_SESSION['user'])){
	$id_user = $_SESSION['user'];

	$table = $mysqli->query("SELECT * FROM admin_role WHERE id_admin = ".$id_user."");

	while($val_table = $table->fetch_assoc()){
		$id_role = $val_table['id_role'];
	}

}

$res_query = $mysqli->query("SELECT * FROM smtp_set");

$mail = new PHPMailer;
$mail->CharSet = 'UTF-8';

// Настройки SMTP
$mail->isSMTP();
$mail->SMTPAuth = true;
$mail->SMTPDebug = 0;

while($val = $res_query->fetch_assoc()){
		$host_r = $val['host'];
		$username_r = $val['username'];
		$pass_r = $val['password'];
		$secure_r = $val['secure'];
		$port_r = $val['port'];
		$email_from_r = $val['email_from'];
		$siteaddr_r = $val['siteaddr'];
		$sitename_r = $val['sitename'];
}

$mail->Host = $host_r;
$mail->Username = $username_r;
$mail->Password = $pass_r;
$mail->SMTPSecure = $secure_r;
$mail->Port = $port_r;
$mail->Mailer = 'smtp';
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $head->getHead(LANG_RMB_RECOVERY); ?>
</head>
<body>
	<?php include "headpanel.php";?>
	<section class="section-table cid-r13PedKtlK page-remember" id="table1-4">
		<div class="container container-table table-login">
			<h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2"><?=LANG_RMB_RECOVERY?></h2>
			<div class="remember-text"><?=LANG_RMB_TEXT?></div>
	 <?php
		//include "connection.php";

		// email пользователя
		if(isset($_POST['remember']))
			$remember = trim($_POST['remember']);

		if($remember){

			$res = $mysqli->query("SELECT * FROM admins WHERE email ='".$remember."'");
			$value = $res->fetch_assoc();
			$id_adm = $value['id_admin'];

			if ($id_adm > 0){

				$res_adm_key = $mysqli->query("SELECT * FROM tmp_key_admin WHERE id_admin = '".$id_adm."'");

				$subject = LANG_RMB_RECOVERY;

				$key = md5($remember.$id_adm.rand(0,10000)); //генерируемый ключ

				$this_site = $siteaddr_r.'/remember_new.php'; //страница создания нового пароля

				$link_rmb = $this_site.'?id_user='.$id_adm.'&key='.$key; //параметры для ссылки восстановления

				$link = '<a href="'.$link_rmb.'">'.$link_rmb.'</a>'; //ссылка восстановления пароля

				$message = LANG_RMB_MESS.' '.$link;

				if($res_adm_key->num_rows == 0){
					$mysqli->query("INSERT INTO tmp_key_admin (id_admin, key_admin) VALUES ('".$id_adm."', '".$key."')");
				}else{
					$mysqli->query("UPDATE tmp_key_admin SET key_admin = '".$key."' WHERE id_admin = ".$id_adm);
				}

				// От кого
				$mail->setFrom($email_from_r, $sitename_r);
				// Кому
				$mail->addAddress($remember);
				// Тема письма
				$mail->Subject = $subject;
				// Тело письма
				$mail->msgHTML($message);

				//отправка сообщения на почту
				if($mail->send()){
					echo '<div class="mess-remember mess-success">'.LANG_RMB_SUCCESS_MESS.'</div>';
				}else{
					echo '<div class="mess-remember mess-err">'.LANG_RMB_ERR_MAIL.'</div>';
					//echo '<div class="mess-remember mess-err">'.$mail->ErrorInfo.'</div>';
				}

			}else{

				echo '<div class="mess-remember mess-err">'.LANG_RMB_ERR_MAIL_FOUND.'</div>';

			}

		}

	?>
			<form method="post" class="remember-form" action="remember.php">
				<input type="email" name="remember" value="" placeholder="E-mail" required>
				<input type="submit" name="send_remember" class="btn btn-primary" value="<?=LANG_SEND?>">
			</form>
			<?php if($id_role == 1): ?>
			<div class="remember-link">
				<a href="/remember_set.php"><?=LANG_RMB_SET?></a>
			</div>
			<?php endif; ?>
		</div>
		<p class="errormsg"><img src="assets/images/errlogin.png"></p>
	</section>
	<?php include "footer.php"; ?>
	<?php include "scripts.php"; ?>
</body>
</html>
