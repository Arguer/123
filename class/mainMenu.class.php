<?php
/*
 * Класс главного меню
 * © Эрис
 */

class MainMenu {
	
	public function showMainMenu($arr_menu, $page_menu){
	
		$menu = '<ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">';
		
		foreach($arr_menu as $value){
			foreach($value as $val=>$key){
				if($page_menu == $val){
					$menu .= '<li class="nav-item active"><a class="nav-link link text-white display-4 active" href="'.$val.'"><span>'.$key.'</span></a></li>';
				}elseif($val == '/index.php' && $page_menu == '/'){
					$menu .= '<li class="nav-item active"><a class="nav-link link text-white display-4 active" href="'.$val.'"><span>'.$key.'</span></a></li>';
				}else{
					$menu .= '<li class="nav-item"><a class="nav-link link text-white display-4" href="'.$val.'"><span>'.$key.'</span></a></li>';
				}
			}
		}
		
		$menu .= '</ul>';
		
		echo $menu;
	
	}

}

?>