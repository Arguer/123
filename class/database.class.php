<?php
class DataBase{

	public static $mConnect;
	public static $mSelectDB;
	
	public static function Connect($host, $user, $pass, $name){
	
		self::$mConnect = mysqli_connect($host, $user, $pass); // создаем соединение с базой
		
		if(!self::$mConnect){ // если подключение не прошло
			echo "<p><font color='red'>Data Base connected error</font></p>";
			exit();
			return false;
		}
		
		self::$mSelectDB = mysqli_select_db($name, self::$mConnect); // выбираем базу
		
		if(!self::$mSelectDB){ // если база не выбрана
			echo "<p>".mysqli_error()."</p>";
			exit();
			return false;
		}
		
		return self::$mConnect; // возвращаем результат
	}
	
	public static function Close() { // закрывает соединение с базой
		return mysqli_close(self::$mConnect);
	}
	
}
?>