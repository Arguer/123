<?php
session_start();
/*
 * Страница справки
 * © Эрис
*/
include "connection.php";
include "localization.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы
?>
<html>
	<head>
		<?php echo $head->getHead(); ?>
		<link rel="stylesheet" href="assets/theme/css/jquery.fancybox.min.css">
	</head>
	<body>
	<?php include "headpanel.php";
		if (!isset($_SESSION['user'])){
		  header('Location: /login.php');
		  exit;
		}
	?>
	<section class="page-help cid-r13PedKtlK">
		<div class="container">
			<h2 class="mbr-section-title mbr-fonts-style align-center"><?=LANG_HELP_1?></h2>
			<h3 class="mbr-section-title mbr-fonts-style align-center"><?=LANG_HELP_2?></h3>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/001.jpg"><img class="help-image" src="/assets/images/001_prev.jpg"></a></p>
			<p><br></p>
			<p><h5 class="align-center"><?=LANG_HELP_3?></h5></p>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/003.jpg"><img class="help-image" src="/assets/images/003_prev.jpg"></a></p>
			<p>
				<ol>
					<li><?=LANG_HELP_4?></li>
					<li><?=LANG_HELP_5?></li>
					<li><?=LANG_HELP_6?></li>
					<li><?=LANG_HELP_7?></li>
					<li><?=LANG_HELP_8?></li>
					<li><?=LANG_HELP_9?></li>
				</ol>
			</p>
			<h3 class="mbr-section-title mbr-fonts-style align-center"><?=LANG_HELP_40?></h3>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/005.jpg"><img class="help-image" src="/assets/images/005_prev.jpg"></a></p>
			<p><br></p>
			<p><h5 class="align-center"><?=LANG_HELP_10?></h5></p>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/006.jpg"><img class="help-image" src="/assets/images/006_prev.jpg"></a></p>
			<p>
				<ol>
					<li><?=LANG_HELP_11?></li>
					<li><?=LANG_HELP_12?></li>
					<li><?=LANG_HELP_13?></li>
					<li><?=LANG_HELP_14?></li>
					<li><?=LANG_HELP_15?></li>
				</ol>
			</p>
			<h3 class="mbr-section-title mbr-fonts-style align-center"><?=LANG_HELP_16?></h3>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/008.jpg"><img class="help-image" src="/assets/images/008_prev.jpg"></a></p>
			<p><?=LANG_HELP_17?></p>
			<h3 class="mbr-section-title mbr-fonts-style align-center"><?=LANG_HELP_18?></h3>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/007.jpg"><img class="help-image" src="/assets/images/007_prev.jpg"></a></p>
			<p><?=LANG_HELP_19?></p>
			<h3 class="mbr-section-title mbr-fonts-style align-center"><?=LANG_HELP_20?></h3>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/010.jpg"><img class="help-image" src="/assets/images/010_prev.jpg"></a></p>
			<p><?=LANG_HELP_21?></p>
			<p><?=LANG_HELP_22?></p>
			<p><?=LANG_HELP_23?></p>
			<h3 class="mbr-section-title mbr-fonts-style align-center"><?=LANG_HELP_24?></h3>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/011.jpg"><img class="help-image" src="/assets/images/011_prev.jpg"></a></p>
			<p><?=LANG_HELP_25?></p>
			<p><?=LANG_HELP_26?></p>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/map1-0.jpg"><img class="help-image" src="/assets/images/map1-0.jpg"></a></p>
			<p><?=LANG_HELP_27?></p>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/map1-1.jpg"><img class="help-image" src="/assets/images/map1-1.jpg"></a></p>
			<p><?=LANG_HELP_28?></p>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/map2-1.jpg"><img class="help-image" src="/assets/images/map2-1.jpg"></a></p>
			<p><?=LANG_HELP_29?></p>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/map3-1.jpg"><img class="help-image" src="/assets/images/map3-1.jpg"></a></p>
			<p><?=LANG_HELP_30?></p>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/table1.jpg"><img class="help-image" src="/assets/images/table1_prev.jpg"></a></p>
			<p><?=LANG_HELP_31?></p>
			<p><?=LANG_HELP_32?></p>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/012.jpg"><img class="help-image" src="/assets/images/012_prev.jpg"></a></p>
			<p><?=LANG_HELP_39?></p>
			<h3 class="mbr-section-title mbr-fonts-style align-center"><?=LANG_HELP_33?></h3>
			<p><?=LANG_HELP_34?></p>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/013.jpg"><img class="help-image" src="/assets/images/013_prev.jpg"></a></p>
			<p><?=LANG_HELP_35?></p>
			<p><?=LANG_HELP_36?></p>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/014.jpg"><img class="help-image" src="/assets/images/014_prev.jpg"></a></p>
			<h3 class="mbr-section-title mbr-fonts-style align-center"><?=LANG_HELP_37?></h3>
			<p class="align-center"><a data-fancybox="gallery" href="/assets/images/009.jpg"><img class="help-image" src="/assets/images/009_prev.jpg"></a></p>
			<p><?=LANG_HELP_38?></p>
		</div>
	</section>
	<!-- footer -->
	<?php include "footer.php"; ?>
	<!-- end footer -->
	<?php include "scripts.php"; ?>
	<script type="text/javascript" src="/js/jquery.fancybox.min.js"></script>
	</body>
</html>