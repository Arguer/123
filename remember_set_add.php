<?php
session_start();
/*
 * Страница добавления настроек восстановления пароля в базу данных
 * © Эрис
*/
include 'connection.php';
include "localization.php";

$response = array();

if(isset($_POST['id_rmb']))
  $id_rmb = trim($_POST['id_rmb']);
if(isset($_POST['host_rmb']))
  $host_rmb = trim($_POST['host_rmb']);
if(isset($_POST['email_rmb']))
  $email_rmb = trim($_POST['email_rmb']);
if(isset($_POST['pass_rmb']))
  $pass_rmb = trim($_POST['pass_rmb']);
if(isset($_POST['secure_rmb']))
  $secure_rmb = trim($_POST['secure_rmb']);
if(isset($_POST['port_rmb']))
  $port_rmb = trim($_POST['port_rmb']);
if(isset($_POST['email_from_rmb']))
  $email_from_rmb = trim($_POST['email_from_rmb']);
if(isset($_POST['siteaddr_rmb']))
  $siteaddr_rmb = trim($_POST['siteaddr_rmb']);
if(isset($_POST['sitename_rmb']))
  $sitename_rmb = trim($_POST['sitename_rmb']);

if(!$host_rmb){
  $response['status'] = 'error';
	$response['info'] = LANG_ERR_HOST;
	$response['name'] = 'host_rmb';
}elseif (!$email_rmb) {
  $response['status'] = 'error';
	$response['info'] = LANG_ERR_EMAIL;
	$response['name'] = 'email_rmb';
}elseif (!$pass_rmb) {
  $response['status'] = 'error';
	$response['info'] = LANG_ERR_PASSWORD;
	$response['name'] = 'pass_rmb';
}elseif (!$secure_rmb) {
  $response['status'] = 'error';
	$response['info'] = LANG_ERR_ENCRYPT;
	$response['name'] = 'secure_rmb';
}elseif (!$port_rmb) {
  $response['status'] = 'error';
	$response['info'] = LANG_ERR_PORT;
	$response['name'] = 'port_rmb';
}elseif (!$email_from_rmb) {
  $response['status'] = 'error';
	$response['info'] = LANG_ERR_EMAIL;
	$response['name'] = 'email_from_rmb';
}elseif (!$siteaddr_rmb) {
  $response['status'] = 'error';
	$response['info'] = LANG_ERR_SITEADDR;
	$response['name'] = 'siteaddr_rmb';
}elseif (!$sitename_rmb) {
  $response['status'] = 'error';
	$response['info'] = LANG_ERR_SITENAME;
	$response['name'] = 'sitename_rmb';
} else {

  $result = $mysqli->query("SELECT * FROM smtp_set");

  if($secure_rmb == 'ssl'){
    $secure_val = 1;
  }elseif ($secure_rmb == 'tls') {
    $secure_val = 2;
  }

  if ($result->num_rows == 0) {

    if ($mysqli->query("INSERT INTO smtp_set (host, username, password, secure, port, email_from, siteaddr, sitename) VALUES ('".$host_rmb."', '".$email_rmb."', '".$pass_rmb."', '".$secure_rmb."', '".$port_rmb."', '".$email_from_rmb."', '".$siteaddr_rmb."', '".$sitename_rmb."')") == TRUE){
			$response['status'] = 'success';
			$response['info'] = LANG_SET_SUCCESS;
		} else {
			$response['status'] = 'error';
			$response['info'] = LANG_ERR_CREATE.'.<br>'.$mysqli->error;
		}

  } else {

    if($id_rmb){

      if ($mysqli->query('UPDATE smtp_set SET host = "'.$host_rmb.'", username = "'.$email_rmb.'", password = "'.$pass_rmb.'", secure = "'.$secure_rmb.'", port = "'.$port_rmb.'", email_from = "'.$email_from_rmb.'", siteaddr = "'.$siteaddr_rmb.'", sitename = "'.$sitename_rmb.'" WHERE id_set = '.$id_rmb.'') == TRUE){
        $response['status'] = 'success';
        $response['info'] = LANG_SUCCESS_CHANGED;
      } else {
        $response['status'] = 'error';
        $response['info'] = LANG_ERR_FORMAT_DATA;
      }

    }

  }

}

echo json_encode($response);

?>
