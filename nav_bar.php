<?php
	session_start();
/*
 * Главное меню
 * © Эрис
*/
	include "connection.php";

	if (isset($_SESSION['user'])){
		$id_user = $_SESSION['user'];

		$table = $mysqli->query("SELECT * FROM admin_role WHERE id_admin = ".$id_user."");

		while($val = $table->fetch_assoc()){
			$id_role = $val['id_role'];
		}
	}

	require_once 'class/mainMenu.class.php'; // подключение класса вывода главного меню

	$menu = new MainMenu(); //класс главного меню

	if($id_role == 1){
		$arr_menu = array(
			array('/index.php'=>LANG_HOME),
			array('/object.php'=>LANG_OBJECTS),
			array('/devices.php'=>LANG_DEVICES),
			array('/company.php'=>LANG_COMPANIES),
			array('/users.php'=>LANG_USERS),
			array('/about.php'=>LANG_ABOUT_SYSTEM)
		);
	}else{
		$arr_menu = array(
			array('/index.php'=>LANG_HOME),
			array('/object.php'=>LANG_OBJECTS),
			array('/devices.php'=>LANG_DEVICES),
			array('/about.php'=>LANG_ABOUT_SYSTEM)
		);
	}

	$this_page = $_SERVER['REQUEST_URI'];

	$menu->showMainMenu($arr_menu, $this_page);

?>
