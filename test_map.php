<?php
session_start();
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы
?>
<html>
<head>
	<?php echo $head->getHead(LANG_PANEL_TITLE, LANG_CONTENT_1); ?>
	<link rel="stylesheet" href="assets/datatables/data-tables.bootstrap4.min.css">
	<script src="assets/web/assets/jquery/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.0.min.js"></script>
	<!--[if lte IE 8]><link rel="stylesheet" href="/leaflet/leaflet.ie.css" /><![endif]-->
	<link rel="stylesheet" href="assets/theme/css/leaflet.css" />
  <script src="js/leaflet/leaflet.js"></script>
	<link rel="stylesheet" href="assets/theme/css/MarkerCluster.css" />
  <link rel="stylesheet" href="assets/theme/css/MarkerCluster.Default.css" />
	<script src="js/leaflet/leaflet.markercluster-src.js"></script>
	<script src="js/d3.v3.min.js" charset="utf-8"></script>
</head>
<body>
	<div class="map" id="map"></div>
</body>
<?php include "scripts.php"; ?>
<script src="assets/datatables/jquery.data-tables.min.js"></script>
<script src="assets/datatables/data-tables.bootstrap4.min.js"></script>
<script src="js/jquery.touchSwipe.min.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/scripts.js"></script>
<script src="js/leaflet/leaflet-hash.js"></script>
<script>
	var map = L.map('map', {
		crs: L.CRS.Simple,
		center: [-100,-100],
		minZoom: -1,
		maxZoom: 5
		//maxBounds: 0
	});
	
	var bounds = [[-100,-500], [860,1200]];
	var image = L.imageOverlay('assets/images/map2.jpg', bounds).addTo(map);

	var sol = L.latLng([ 145, 175 ]);
	L.marker(sol).addTo(map);
	
	map.setMaxBounds(bounds);

	map.setView( [70, 120], 1);
</script>
</html>