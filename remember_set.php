<?php
session_start();
/*
 * Страница настроек восстановления пароля
 * © Эрис
*/
include 'connection.php';
include "localization.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы

if (isset($_SESSION['user'])){
  $id_user = $_SESSION['user'];
}

$table = $mysqli->query("SELECT * FROM admin_role WHERE id_admin = ".$id_user."");

while($val = $table->fetch_assoc()){
  $id_role = $val['id_role'];
}

if ($id_role != 1){
  header('Location: /login.php');
  exit;
}

$arr_host = array(
  'Gmail' => 'smtp.gmail.com',
  'Яндекс Почта' => 'smtp.yandex.ru',
  'Mail.ru' => 'smtp.mail.ru',
  'Рамблер' => 'smtp.rambler.ru',
  'iCloud' => 'smtp.mail.me.com',
  'Мастерхост' => 'smtp.masterhost.ru',
  'Timeweb' => 'smtp.timeweb.ru',
  'Хостинг Центр (hc.ru)' => 'smtp.домен.ru',
  'REG.RU' => 'serverXXX.hosting.reg.ru',
  'ДЖИНО' => 'smtp.jino.ru',
  'nic.ru' => 'mail.nic.ru',
  'beget.com' => 'smtp.beget.com'
);

//$arr_secure = array('ssl', 'tls');

$flag_edit = 0;

$res = $mysqli->query("SELECT * FROM smtp_set");

if($res->num_rows > 0){
    while($value = $res->fetch_assoc()){
      $id_rmb = $value['id_set'];
      $host_rmb = $value['host'];
      $email_rmb = $value['username'];
      $pass_rmb = $value['password'];
      $secure_rmb = $value['secure'];
      $port_rmb = $value['port'];
      $email_from_rmb = $value['email_from'];
      $siteaddr_rmb = $value['siteaddr'];
      $sitename_rmb = $value['sitename'];
    }
    $flag_edit = 1;
}

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $head->getHead(LANG_PANEL_TITLE, LANG_CONTENT_1); ?>
</head>
<body>
	<?php include "headpanel.php";?>
	<section class="section-table cid-r13PedKtlK page-remember" id="table1-4">
		<div class="container container-table table-login">
			<h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2"><?=LANG_RMB_SETTINGS?></h2>
      <h5 class="mbr-section-title mbr-fonts-style align-center pb-3"><?=LANG_RMB_SET_TEXT?></h5>
      <form class="mbr-form form-install-bd" name="remember_set" id="remember_set" method="post" action="" autocomplete="off">
        <div class="row row-sm-offset row-host">
          <div class="col-md-4 multi-horizontal" data-for="host_rmb">
            <div class="form-group">
              <label class="form-control-label mbr-fonts-style display-7">Host* (<?=LANG_EXAMPLE?>: smtp.gmail.com):</label>
              <input type="text" class="form-control" name="host_rmb" maxlength="150" id="host_rmb" value="<?php echo $host_rmb; ?>" placeholder="Host">
            </div>
          </div>
          <a href="#" class="example-link"><?=LANG_MORE_EXAMPLES?></a>
          <div class="example-block">
              <?php foreach($arr_host as $key=>$val): ?>
                <p><?php echo $key.': <b>'.$val.'</b>'; ?></p>
              <?php endforeach; ?>
          </div>
        </div>
        <div class="row row-sm-offset">
          <div class="col-md-4 multi-horizontal" data-for="email_rmb">
            <div class="form-group">
              <label class="form-control-label mbr-fonts-style display-7"><?=LANG_EMAIL?>* (<?=LANG_MAILBOX?>):</label>
              <input type="email" class="form-control" name="email_rmb" maxlength="150" id="email_rmb" value="<?php echo $email_rmb; ?>" placeholder="E-mail">
            </div>
          </div>
        </div>
        <div class="row row-sm-offset">
          <div class="col-md-4 multi-horizontal" data-for="pass_rmb">
            <div class="form-group">
              <label class="form-control-label mbr-fonts-style display-7"><?=LANG_PASS_MAILBOX?>*:</label>
              <input type="password" class="form-control" name="pass_rmb" maxlength="150" id="pass_rmb" value="<?php echo $pass_rmb; ?>" placeholder="<?=LANG_PASS?>">
            </div>
          </div>
        </div>
        <div class="form-group" data-for="secure">
          <label class="form-control-label mbr-fonts-style display-7"><?=LANG_ENCRYPT_TYPE?>*:</label>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="secure_rmb" id="secure_radio1" value="ssl" checked>
            <label class="form-check-label" for="secure_radio1">SSL</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="secure_rmb" id="secure_radio2" value="tls">
            <label class="form-check-label" for="secure_radio2">TLS</label>
          </div>
        </div>
        <div class="row row-sm-offset">
          <div class="col-md-4 multi-horizontal" data-for="port_rmb">
            <div class="form-group">
              <label class="form-control-label mbr-fonts-style display-7"><?=LANG_PORT?>* (<?=LANG_USUALLY?>: 465):</label>
              <input type="text" class="form-control" name="port_rmb" maxlength="150" id="port_rmb" value="<?php echo $port_rmb; ?>" placeholder="<?=LANG_PORT?>">
            </div>
          </div>
        </div>
        <div class="row row-sm-offset">
          <div class="col-md-4 multi-horizontal" data-for="email_from_rmb">
            <div class="form-group">
              <label class="form-control-label mbr-fonts-style display-7"><?=LANG_EMAIL_VIEW?>* (<?=LANG_EXAMPLE?>: noreply@site.com):</label>
              <input type="text" class="form-control" name="email_from_rmb" maxlength="150" id="email_from_rmb" value="<?php echo $email_from_rmb; ?>" placeholder="E-mail">
            </div>
          </div>
        </div>
        <div class="row row-sm-offset">
          <div class="col-md-4 multi-horizontal" data-for="siteaddr_rmb">
            <div class="form-group">
              <label class="form-control-label mbr-fonts-style display-7"><?=LANG_SITEADDR?>* (<?=LANG_EXAMPLE?>: http://site.com или https://site.com):</label>
              <input type="text" class="form-control" name="siteaddr_rmb" maxlength="150" id="siteaddr_rmb" value="<?php echo $siteaddr_rmb; ?>" placeholder="http://site.com">
            </div>
          </div>
        </div>
        <div class="row row-sm-offset">
          <div class="col-md-4 multi-horizontal" data-for="sitename_rmb">
            <div class="form-group">
              <label class="form-control-label mbr-fonts-style display-7"><?=LANG_SITENAME?>*:</label>
              <input type="text" class="form-control" name="sitename_rmb" maxlength="150" id="sitename_rmb" value="<?php echo $sitename_rmb; ?>" placeholder="<?=LANG_SITENAME?>">
            </div>
          </div>
        </div>
        <?php if($flag_edit == 1): ?>
        <input type="hidden" name="id_rmb" id="id_rmb" value="<?php echo $id_rmb; ?>">
        <?php endif; ?>
        <div class="row row-sm-offset">
          <div class="col-md-4 multi-horizontal">
            <div class="form-group form-btn-group">
              <button type="submit" name="send_rmb_set" id="send_rmb_set" class="btn btn-primary btn-form display-4"><?php if($flag_edit == 0): ?><?=LANG_SET_INST?><?php else: ?><?=LANG_SET_CHANGE?><?php endif; ?></button>
              <a href="/remember.php" class="back-link"><?=LANG_BACK_RMB?></a>
            </div>
          </div>
		  <p>* Поля обязательные для заполнения</p>
        </div>
      </form>
		</div>
	</section>
	<?php include "footer.php"; ?>
	<?php include "scripts.php"; ?>
  <script>
    $('#remember_set').find('#send_rmb_set').click(function(e){
      e.preventDefault();
      var form = $('#remember_set');
      $('.err-text').remove();
      $('.success-text').remove();
      // обработка ajax
      $.ajax({
        type: "post",
        url: "remember_set_add.php",
        dataType: "json",
        data: form.serialize(),
        success: function(data) {
          // если запрос успешно обработан
          form.find('input, textarea').removeClass('err-field');
          if(data.status == 'error'){
            // если json вернул ошибки
            form.before('<div class="err-text">' + data.info + '</div>');
            if(data.name)
              form.find('[name="' + data.name + '"]').addClass('err-field');
          }else if(data.status == 'success'){
            // если данные успешно обработаны
            alert(data.info);
            location.href='/remember_set.php';
          }
        },
        error: function (xhr, ajaxOptions, thrownError){
          form.before('<div class="err-text">' + thrownError + '</div>');
        }
      });
    });
    $('.dropdown-host').find('.dropdown-item').click(function(e){
      e.preventDefault();
      var text_val = $(this).text(),
        data_val = $(this).attr('data-value');
      $('.dropdown-host').find('.dropdown-item').removeClass('hover-item');
      $(this).parent('.dropdown-menu').parent('.dropdown').find('.inp-dropdown').val(data_val);
      $(this).parent('.dropdown-menu').parent('.dropdown').find('.dropdown-toggle').text(text_val);
      $(this).parent('.dropdown-menu').parent('.dropdown').find('.dropdown-toggle').attr('title', text_val);
      $(this).addClass('hover-item');
    });
    $('.example-link').click(function(e){
      e.preventDefault();
      $('.example-block').slideToggle();
    });
  </script>
</body>
</html>
