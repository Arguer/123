<?php
session_start();
/*
 * Страница списка аккаунтов
 * © Эрис
*/
include "connection.php";
include "localization.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы

?>
<html>
	<head>
		<?php echo $head->getHead(LANG_USERS, LANG_USERS); ?>
	</head>
	<body>
	<?php include "headpanel.php";

		if (isset($_SESSION['user'])){
			$id_user = $_SESSION['user'];

			$table = $mysqli->query("SELECT * FROM admin_role WHERE id_admin = ".$id_user."");

			while($val = $table->fetch_assoc()){
				$id_role = $val['id_role'];
			}
		}

		if (!isset($_SESSION['user']) || $id_role != 1){
		  header('Location: /login.php');
		  exit;
		}
	?>
	<section class="section-table cid-r13PedKtlK" id="table1-5">
		<div class="tab-pad80">
			<div class="container container-table">
				<h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2"><?=LANG_LIST_USERS?></h2>
				<div class="container scroll">
				<table class="table table-panel" cellspacing="0">
					<thead>
						<tr class="table-heads">
							<th class="head-item mbr-fonts-style display-7"><?=LANG_USER_ID?></th>
							<th class="head-item mbr-fonts-style display-7"><?=LANG_USER_LOGIN?></th>
							<th class="head-item mbr-fonts-style display-7"><?=LANG_USER_EMAIL?></th>
							<th class="head-item mbr-fonts-style display-7"><?=LANG_USER_COMPANY?></th>
							<th class="head-item mbr-fonts-style display-7"><?=LANG_CHANGE?></th>
						</tr>
					</thead>
					<tbody>
		<?php

		// вывод списка аккаунтов

		$res = $mysqli->query("SELECT a.*, c.id_company, c.name FROM admins a INNER JOIN company c ON a.id_company = c.id_company ORDER BY a.id_admin", MYSQLI_USE_RESULT);
		while ($value = $res->fetch_assoc())
		{
			echo '<tr>
					<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_USER_ID.'">'.$value['id_admin'].'</td>
					<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_USER_LOGIN.'">'.$value['login'].'</td>
					<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_USER_EMAIL.'">'.$value['email'].'</td>
					<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_USER_COMPANY.'">'.$value['name'].'</td>
					<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANGE.'">
						<a href="/user_set.php?user='.$value['id_admin'].'" class="edituser" data-id="'.$value['id_admin'].'">'.LANG_EDIT.'</a>
					</td>
				  </tr>';
		}

		?>
					</tbody>
				</table>
				<div class="panel-view-btn input-group-btn">
					<a href="/user_set.php" class="add-object btn btn-primary btn-form display-4">[+] <?=LANG_USER_ADD?></a>
				</div>
				</div>
			</div>
		</div>
	</section>
	<!-- footer -->
	<?php include "footer.php"; ?>
	<!-- end footer -->
	<?php include "scripts.php"; ?>
	</body>
</html>
