<?php
session_start();
/*
 * Страница устройств
 * © Эрис
*/

include "connection.php";

if (isset($_SESSION['user'])){
	$id_user = $_SESSION['user'];
}

$table = $mysqli->query("SELECT * FROM admin_role WHERE id_admin = ".$id_user."");

while($val = $table->fetch_assoc()){
	$id_role = $val['id_role'];
}

if (!isset($_SESSION['user'])) {
	header('Location: /login.php');
	exit;
}

include "localization.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы
?>
<html>
<head>
	<?php echo $head->getHead(LANG_DEVICES); ?>  
</head>
<body>
	<?php 
		include "headpanel.php";
	?>
	<section class="section-table cid-r13PedKtlK" id="table1-4">
	<div class="tab-pad80">
	  <div class="container container-table">
			  <h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2"><?=LANG_DEVICES_COMPANY?></h2>
			  <div class="container scroll"> 
			  <table class="table isSearch table-panel" cellspacing="0">
				<thead>
					<tr class="table-heads">
						<th class="head-item mbr-fonts-style display-7"><?=LANG_TYPE?></th>
						<th class="head-item mbr-fonts-style display-7"><?=LANG_FACTORY_NUMBER?></th>
						<th class="head-item mbr-fonts-style display-7"><?=LANG_DESCRIPTION?></th>
						<?php if($id_role == 1 || $id_role == 2): ?><th class="head-item mbr-fonts-style display-7"><?=LANG_CHANGE?></th><?php endif ?>
					</tr>
				</thead>
				<tbody>
	<?php
		$id_edit_device = $_GET['edit'];
		
		$id_company = $_SESSION['company'];
		$res = $mysqli->query("SELECT * FROM devices INNER JOIN type_of_device ON devices.id_type = type_of_device.id_type WHERE id_device in (SELECT id_device FROM eventlog WHERE id_place in (SELECT id_place FROM places WHERE places.id_company = ".$id_company. "))", MYSQLI_USE_RESULT);
		while ($value = $res->fetch_assoc())
		{
			if ($id_edit_device == $value['id_device'])
			{
				echo '<tr>
						<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_TYPE.'">'.$value['name'].'</td>
						<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_FACTORY_NUMBER.'">'.$value['znumber'].'</td> ';
				echo '  <td class="body-item mbr-fonts-style display-7" data-label="'.LANG_DESCRIPTION.'"><input type = "text" id = "idesc" value="'.$value['description'].'"></td>';
				if($id_role == 1 || $id_role == 2) echo '<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANGE.'"><div id="save" onclick = "edit_dev()"><a href ="/devices.php">'.LANG_SAVE.'</a></div><input id ="id_dev" type = "hidden" value = "'.$id_edit_device.'"></td>';
			}
			else
			{
				echo '<tr>
						<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_TYPE.'">'.$value['name'].'</td>
						<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_FACTORY_NUMBER.'">'.$value['znumber'].'</td> ';
				echo '  <td class="body-item mbr-fonts-style display-7" data-label="'.LANG_DESCRIPTION.'"><a href="/more.php?id='.$value['id_device'].'">'.$value['description'].'</a></td>';
				if($id_role == 1 || $id_role == 2) echo '<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANGE.'"><a href="/devices.php?edit='.$value['id_device'].'">'.LANG_EDIT.'</td>';
			}
			
			echo ' </tr>';
		}
	
	echo '</table>';
	
	?>
	 
			</div>
			</div>
	</div>		
	<div id="content2"></div>
	</section>
	<?php include "footer.php"; ?>
	<script> 
		function edit_dev()  
		{   
			var idescript = document.getElementById("idesc");
			var id_device = document.getElementById("id_dev");
			
			$.ajax({  
				url: "edit.php",
				cache: false,  
				type: "POST",
				data: { 
					   key: "1dev", 
					   description: idescript.value,
					   id: id_device.value
				},     
				success: function(html){  
					$("#content2").html(html);  
				}  
			});  
		} 
	</script>
	<?php include "scripts.php"; ?>
</body>
</html>

