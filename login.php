<?php
session_start();
/*
 * Авторизация
 * © Эрис
*/
include "localization.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $head->getHead(LANG_PANEL_TITLE, LANG_CONTENT_1); ?>
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp"></script>
</head>
<body>
	<?php include "headpanel.php";?>
	<section class="section-table cid-r13PedKtlK" id="table1-4">
		<div class="container container-table table-login">
			<h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2"><?=LANG_AUTH_USER?></h2>
	 <?php
		include "connection.php";

		$login = $_POST['login'];
		$pass  = $_POST['pass'];
		
		//Делаем защиту от SQL инъекций
	//	$pass = trim($pass);
		$pass = htmlspecialchars($pass);
		
	//	$pass = trim($login);
		$login = htmlspecialchars($login);
		
		if($login && $pass){
			
			$res = $mysqli->query("SELECT * FROM admins WHERE login ='".$login."'" , MYSQLI_USE_RESULT);
				$value = $res->fetch_assoc();			 
				$id_adm = $value['id_admin'];
				
			if ($id_adm > 0){
				
				if (version_compare(PHP_VERSION, '5.5.0') >= 0) {
				
					if(password_verify($pass, $value['password'])){
						$_SESSION['user'] = $value['id_admin'];
						$_SESSION['log'] = $value['login'];
						$_SESSION['company'] = $value['id_company'];
						('Location: /object.php');
						exit;
					}else{
						echo LANG_ERR_AUTH;
					}
				
				}else{
					
					if($pass == $value['password']){
						$_SESSION['user'] = $value['id_admin'];
						$_SESSION['log'] = $value['login'];
						$_SESSION['company'] = $value['id_company'];
						header('Location: /object.php');
						exit;
					}else{
						echo LANG_ERR_AUTH;
					}
					
				}
				
			}else{
				echo LANG_ERR_AUTH;
			}
			
		}
		
	?>
		</div>
		<p class="errormsg"><img src="assets/images/errlogin.png"></p>
	</section>
	<?php include "footer.php"; ?>
	<?php include "scripts.php"; ?>
</body>
</html>