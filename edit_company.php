<?php
	session_start();
/*
 * Редактирование компании
 * © Эрис
*/
	
	include "localization.php";
	include "connection.php";

	$name = trim($_POST['name']);
	$addr = trim($_POST['address']);
	$desc = trim($_POST['description']);
	$key = $_POST['key'];
	$cid = trim($_POST['id']);
	$edit_com = intval($_GET['editcom']);
	
	if($key == '199') {
	
		$result = array('status'=>'error', 'info'=>LANG_ERR_FIELDS, 'errorCode'=> '1');
	
		if(!$name) {
			
			$result['info'] = LANG_ERR_NAME;
			$result['name'] = 'name';
			echo json_encode($result);
		
		} elseif (!$addr) {
			
			$result['info'] = LANG_ERR_ADDR;
			$result['name'] = 'address';
			echo json_encode($result);
		
		} else {
			
			if ($mysqli->query('UPDATE company SET name = "'.$name.'", adress = "'.$addr.'", description ="'.$desc.'" WHERE id_company = '.$cid) == TRUE)
			{
				$result['status'] = 'success';
				$result['info'] = LANG_SUCCESS_CHANGED;
				echo json_encode($result);	
			} else {
				$result['status'] = 'error';
				$result['info'] = LANG_ERR_FORMAT_DATA;
				echo json_encode($result);
			}
			
		}

	}
	
?>