<?php
session_start();
/*
 * Страница ошибки 404
 * © Эрис
*/
include "localization.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы
?>
<!DOCTYPE html>
<html>
	<head>
	<?php echo $head->getHead(LANG_404); ?>
	</head>
	<body>
	<?php include "headpanel.php"; ?>
	<section class="cid-qTkA127IK8 mbr-fullscreen mbr-parallax-background page-about page-404" id="header2-1">
		<div class="mbr-fullscreen-back"></div>
		<div class="container align-center section-about section-404">
			<h1 class="title-404 mbr-white">404</h1>
			<div class="row justify-content-md-center">
				<div class="mbr-white col-lg-10 col-md-12 col-sm-12">
					<p><?=LANG_404?></p>
				</div>
			</div>
		</div>
	</section>
	<?php include "footer.php"; ?>
	<?php include "scripts.php"; ?>
	</body>
</html>