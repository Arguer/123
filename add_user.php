<?php
	session_start();
/*
 * Добавление аккаунта
 * © Эрис
*/
	include "localization.php";
	include "connection.php";

	if(isset($_POST['uid']))
		$id_user = $_POST['uid'];

	if(isset($_POST['login']))
		$login = trim($_POST['login']);
	if(isset($_POST['email']))
		$email = trim($_POST['email']);
	if(isset($_POST['password']))
		$password = trim($_POST['password']);
	if(isset($_POST['password2']))
		$password2 = trim($_POST['password2']);
	if(isset($_POST['company_user']))
		$company = trim($_POST['company_user']);
	if(isset($_POST['user_role']))
		$user_role = trim($_POST['user_role']);

	$result = array();

	$login_ln = mb_strlen($login,'UTF-8'); //длина логина
	$password_ln = mb_strlen($password,'UTF-8'); //длина пароля

	$pass_err = 0;

	// поверка на совпадение паролей
	if($password != $password2){
		$pass_err = 1;
	}

	if(!$login){
		$result['status'] = 'error';
		$result['name'] = 'login';
		$result['info'] = LANG_ERR_LOGIN;
	}elseif($login_ln < 3){
		$result['status'] = 'error';
		$result['name'] = 'login';
		$result['info'] = LANG_ERR_LOGIN_MIN_LN;
	}elseif($login_ln > 50){
		$result['status'] = 'error';
		$result['name'] = 'login';
		$result['info'] = LANG_ERR_LOGIN_LN;
	}elseif(!$email){
		$result['status'] = 'error';
		$result['name'] = 'email';
		$result['info'] = LANG_ERR_EMAIL;
	}elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$result['status'] = 'error';
		$result['name'] = 'email';
		$result['info'] = LANG_ERR_EMAIL_CORRECT;
	}elseif(!$password){
		$result['status'] = 'error';
		$result['name'] = 'password';
		$result['info'] = LANG_ERR_PASS;
	}elseif($password_ln < 3){
		$result['status'] = 'error';
		$result['name'] = 'password';
		$result['info'] = LANG_ERR_PASS_MIN_LN;
	}elseif($password_ln > 50){
		$result['status'] = 'error';
		$result['name'] = 'password';
		$result['info'] = LANG_ERR_PASS_LN;
	}elseif(!$password2){
		$result['status'] = 'error';
		$result['name'] = 'password2';
		$result['info'] = LANG_ERR_PASS_CONFIRM;
	}elseif($pass_err == 1){
		$result['status'] = 'error';
		$result['name'] = 'password';
		$result['name2'] = 'password2';
		$result['info'] = LANG_ERR_PASS_DUBLICAT;
	}elseif(!$company || $company == 0){
		$result['status'] = 'error';
		$result['name'] = 'dropdown_company';
		$result['info'] = LANG_ERR_COMPANY_SET;
	}elseif(!$user_role || $user_role == 0){
		$result['status'] = 'error';
		$result['name'] = 'user_role';
		$result['info'] = LANG_ERR_USER_ROLE;
	}else{

		$hash = password_hash($password, PASSWORD_DEFAULT);

		$password = $hash;

		$result['pass'] = $password;

		if($_POST['key']=='132'){

			$unique_login_result = $mysqli->query("SELECT * FROM admins WHERE login='".$login."'");

			if ($unique_login_result->num_rows == 0){

				if ($mysqli->query("INSERT INTO admins (login, email, password, id_company) VALUES ('".$login."', '".$email."', '".$password."', ".$company.");") == TRUE){

					if($mysqli->query("SET @last_id := LAST_INSERT_ID();")){

						if($mysqli->query("INSERT INTO admin_role (id_admin, id_role) VALUES (@last_id, ".$user_role.");") == TRUE){
							$result['status'] = 'success';
							$result['info'] = LANG_DATA_SUCCESS;
						}else{
							$result['status'] = 'error';
							$result['info'] = LANG_ERR_FORMAT_DATA.': 2 ';
						}

					}else{
						$result['status'] = 'error';
						$result['info'] = LANG_ERR_FORMAT_DATA.': last_id ';
					}

				}else{
					$result['status'] = 'error';
					$result['info'] = LANG_ERR_FORMAT_DATA.': 1 ';
				}

			}else{

				$result['status'] = 'error';
				$result['name'] = 'login';
				$result['info'] = LANG_ERR_LOGIN_DUBLICAT;

			}

		}elseif($_POST['key']=='321'){


			$unique_login_result_edit = $mysqli->query("SELECT * FROM admins WHERE login='".$login."'");

			$curr_data = $mysqli->query("SELECT * FROM admins WHERE id_admin='".$id_user."'");

			while($val = $curr_data->fetch_assoc()){
				$curr_login = $val['login'];
			}

			if($curr_login != $login){

				if ($unique_login_result_edit->num_rows == 0){

					//if ($mysqli->query('UPDATE admins a LEFT JOIN admin_role ar ON ar.id_admin = a.id_admin SET a.login = "'.$login.'", a.email = "'.$email.'", a.password = "'.$password.'", a.id_company = "'.$company.'", ar.id_role = "'.$user_role.'" WHERE a.id_admin = '.$id_user) == TRUE){

					if ($mysqli->query('UPDATE admins SET login = "'.$login.'", email = "'.$email.'", password = "'.$password.'", id_company = "'.$company.'" WHERE id_admin = '.$id_user) == TRUE){

						if ($mysqli->query('UPDATE admin_role SET id_role = "'.$user_role.'" WHERE id_admin = '.$id_user) == TRUE){
							$result['status'] = 'success';
							$result['info'] = LANG_SUCCESS_CHANGED;
						}else{
							$result['status'] = 'error';
							$result['info'] = LANG_ERR_CHANGE;
						}

					}else{
						$result['status'] = 'error';
						$result['info'] = LANG_ERR_CHANGE;
					}

				}else{

					$result['status'] = 'error';
					$result['name'] = 'login';
					$result['info'] = LANG_ERR_LOGIN_DUBLICAT;

				}

			}else{

				if ($mysqli->query('UPDATE admins SET login = "'.$login.'", email = "'.$email.'", password = "'.$password.'", id_company = "'.$company.'" WHERE id_admin = '.$id_user) == TRUE){

					if ($mysqli->query('UPDATE admin_role SET id_role = "'.$user_role.'" WHERE id_admin = '.$id_user) == TRUE){
						$result['status'] = 'success';
						$result['info'] = LANG_SUCCESS_CHANGED;
					}else{
						$result['status'] = 'error';
						$result['info'] = LANG_ERR_CHANGE;
					}

				}else{
					$result['status'] = 'error';
					$result['info'] = LANG_ERR_CHANGE;
				}

			}

		}

	}

	// преобразуем в JSON-формат
	echo json_encode($result);

?>
