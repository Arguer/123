<?php
session_start();
/*
 * Страница отображения данных подразделений
 * © Эрис
*/
include "localization.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы
?>
<html>
<head>
	<?php echo $head->getHead(LANG_PANEL_TITLE, LANG_CONTENT_1); ?>
	<link rel="stylesheet" href="assets/datatables/data-tables.bootstrap4.min.css">
	<script src="assets/web/assets/jquery/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.0.min.js"></script>
	<!--[if lte IE 8]><link rel="stylesheet" href="/leaflet/leaflet.ie.css" /><![endif]-->
	<link rel="stylesheet" href="assets/theme/css/leaflet.css" />
	<script src="js/leaflet/leaflet.js"></script>
	<link rel="stylesheet" href="assets/theme/css/MarkerCluster.css" />
	<link rel="stylesheet" href="assets/theme/css/MarkerCluster.Default.css" />
	<script src="js/leaflet/leaflet.markercluster-src.js"></script>
	<script src="js/d3.v3.min.js" charset="utf-8"></script>
	<script src="js/jQueryRotateCompressed.2.2.js"></script>
	<link rel="stylesheet" href="assets/theme/css/jquery.fancybox.min.css">
</head>
<body onLoad="init(); imageInit();">
<!--body-->
<?php include "headpanel.php";
	$id_place = intval($_GET['id']);
	$get_id = $_GET['id'];
	$page = intval($_GET['id']);

	if(is_array($get_id)){
		//конвертим массив в строку
		$arr_id = implode(",", $get_id);
	}else{
	   $arr_id = $get_id;
	}

	if (!isset($_SESSION['user']))
	{
	  header('Location: /login.php');
	  exit;
	}

	// Подключение к базе данных
	include "connection.php";

?>

<section class="section-table section-table-panel cid-r13PedKtlK" id="table1-4">

	<div class="section-side-panel">
		<div class="sidebar_btn">
			<svg class="ham hamRotate180 ham5" viewBox="0 0 100 100" width="55">
			  <path class="line top"
					d="m 30,33 h 40 c 0,0 8.5,-0.68551 8.5,10.375 0,8.292653 -6.122707,9.002293 -8.5,6.625 l -11.071429,-11.071429" />
			  <path class="line middle"
					d="m 70,50 h -40" />
			  <path class="line bottom"
					d="m 30,67 h 40 c 0,0 8.5,0.68551 8.5,-10.375 0,-8.292653 -6.122707,-9.002293 -8.5,-6.625 l -11.071429,11.071429" />
			</svg>
		</div>
		<div class="places-wrapper">
			<div class="place-item place-item-all">
				<div class="place-name-wrap">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" name="check" id="check_all" value="">
						<label class="custom-control-label" for="check_all"></label>
					</div>
					<div class="place-name"></div>
				</div>
			</div>
		<?php

			/*$res_devices = $mysqli->query("SELECT d.*, ev1.*
						   FROM devices d
						   JOIN eventlog ev1 ON (d.id_device = ev1.id_device)
						   LEFT OUTER JOIN eventlog ev2 ON (d.id_device = ev2.id_device AND
						   	(ev1.date < ev2.date OR ev1.date = ev2.date AND ev1.id_device < ev2.id_device))
						   WHERE ev2.id_device IS NULL AND ev1.id_place IN(".$arr_id.")");*/


			$res_devices = $mysqli->query("SELECT d.*, ev1.*
										   FROM devices d
										   INNER JOIN eventlog ev1 ON d.id_device = ev1.id_device
										   AND ev1.id_place IN(".$arr_id.") GROUP BY ev1.id_device");

			$arr_dev = array();
			$arr_device = array();

			while ($val_dev = $res_devices->fetch_assoc()){

				$arr_dev[$val_dev['id_place']][$val_dev['id_device']] = $val_dev['description'];
				$arr_device[] = $val_dev['id_device'];

			}

			$arr_dev_str = implode(",",$arr_device);

			//print_r($arr_dev);

			$res_places = $mysqli->query("SELECT * FROM places WHERE id_place IN(".$arr_id.") GROUP BY id_place", MYSQLI_USE_RESULT);

			while ($val_place = $res_places->fetch_assoc()){

				echo '<div class="place-item">'
					.'<div class="place-name-wrap">'
					.'<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input checkbox-obj checkbox-inp" name="check" id="check_'.$val_place['id_place'].'" value="'.$val_place['id_place'].'" checked>
						<label class="custom-control-label" for="check_'.$val_place['id_place'].'"></label>
					  </div>'
					.'<div class="place-name">'.$val_place['name'].'<i class="fa fa-angle-down fa-arrow fa-angle-up" aria-hidden="true"></i></div></div>'
					.'<div class="device-list">';

				foreach($arr_dev as $k=>$v){

					if($k == $val_place['id_place']){

						foreach($v as $kk=>$vv){
							echo '<div class="device-item"><a href="/more.php?id='.$kk.'">'.$vv.'</a></div>';
						}

					}

				}

				echo '</div></div>';

			}

		?>
		</div>
	</div>

	<div class="section-wrapper">
		<div id='map' class='map'>
			<div class="message-warn">
				<span class="message-close"></span>
				<div class="message-wrap"></div>
			</div>
			<div class="hint-latlng"></div>
		</div>
		<div class="navigation">
		<div class="navbar-points">
			<div class="point-item" >
				<span class="point-title" color-point="red">A</span>
				<input type="text" name="lat1" id="lat1" class="point-inp point-lat" color-point="red" title="широта" placeholder="широта" autocomplete="off">
				<input type="text" name="lng1" id="lng1" class="point-inp point-lng" color-point="red" title="долгота" placeholder="долгота" autocomplete="off">
				<span class="point-delete" title="Очистить"><i class="fa fa-trash" aria-hidden="true"></i></span>
			</div>
			<div class="point-item">
				<span class="point-title" color-point="blue">B</span>
				<input type="text" name="lat2" id="lat2" class="point-inp point-lat" color-point="blue" title="широта" placeholder="широта" autocomplete="off">
				<input type="text" name="lng2" id="lng2" class="point-inp point-lng" color-point="blue" title="долгота" placeholder="долгота" autocomplete="off">
				<span class="point-delete" title="Очистить"><i class="fa fa-trash" aria-hidden="true"></i></span>
			</div>
			<input type="hidden" name="img_name_upload" id="img_name_upload" value="">
			<!--div class="point-image">
				<input id="upimg_map" type="file" name="upimg_map" />
			</div-->
			<a id="open_modal" data-fancybox="image_upload_modal" data-src="#image_upload_modal" class="btn btn-sm btn-primary display-3" href="">Изображение</a>
		</div>
    	<div class="navbar-buttons">
			<a id="autosize" class="btn btn-sm btn-primary display-3"><?=LANG_AUTO_SCALE?></a>
		</div>
		</div>
  	<div class="container container-table container-panel-table">
  	<br>
	<h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2"><?=LANG_DEV_REG_OBJ?></h2>
	<div id="before-load">
		<i class="fa fa-spinner fa-spin"></i>
	</div>
	<div id="content2"></div>

	<script>
        function refresh() {

			var arrayChecked;

			if($('input.checkbox-obj:checked').length > 0){
				//собираем массив из id
				$('.checkbox-inp').each(function(){
					arrayChecked = $('input.checkbox-obj:checked').map(function(){ return this.value; }).get();
				});
			}else{
					arrayChecked = ['null'];
			}

			$.ajax({
				url: "panel_table.php",
				cache: false,
				type: "POST",
				async:true,
				data: {arr:arrayChecked},
			}).done(function(html){
				//выводим таблицу данных устройств на странице
				$("#content2").html(html);
			});

        }

        $(document).ready(function() {

			if($('input.checkbox-obj:checked').length > 0){
				$('.place-item-all').find('#check_all').prop('checked','checked');
			}

			if($('.place-item-all').find('#check_all').prop('checked')) {
				$('.place-item-all').find('.place-name').text("<?=LANG_UNSELECT_ALL?>");
			}else{
				$('.place-item-all').find('.place-name').text("<?=LANG_SELECT_ALL?>");
			}

			$('.place-item-all').find('#check_all').on('change',function(){
				if($('.place-item-all').find('#check_all').prop('checked')) {
					$('.place-item-all').find('.place-name').text("<?=LANG_UNSELECT_ALL?>");
					$('input.checkbox-obj').each(function(){
						$('input.checkbox-obj').prop('checked','checked');
					});
					refresh();
					test();
				}else{
					$('.place-item-all').find('.place-name').text("<?=LANG_SELECT_ALL?>");
					$('input.checkbox-obj').each(function(){
						$('input.checkbox-obj').prop('checked',false);
					});
					refresh();
					test();
				}
			});

			refresh();

			setInterval(function(){
				refresh();
				test();
			}, 10000);

			$('.checkbox-obj').on('change', function(){
				refresh();
				test();
			});

        });

    </script>

<?php

	$res = $mysqli->query("SELECT gps FROM eventlog WHERE gps <> 0 AND gps IS NOT NULL AND gps <> ''" , MYSQLI_USE_RESULT);

	while ($value = $res->fetch_assoc()) {
		$val_gps = $value['gps'];
		$last_gps = explode(',', $val_gps);
	}

?>

		</div>
	</div>
</section>
<?php include "footer.php";?>
<div class="fancybox-content modal-content" id="image_upload_modal">
	<div class="image-map">
		<div class="hint-latlng"></div>
		<div class="image-content"></div>
	</div>
	<div class="buttons-block">
		<div class="navbar-points">
			<div class="point-image">
				<div class="title-point-img">Загрузить изображение:</div>
				<input id="upimg" type="file" name="upimg" /><br>
				<a id="upload_img" class="btn btn-sm btn-primary display-3">Загрузить</a>
			</div>
			<div class="title-point-img">Указать координаты точек:</div>
			<div class="point-item-img" >
				<span class="point-title-img" color-point="red">A</span>
				<input type="text" name="img_lat1" id="img_lat1" class="point-inp-img point-lat-img" color-point="red" title="широта" autocomplete="off" placeholder="широта">
				<input type="text" name="img_lng1" id="img_lng1" class="point-inp-img point-lng-img" color-point="red" title="долгота" autocomplete="off" placeholder="долгота">
				<span class="point-delete" title="Очистить"><i class="fa fa-trash" aria-hidden="true"></i></span>
			</div>
			<div class="point-item-img">
				<span class="point-title-img" color-point="blue">B</span>
				<input type="text" name="img_lat2" id="img_lat2" class="point-inp-img point-lat-img" color-point="blue" title="широта" autocomplete="off" placeholder="широта">
				<input type="text" name="img_lng2" id="img_lng2" class="point-inp-img point-lng-img" color-point="blue" title="долгота" autocomplete="off" placeholder="долгота">
				<span class="point-delete" title="Очистить"><i class="fa fa-trash" aria-hidden="true"></i></span>
			</div>
			<a id="upload_map_img" class="btn btn-sm btn-primary display-3">Применить</a>
		</div>
	</div>
</div>
<script>
	$(window).load(function() {
	  $('#before-load').find('i').fadeOut().end().delay(400).fadeOut('slow');
	});
</script>

<?php include "scripts.php"; ?>
<script src="assets/datatables/jquery.data-tables.min.js"></script>
<script src="assets/datatables/data-tables.bootstrap4.min.js"></script>
<script src="js/jquery.touchSwipe.min.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/scripts.js"></script>
<script src="js/leaflet/leaflet-hash.js"></script>
<script src="js/jquery.fancybox.min.js"></script>
<script>
	"use strict"

	//объявляются переменные
	var geojson,
		metadata,
		marker = new L.Marker(),
		markerclusters,
		tileServer = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
		tileAttribution = 'Map data: <a href="http://openstreetmap.org">OSM</a>',
		rmax = 35, //Maximum radius for cluster pies
		//объявляется переменная карты
		//map = L.map('map', {
				//minZoom:10, maxZoom: 18
		//	}).setView([56.7717141, 54.1205757], 18);
		map = L.map('map').setView([<?php echo $val_gps; ?>], 4);

	/* Загрузка изображения */
	loadImage();
	loadLatLng();

	map.on('zoom', function(){
		imageInit();
		//console.log('1');
	});

	var image_map = $('.image-content'),
		pinp_img = $('.point-inp-img'),
		ptitle_img = $('.point-title-img');

	pinp_img.on('mouseup', function(e){
		
		if(pinp_img.on('focus')){
			
			var color_point = $(this).attr('color-point');
			
			pinp_img.removeClass('focus');
			ptitle_img.removeClass('focus');
			$(this).parent('.point-item-img').find('.point-inp-img').addClass('focus');
			$(this).parent('.point-item-img').find('.point-title-img').addClass('focus');
			$('.hint-latlng').addClass('hint-show');
			
		}
		
	});

	//наведение курсора над слоем изображения
	image_map.on('mousemove', function(e) {
		var parentOffset = $(this).parent().position(),
			posX = e.pageX - parentOffset.left,
			posY = e.pageY - parentOffset.top;
		$('.image-map').find('.hint-latlng').text(posX+', '+posY);
	});

	//обработка нажатия на кнопку "Загрузить"
	$('#upload_img').on('click', function() {
		var file_data = $('#upimg').prop('files')[0];
		var form_data = new FormData();
		form_data.append('file', file_data);
		//$('#img_name_upload').val(file_data);
		$.ajax({
			url: 'image_upload.php',
			dataType: 'text',
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function(e){
				loadImage();
			}
		});
	});

	//обработка нажатия на слой изображения
	image_map.on('click', function(e){
		if(pinp_img.hasClass('focus')){
			var parentOffset = $(this).parent().offset(),
			posX = e.pageX - parentOffset.left,
			posY = e.pageY - parentOffset.top;
			$('.point-inp-img.point-lat-img.focus').val(posY);
			$('.point-inp-img.point-lng-img.focus').val(posX);
		}
	});
	
	var pinp = $('.point-inp');
	var ptitle = $('.point-title');
	var map_cont = $('#map');
	
	//иконки маркеров
	var redIcon = L.icon({
		iconUrl: '/js/leaflet/images/marker-icon-2-red.png',
		iconSize: [20, 33],
		iconAnchor: [10 ,33],
		shadowUrl: '/js/leaflet/images/marker-shadow.png',
		shadowSize: [41, 41],
		shadowAnchor: [13, 41]
	});
	var blueIcon = L.icon({
		iconUrl: '/js/leaflet/images/marker-icon.png',
		iconSize: [20, 33],
		iconAnchor: [10 ,33],
		shadowUrl: '/js/leaflet/images/marker-shadow.png',
		shadowSize: [41, 41],
		shadowAnchor: [13, 41]
	});
	var greenIcon = L.icon({
		iconUrl: '/js/leaflet/images/marker-icon-3.png',
		iconSize: [20, 33],
		iconAnchor: [10 ,33],
		shadowUrl: '/js/leaflet/images/marker-shadow.png',
		shadowSize: [41, 41],
		shadowAnchor: [13, 41]
	});
	var testIcon = L.icon({
		iconUrl: '/js/leaflet/images/marker-icon-2.png',
		iconSize: [20, 33],
		iconAnchor: [10 ,33],
		shadowUrl: '/js/leaflet/images/marker-shadow.png',
		shadowSize: [41, 41],
		shadowAnchor: [13, 41]
	});
	var new_red_marker, new_blue_marker, new_green_marker;
	var new_marker, new_b_marker;
	
	//наведение курсора над полями координат
	pinp.on('mouseup', function(e){
		
		if(pinp.on('focus')){
			
			var color_point = $(this).attr('color-point');
			
			pinp.removeClass('focus');
			ptitle.removeClass('focus');
			$(this).parent('.point-item').find('.point-inp').addClass('focus');
			$(this).parent('.point-item').find('.point-title').addClass('focus');
			$('#map').attr('cursor-map', color_point);
			$('#map').find('.hint-latlng').addClass('hint-show');
			
		}
		
	});
	
	//наведение курсора над картой
	map.on('mousemove', function(e) {
		$('#map').find('.hint-latlng').text(e.latlng.lat+', '+e.latlng.lng);
	});
	
	//обработка нажатия на карте
	map.on('click', function(e){
		if(pinp.hasClass('focus')){
			//console.log(e.latlng.lat);
			$('.point-inp.point-lat.focus').val(e.latlng.lat);
			$('.point-inp.point-lng.focus').val(e.latlng.lng);
			if($('.point-inp.focus').is('[color-point=red]')){
				if(new_red_marker) map.removeLayer(new_red_marker);
				new_red_marker = new L.marker(e.latlng, {icon: redIcon}).addTo(map);
				//var red_marker_id = new_red_marker._leaflet_id;
				//new_red_marker._icon.id = 'red_marker_' + red_marker_id;
			}
			if($('.point-inp.focus').is('[color-point=blue]')){
				if(new_blue_marker) map.removeLayer(new_blue_marker);
				new_blue_marker = new L.marker(e.latlng, {icon: blueIcon}).addTo(map);
			}
			if($('.point-inp.focus').is('[color-point=green]')){
				if(new_green_marker) map.removeLayer(new_green_marker);
				new_green_marker = new L.marker(e.latlng, {icon: greenIcon}).addTo(map);
			}
		}
	});

	//очистка полей координат
	$('.point-delete').click(function(){
		$(this).parent().find('input[type=text]').val('');
	});
	
	//обработка нажатия на странице
	$(document).click(function(e){
		
		if(!pinp_img.is(e.target) && pinp_img.has(e.target).length === 0){
			pinp_img.removeClass('focus');
			pinp_img.blur();
			ptitle_img.removeClass('focus');
			$('.image-map').find('.hint-latlng').removeClass('hint-show');
		}
		
		if(!pinp.is(e.target) && pinp.has(e.target).length === 0){
			pinp.removeClass('focus');
			ptitle.removeClass('focus');
			$('#map').removeAttr('cursor-map');
			$('#map').find('.hint-latlng').removeClass('hint-show');
		}
		
	});

	//обработка нажатия кнопки "Применить"
	$('#upload_map_img').on('click', function(){
		
		var img_map = $('.img-map'),
			img_lat1 = $('#img_lat1'),
			img_lat2 = $('#img_lat2'),
			img_lng1 = $('#img_lng1'),
			img_lng2 = $('#img_lng2'),
			img_width = parseInt(img_map.width()),
			img_height = parseInt(img_map.height());
			
		var lat_dist = img_lat1.val() - img_lat2.val();
		var lng_dist = img_lng1.val() - img_lng2.val();
			
		var img_data = new FormData();
		
		img_data.append('img_width', img_width);
		img_data.append('img_height', img_height);
		img_data.append('img_lat1', img_lat1.val());
		img_data.append('img_lat2', img_lat2.val());
		img_data.append('img_lng1', img_lng1.val());
		img_data.append('img_lng2', img_lng2.val());
		img_data.append('lat_dist', convertMinus(lat_dist));
		img_data.append('lng_dist', convertMinus(lng_dist));
		
		$.ajax({
			url: 'image_upload_param.php',
			dataType: 'text',
			cache: false,
			contentType: false,
			processData: false,
			data: img_data,
			type: 'post',
			success: function(e){
				//alert('success');
			}
		});
		
		var lat1 = $('#lat1').val(),
			lat2 = $('#lat2').val(),
			lng1 = $('#lng1').val(),
			lng2 = $('#lng2').val();
		
		var map_data = new FormData();

		var file_data_img;

		if($('#upimg').prop('files')[0]!=undefined){
			file_data_img = $('#upimg').prop('files')[0];
			map_data.append('file', file_data_img);
		}else{
			file_data_img = $('#img_name_upload').val();
			map_data.append('img_name_upload', file_data_img);
		}

		map_data.append('lat1', lat1);
		map_data.append('lat2', lat2);
		map_data.append('lng1', lng1);
		map_data.append('lng2', lng2);
		
		$.ajax({
			url: 'upload.php',
			dataType: 'text',
			cache: false,
			contentType: false,
			processData: false,
			data: map_data,
			type: 'post',
			success: function(php_script_response){
				window.location.reload(true);
				//init();
			}
		});
		
	});

	function convertMinus(num){
		
		var num;
		
		if(num < 0){
			num = num*(-1);
			return num;
		}else{
			return num;
		}
		
	}

	//функция округления
	var rounded = function(number){
		return +number.toFixed(5);
	}

	//функция отображения изображения
	function loadImage(){
		
		$.ajax({
			url: '/uploads/data_image_file.json',
			dataType: 'json',
			success: function (data) {
				var img_cont = $('.image-content');
				var image_block = '<img class="img-map" src="/uploads/'+data.fileName+'">';
				if(img_cont.html() == ""){
					img_cont.html(image_block);
				}else{
					img_cont.empty();
					img_cont.html(image_block);
				}
			}
		});
		
	}

	//функция отображения точек на изображении
	function loadLatLng() {
		
		$.ajax({
			url: '/uploads/data_image_param.json',
			dataType: 'json',
			success: function (data) {
				
				$('#img_lat1').val(data.lat1);
				$('#img_lat2').val(data.lat2);
				$('#img_lng1').val(data.lng1);
				$('#img_lng2').val(data.lng2);
				
				var marker_image1 = '<div class="marker-image marker1"></div>';
				image_map.append(marker_image1);
				$('.marker-image.marker1').css({top:data.lat1+'px', left:data.lng1+'px'});
				
				var marker_image2 = '<div class="marker-image marker2"></div>';
				image_map.append(marker_image2);
				$('.marker-image.marker2').css({top:data.lat2+'px', left:data.lng2+'px'});
				
				var marker1_left = $('.marker-image.marker1').offset().left;
				var marker2_left = $('.marker-image.marker2').offset().left;
				
			}
		});
		
	}

	function defineFeature(feature, latlng) {
	  	var categoryVal = feature.properties.color,
	    	iconVal = feature.properties.color,
			latlng = feature.geometry.coordinates;
	    var myClass = 'marker category-'+categoryVal+' icon-'+iconVal;
	    var myIcon = L.divIcon({
	        className: myClass,
	        iconSize:null
	    });
	    return L.marker(latlng, {icon: myIcon, title: feature.properties.hintContent });
	}

	function defineFeaturePopup(feature, layer) {
		var popupContent = feature.properties.balloonContent;
	  	layer.bindPopup(popupContent,{offset: L.point(1,-2)});
		layer.on('click', function(){
			var max_id = setInterval(function () {});
			while (max_id--) {
				clearInterval(max_id);
			}
		});
	}

	//создание кластера маркеров
	function defineClusterIcon(cluster) {

	    var children = cluster.getAllChildMarkers(),
	        n = children.length, //Get number of markers in cluster
	        strokeWidth = 1, //Set clusterpie stroke width
	        r = rmax-2*strokeWidth-(n<10?12:n<100?8:n<1000?4:0), //Calculate clusterpie radius...
	        iconDim = (r+strokeWidth)*2, //...and divIcon dimensions (leaflet really want to know the size)
	        data = d3.nest() //Build a dataset for the pie chart
	          .key(function(d) { return d.feature.properties.color; })
	          .entries(children, d3.map),
	        //bake some svg markup
	        html = bakeThePie({data: data,
	                            valueFunc: function(d){return d.values.length;},
	                            strokeWidth: 1,
	                            outerRadius: r,
	                            innerRadius: r-10,
	                            pieClass: 'cluster-pie',
	                            pieLabel: n,
	                            pieLabelClass: 'marker-cluster-pie-label',
	                            pathClassFunc: function(d){return "category-"+d.data.key;},
															//pathClassFunc: function(d){return "category-1";},
	                            pathTitleFunc: function(d){
													var titleContent;
													for(var i=0; i<d.value; i++){
														if(i > 0){
															titleContent += '\n' + d.data.values[i].feature.properties.hintContent;
														}else{
															titleContent = d.data.values[i].feature.properties.hintContent;
														}
													}
													return titleContent;
												}
	                          }),
	        //Create a new divIcon and assign the svg markup to the html property
	        myIcon = new L.DivIcon({
	            html: html,
	            className: 'marker-cluster marker-cluster-' + cluster._leaflet_id,
	            iconSize: new L.Point(iconDim, iconDim)
	        });

	    return myIcon;
		
	}

	/*function that generates a svg markup for the pie chart*/
	function bakeThePie(options) {
	    /*data and valueFunc are required*/
	    if (!options.data || !options.valueFunc) {
	        return '';
	    }
	    var data = options.data,
	        valueFunc = options.valueFunc,
	        r = options.outerRadius?options.outerRadius:28, //Default outer radius = 28px
	        rInner = options.innerRadius?options.innerRadius:r-10, //Default inner radius = r-10
	        strokeWidth = options.strokeWidth?options.strokeWidth:1, //Default stroke is 1
	        pathClassFunc = options.pathClassFunc?options.pathClassFunc:function(){return '';}, //Class for each path
	        pathTitleFunc = options.pathTitleFunc?options.pathTitleFunc:function(){return '';}, //Title for each path
	        pieClass = options.pieClass?options.pieClass:'marker-cluster-pie', //Class for the whole pie
	        pieLabel = options.pieLabel?options.pieLabel:d3.sum(data,valueFunc), //Label for the whole pie
	        pieLabelClass = options.pieLabelClass?options.pieLabelClass:'marker-cluster-pie-label',//Class for the pie label

	        origo = (r+strokeWidth), //Center coordinate
	        w = origo*2, //width and height of the svg element
	        h = w,
	        donut = d3.layout.pie(),
	        arc = d3.svg.arc().innerRadius(rInner).outerRadius(r);

	    //Create an svg element
	    var svg = document.createElementNS(d3.ns.prefix.svg, 'svg');
	    //Create the pie chart
	    var vis = d3.select(svg)
	        .data([data])
	        .attr('class', pieClass)
	        .attr('width', w)
	        .attr('height', h);

	    var arcs = vis.selectAll('g.arc')
	        .data(donut.value(valueFunc))
	        .enter().append('svg:g')
	        .attr('class', 'arc')
	        .attr('transform', 'translate(' + origo + ',' + origo + ')');

	    arcs.append('svg:path')
	        .attr('class', pathClassFunc)
	        .attr('stroke-width', strokeWidth)
	        .attr('d', arc)
	        .append('svg:title')
	          .text(pathTitleFunc);

	    vis.append('text')
	        .attr('x',origo)
	        .attr('y',origo)
	        .attr('class', pieLabelClass)
	        .attr('text-anchor', 'middle')
	        //.attr('dominant-baseline', 'central')
	        /*IE doesn't seem to support dominant-baseline, but setting dy to .3em does the trick*/
	        .attr('dy','.3em')
	        .text(pieLabel);
	    //Return the svg-markup rather than the actual element
	    return serializeXmlNode(svg);
	}

	/*Function for generating a legend with the same categories as in the clusterPie*/
	function renderLegend() {

		var data = d3.entries(),
	      legenddiv = d3.select('body').append('div')
	        .attr('id','legend');

	    var heading = legenddiv.append('div')
	        .classed('legendheading', true);

	    var legenditems = legenddiv.selectAll('.legenditem')
	        .data(data);

	    legenditems
	        .enter()
	        .append('div')
	        .attr('class',function(d){return 'category-'+d.key;})
	        .classed({'legenditem': true})
	        .text(function(d){return d.value;});
		
	}

	/*Helper function*/
	function serializeXmlNode(xmlNode) {
	    if (typeof window.XMLSerializer != "undefined") {
	        return (new window.XMLSerializer()).serializeToString(xmlNode);
	    } else if (typeof xmlNode.xml != "undefined") {
	        return xmlNode.xml;
	    }
	    return "";
	}

	//обработка закрытия попапа
	map.on('popupclose', function(){

		setInterval(function(){
			refresh();
			test();
		}, 10000);
		
	});

	//функция настройки изображения на карте
	function imageInit() {

		$.ajax({
			url: '/uploads/data_image_param.json',
			dataType: 'json',
			success: function (data) {

				var lat1 = $('#lat1').val(),
					lat2 = $('#lat2').val(),
					lng1 = $('#lng1').val(),
					lng2 = $('#lng2').val(),
					leaf_img_layer = $('.leaflet-image-layer'),
					image_layer_height = $('.leaflet-image-layer').height(),
					image_layer_width = $('.leaflet-image-layer').width();
				
				var x1 = 0,
					y1 = 0,
					px1 = data.lng1,
					py1 = data.lat1,
					px2 = data.lng2,
					py2 = data.lat2,
					img_width = data.imgWidth,
					img_height = data.imgHeight,
					sign;

				var dpx = parseInt(px1) - parseInt(px2);
				var dpy = parseInt(py1) - parseInt(py2);
				var dpgx = lng1 - lng2;
				var dpgy = lat1 - lat2;

				//находим координаты для точек
				var gx1 = (0          - px1) * dpgx / dpx + parseFloat(lng1);
				var gx3 = (img_width  - px1) * dpgx / dpx + parseFloat(lng1);
				var gy1 = (0          - py1) * dpgy / dpy + parseFloat(lat1);
				var gy3 = (img_height - py1) * dpgy / dpy + parseFloat(lat1);
				var gx2 = gx3;
				var gy2 = gy1;

				//****
				//B - Big image (из data_image_param.json)
				//M - Middle image (изображение, которое растягивается по точкам при загрузке карты)
				//S - Small image (изображение, принимающее требуемый вид с учетом исходных пропорций)
				//****

				//угол по точкам на карте
				var angle_ab_map = getAngle(lng1, lat1, lng2, lat2);

				var point_ax, point_ay, point_bx, point_by;

				//координаты точек в зависимости от угла
				if(angle_ab_map > 0 && angle_ab_map <= 90 && lng1 < lng2){
					point_ax = 0;
					point_ay = image_layer_height;
					point_bx = image_layer_width;
					point_by = 0;
				}else if(angle_ab_map < 0 && angle_ab_map >= -90 && lng1 < lng2){
					point_ax = 0;
					point_ay = 0;
					point_bx = image_layer_width;
					point_by = image_layer_height;
				}else if(angle_ab_map > 0 && angle_ab_map <= 90 && lng1 > lng2){
					point_ax = image_layer_width;
					point_ay = 0;
					point_bx = 0;
					point_by = image_layer_height;
				}else if(angle_ab_map < 0 && angle_ab_map >= -90 && lng1 > lng2){
					point_ax = image_layer_width;
					point_ay = image_layer_height;
					point_bx = 0;
					point_by = 0;
				}

				//длина отрезка между точками A и B изображения M
				var dist_img_ab = getDistPoint(point_ax, point_ay, point_bx, point_by);

				//процент разности ширины изображения B и М
				var per_width_bm = getNumPer(img_width, image_layer_width);

				//процент разности высоты изображения B и М
				var per_height_bm = getNumPer(img_height, image_layer_height);

				//расчет высоты изображения
				var img_m_height = image_layer_height - (image_layer_height*per_height_bm)/100;

				//console.log(per_width_bm);

				var px1_m = px1*1 - (px1*per_width_bm)/100;
				var px2_m = px2*1 - (px2*per_width_bm)/100;
				
				var py1_m = py1*1 - (py1*per_height_bm)/100;
				var py2_m = py2*1 - (py2*per_height_bm)/100;

				//длина отрезка между точек изображения M
				var dist_img_m = getDistPoint(px1_m, py1_m, px2_m, py2_m);

				//console.log(dist_img_m);
				
				//задаем новую высоту изображения S
				//leaf_img_layer.height(img_m_height);
				leaf_img_layer.height('auto');

				//новая высота изображения S
				var image_layer_height_s = leaf_img_layer.height();

				//разность высоты M и S
				var ms_height_diff = image_layer_height_s - image_layer_height;

				//процент разности высоты изображения B и S
				var per_height_bs = getNumPer(img_height, image_layer_height_s);

				var px1_s = px1*1 - (px1*per_width_bm)/100;
				var px2_s = px2*1 - (px2*per_width_bm)/100;
				
				var py1_s = py1*1 - (py1*per_height_bs)/100;
				var py2_s = py2*1 - (py2*per_height_bs)/100;

				//длина отрезка между точек изображения S
				var dist_img_s = getDistPoint(px1_s, py1_s, px2_s, py2_s);

				//процент разности между точками изображения M и S
				var diff_per_ms = getDiffPer(dist_img_ab, dist_img_s);

				var px1_s2 = px1_s*1 + (px1_s*diff_per_ms)/100;
				var px2_s2 = px2_s*1 + (px2_s*diff_per_ms)/100;
				
				var py1_s2 = py1_s*1 + (py1_s*diff_per_ms)/100;
				var py2_s2 = py2_s*1 + (py2_s*diff_per_ms)/100;

				var px1_s2_pos = (px1_m - px1_s2) + (point_ax - px1_s2);
				var py1_s2_pos = (py1_m - py1_s2) + (point_ay - py1_s2);
				
				//отступ сверху
				var py1_s2_m = point_ay - py1_s2;
				
				//отступ слева
				var px1_s2_m;

				if(lng1 > lng2){
					px1_s2_m = point_ax - px1_s2;
				}else{
					px1_s2_m = px1_s2*(-1);
				}

				//новая ширина изображения S
				var image_layer_width_s = image_layer_width + (image_layer_width*diff_per_ms)/100;

				//угол точек изображения S2
				var img_s_angle = getAngle(px1_s2, py1_s2, px2_s2, py2_s2);
				//угол точек на карте
				var map_angle = getAngle(point_ax, point_ay, point_bx, point_by);

				var angle;

				//разность углов
				if (lng2 < lng1) {
					angle = 180 + (map_angle - img_s_angle);
				} else {
					angle = map_angle - img_s_angle;
				}

				//console.log(px1_s2);

				//задаем css настройки для изображения
				var img_opt = leaf_img_layer.attr('style');
				var img_transform = leaf_img_layer[0].style.transform + ' rotate('+angle+'deg)';
				var img_origin = px1_s2+'px '+py1_s2+'px';
				leaf_img_layer.css({
					'transform':img_transform,
					'width':image_layer_width_s,
					'transform-origin':img_origin,
					'top':py1_s2_m,
					'left':px1_s2_m
				});

				//проверочные точки
				if(new_marker) map.removeLayer(new_marker);
				new_marker = new L.marker([lat1, lng1], {icon: redIcon}).addTo(map);
				if(new_b_marker) map.removeLayer(new_b_marker);
				new_b_marker = new L.marker([lat2, lng2], {icon: blueIcon}).addTo(map);
				//var new_g_marker = new L.marker([lat3, lng3], {icon: greenIcon}).addTo(map);

				//нахождение длины отрезка от точки до точки
				function getDistPoint(xa, ya, xb, yb) {	
					return Math.sqrt((xb - xa)*(xb - xa) + (yb - ya)*(yb - ya));
				}
				
				//функция расчета процента разности
				function getDiffPer(a, b) {
					return (a/b-1)*100;
				}
				
				//функция расчета угла между точками
				function getAngle(ax, ay, bx, by) {
					return Math.atan((by - ay)/(bx - ax))*180/Math.PI;
				}

				//функция расчета процента разности
				function getNumPer(num, rnum) {
					
					var summPerNum, perSumm, nums, summ;
					
					nums = num - rnum;
					summPerNum = num / 100;
					perSumm = 1/summPerNum;
					summ = nums * perSumm;
					
					//проверка на отрицательное число
					if(summ < 0){
						summ = summ*(-1);
						return summ;
					}else{
						return summ;
					}
					
				}

			}
		});

	}//function imageInit()

	function init() {

		$.ajax({
			url: '/uploads/data_image_file.json',
			dataType: 'json',
			success: function(data){
				if(data.fileName)
					$('#img_name_upload').val(data.fileName);
			}
		});
		
		$.ajax({
			url: '/uploads/data_file.json',
			dataType: 'json',
			success: function (data) {
				$('#lat1').val(data.lat1);
				$('#lat2').val(data.lat2);
				$('#lng1').val(data.lng1);
				$('#lng2').val(data.lng2);

				var bounds = [[data.lat1, data.lng1], [data.lat2, data.lng2], [data.lat2, data.lng2]];

				var image_layer = L.imageOverlay('/uploads/'+data.fileName, bounds, {opacity:0.4});
				map.removeLayer(image_layer);
				image_layer.addTo(map);
			}
		});
		
		L.tileLayer(tileServer, {attribution: tileAttribution,  maxZoom: 18}).addTo(map);

		var array_check_place = [];

		$('.checkbox-inp').each(function(){
			array_check_place = $('input.checkbox-obj:checked').map(function(){ return this.value; }).get();
		});

		if(array_check_place.length == 0){
			array_check_place = ['null'];
		}

		//если кластер маркеров существует, то очищается, чтобы не дублировать
		if(markerclusters){
			markerclusters.clearLayers();
			markerclusters.remove();
		}

		$('#legend').remove();

		//создается кластер маркеров
		markerclusters = L.markerClusterGroup({
			maxClusterRadius: 2*rmax,
			spiderfyOnMaxZoom: false,
			zoomToBoundsOnClick: false,
			iconCreateFunction: defineClusterIcon //this is where the magic happens
		});

		//обработка нажатия на кластер
		markerclusters.on('clusterclick', function(a){
			var popUpText = '<ul>';
			for(var i=0; i < a.layer.getAllChildMarkers().length; i++){
				popUpText += '<li>'+a.layer.getAllChildMarkers()[i]._popup._content+'</li>';
			}
			popUpText += '</ul>';
			var popup = L.popup().setLatLng([a.layer._cLatLng.lat, a.layer._cLatLng.lng]).setContent(popUpText).openOn(map);
			//очищаем все счетчики
			var max_id = setInterval(function () {});
			while (max_id--) {
				clearInterval(max_id);
			}
		});

		$.ajax({
			url: 'panel_json.php',
			type: 'post',
			dataType: 'json',
			data: {arrp:array_check_place}
		}).done(function(data_json) {

		  //and the empty markercluster layer
		  //добавление кластеров
		  map.addLayer(markerclusters);

			data_json = JSON.parse(data_json);

			metadata = data_json.features;

			var markers = L.geoJson(data_json, {
				pointToLayer: defineFeature,
				onEachFeature: defineFeaturePopup
			});

			//добавление маркеров в слой кластера
			markerclusters.addLayer(markers);

			//автомасштаб
			document.getElementById('autosize').onclick = function() {
				map.fitBounds(markers.getBounds());
			};

			renderLegend();

		});

		var hash = new L.Hash(map);

	} //function init()

	//функция удаления дубликатов из массива
	var getUnique = function (arru) {
		var i = 0,
		current,
		length = arru.length,
		unique = [];
		for (; i < length; i++) {
			current = arru[i];
			if (!~unique.indexOf(current)) {
				unique.push(current);
			}
		}
		return unique;
	};

	//функция добавления устройств в cookie
	function getArrayItem(arr_item) {

		var arr = [];
		var arr_tmp = [];
		var arr_snd = [];

		//for(var p=0; p < arr_item.features.length; p++){
		for(var p=0; p < arr_item.messFeatures.length; p++){

			//var item = arr_item.features[p].properties.balloonContent;
			var item = arr_item.messFeatures[p].messProp.messContent;

			//if(arr_item.features[p].properties.color == 'red'){
			if(arr_item.messFeatures[p].messProp.mess == 'yes'){

				if($.cookie('arr') != null){

					var arr_cookie = $.parseJSON($.cookie('arr'));
					var arr_cookie_id = arr_cookie.indexOf(item, 0);

					for(var a=0; a < arr_cookie.length; a++){
						if(arr_cookie_id != -1){
								arr_tmp.push(arr_cookie[a]);
						}
					}

					if(arr_cookie_id == -1){
							arr_tmp.push(item);
					}

					arr_snd = getUnique(arr_tmp);

				}else{
					arr.push(item);
				}

			}

		}

		if($.cookie('arr') != null){
			$.cookie('arr', JSON.stringify(arr_snd));
		}else{
			$.cookie('arr', JSON.stringify(arr));
		}

	}


	function test(){

		var array_check_place = [];

		$('.checkbox-inp').each(function(){
			array_check_place = $('input.checkbox-obj:checked').map(function(){ return this.value; }).get();
		});

		if(array_check_place.length == 0){
			array_check_place = ['null'];
		}

		if(markerclusters){
			markerclusters.clearLayers();
			markerclusters.remove();
		}

		$('#legend').remove();

		//создается кластер маркеров
		markerclusters = L.markerClusterGroup({
			maxClusterRadius: 2*rmax,
			spiderfyOnMaxZoom: false,
			zoomToBoundsOnClick: false,
			iconCreateFunction: defineClusterIcon //this is where the magic happens
		});

		//обработка нажатия на кластер
		markerclusters.on('clusterclick', function(a){
				var popUpText = '<ul>';
				for(var i=0; i < a.layer.getAllChildMarkers().length; i++){
					popUpText += '<li>'+a.layer.getAllChildMarkers()[i]._popup._content+'</li>';
				}
				popUpText += '</ul>';
				var popup = L.popup().setLatLng([a.layer._cLatLng.lat, a.layer._cLatLng.lng]).setContent(popUpText).openOn(map);
				//очищаем все счетчики
				var max_id = setInterval(function () {});
				while (max_id--) {
					clearInterval(max_id);
				}
		});

		$.ajax({
			url: 'panel_json.php',
			type: 'post',
			dataType: 'json',
			data: {arrp:array_check_place}
		}).done(function(data_json) {

			//добавление кластеров
			map.addLayer(markerclusters);

			data_json = JSON.parse(data_json);

			metadata = data_json.features;

			//console.log(data_json.messFeatures);

			getArrayItem(data_json);

			var mess_warn = '';

			var arr_devices = $.parseJSON($.cookie('arr'));
			//console.log(arr_devices);

			if(arr_devices != null){
				for(var d=0; d<arr_devices.length; d++){
					mess_warn += '<p>'+arr_devices[d]+'</p>';
					//console.log(arr_devices[d]);
				}
			}

			if(mess_warn){
				$('.message-warn').find('.message-wrap').html(mess_warn);
				$('.message-warn').fadeIn(300);
			}

			var markers = L.geoJson(data_json, {
				pointToLayer: defineFeature,
				onEachFeature: defineFeaturePopup
			});

			markerclusters.addLayer(markers);
			//map.fitBounds(markers.getBounds());

			renderLegend();

		});

	}

	$('.message-close').click(function(){
		$('.message-warn').fadeOut(300);
		setTimeout(function(){
			$('.message-warn').find('.message-wrap').empty();
		}, 500);
		$.removeCookie('arr');
	});

	//init();

</script>
</body>
</html>
