<?php
	session_start();
/*
 * Страница списка компаний
 * © Эрис
*/
	include "connection.php";
	include "localization.php";
	require_once "class/head.class.php";
	$head = new HeadPage(); //класс HEAD страницы

	if (isset($_SESSION['user'])){
		$id_user = $_SESSION['user'];

		$table = $mysqli->query("SELECT * FROM admin_role WHERE id_admin = ".$id_user."");

		while($val = $table->fetch_assoc()){
			$id_role = $val['id_role'];
		}
	}

	if (!isset($_SESSION['user']) || $id_role != 1)
	{
	  header('Location: /login.php');
	  exit;
	}

?>
<!DOCTYPE>
<html>
<head>
	<?php echo $head->getHead(LANG_LIST_COMPANY); ?>
</head>
<body>
	<!-- header -->
	<?php include "headpanel.php"; ?>
	<!-- end header -->
	<section class="section-table cid-r13PedKtlK" id="table1-5">
	<div class="tab-pad80">
	  <div class="container container-table">
		<h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2"><?=LANG_LIST_COMPANY?></h2>
			<div class="container scroll">
			  <table class="table isSearch table-panel" cellspacing="0">
				<thead>
				  <tr class="table-heads">
					  <th class="head-item mbr-fonts-style display-7"><?=LANG_NAME_COMPANY?></th>
					  <th class="head-item mbr-fonts-style display-7"><?=LANG_ADDR?></th>
					  <th class="head-item mbr-fonts-style display-7"><?=LANG_DESCRIPTION?></th>
					  <th class="head-item mbr-fonts-style display-7"><?=LANG_CHANGE?></th>
				  </tr>
				</thead>
				<tbody>
		<?php

		$edit_com = intval($_GET['editcom']);

		// вывод списка компаний
		$res = $mysqli->query("SELECT * FROM company", MYSQLI_USE_RESULT);
		while ($value = $res->fetch_assoc())
		{
			echo '<tr>
					<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_NAME_COMPANY.'">'.$value['name'].'</td>
					<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_ADDR.'">'.$value['adress'].'</td>';
			echo '<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_DESCRIPTION.'">'.$value['description'].'</td>
					<td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANGE.'">
						<a href="/company_set.php?editcom='.$value['id_company'].'" class="editcom" data-id="'.$value['id_company'].'">'.LANG_EDIT.'</a>
					</td>';
			echo '</tr>';
		}

		?>
				</tbody>
			</table>
			<div class="panel-view-btn input-group-btn">
				<a href="/company_set.php" class="add-object btn btn-primary btn-form display-4">[+] <?=LANG_ADD_COMPANY?></a>
			</div>
			</div>
		</div>
	</div>
	</section>
	<!-- footer -->
	<?php include "footer.php"; ?>
	<!-- end footer -->
	<?php include "scripts.php"; ?>
	<script src="js/post_new_company.js"></script>
</body>
</html>
