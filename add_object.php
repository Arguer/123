<?php
session_start();
/*
 * Страница добавления подразделения
 * © Эрис
*/

include "localization.php";

require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы
?>
<html>
<head>
	<?php echo $head->getHead(LANG_OBJECTS, LANG_OBJECTS_CONTENT); ?>
	<!--script type="text/javascript" src="js/jquery.js"></script-->
</head>
<body>
<?php include "headpanel.php";

	if (isset($_SESSION['user'])){
		$id_user = $_SESSION['user'];
	}

	$table = $mysqli->query("SELECT * FROM admin_role WHERE id_admin = ".$id_user."");

	while($val = $table->fetch_assoc()){
		$id_role = $val['id_role'];
	}

	if (!isset($_SESSION['user']) || ($id_role != 1 && $id_role != 2))
	{
	  header('Location: /login.php');
	  exit;
	}

	include "connection.php";

	$edit_place = intval($_GET['edt_place']);

	if(isset($_POST['key'])){
		$key  = $_POST['key'];
	}
	if(isset($_POST['description'])){
		$desc = $_POST['description'];
	}
	if(isset($_POST['id'])){
		$id = intval($_POST['id']);
	}

	if(isset($_POST['cancel'])){
		header('Location: /object.php');
		exit;
	}

	$redir = intval($_GET['redir']);

	//$id_user = $_SESSION['user'];
	$res = $mysqli->query("SELECT * FROM admin_places INNER JOIN places ON admin_places.id_place = places.id_place WHERE admin_places.id_admin = ".$id_user , MYSQLI_USE_RESULT);

	while ($value = $res->fetch_assoc()){
		if ($value['id_place'] == $redir){
			$fname = $value['name'];
			$flogin = $value['login'];
			$fpassword = $value['password'];
			$fdescript = $value['description'];
		}
	}

?>
	<section class="mbr-section form1 cid-r3l5xi9p39 mbr-parallax-background" id="form1-7">
		<div class="container">
			<div class="row justify-content-center">
				<div class="title col-12 col-lg-8">
					<br>
					<h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2">
					<?php
					if($redir > 0) {
						echo LANG_EDIT_OBJ;
					}else
						echo LANG_ADD_OBJ;
					?>
					</h2>
					<h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5"><?=LANG_FILL_FIELDS_OBJ?></h3>
					<div class="mbr-block-title"></div>
				</div>
			</div>
			<form class="mbr-form" id="send_form" action="object.php" method="post">
				<input type="hidden" name="id_admin" value="<?php echo $_SESSION['user']; ?>" id="iadm">
				<div class="row row-sm-offset">
					<div class="col-md-4 multi-horizontal" data-for="name">
						<div class="form-group">
							<label class="form-control-label mbr-fonts-style display-7"><?=LANG_NAME?>*</label>
							<input type="text" class="form-control" name="name" maxlength="150" id="inam" value = "<?php echo $fname ?>">
						</div>
					</div>
				</div>
				<div class="row row-sm-offset">
					<div class="col-md-4 multi-horizontal" data-for="login">
						<div class="form-group">
							<label class="form-control-label mbr-fonts-style display-7"><?=LANG_LOGIN?>*&nbsp;</label>
							<input type="text" class="form-control" name="login"  id="ilog"  value = "<?php echo $flogin ?>">
						</div>
					</div>
					<div class="col-md-4 multi-horizontal" data-for="password">
						<div class="form-group">
							<label class="form-control-label mbr-fonts-style display-7"><?=LANG_PASS?>*</label>
							<input type="password" class="form-control" name="password" id="ipas"  value = "<?php echo $fpassword ?>">
						</div>
					</div>
					<div class="col-md-4 multi-horizontal" data-for="password">
						<div class="form-group">
							<label class="form-control-label mbr-fonts-style display-7"><?=LANG_PASS_CONF?>*</label>
							<input type="password" class="form-control" name="password2" id="ipas2"  value = "<?php echo $fpassword ?>">
						</div>
					</div>
				</div>
				<div class="form-group" data-for="message">
					<label class="form-control-label mbr-fonts-style display-7"><?=LANG_DESCRIPTION?></label>
					<textarea type="text" class="form-control" name="description" rows="7" maxlength="250" id="idis" ><?php echo $fdescript ?></textarea>
				</div>
				<span class="input-group-btn">
					<button type="submit" class="btn btn-primary btn-form display-4"><?php echo ($redir > 0) ? LANG_SAVE_CHANGE : LANG_ADD; ?></button>
				<?php
				if($redir > 0)
				{
					echo  '<input type="hidden" id="iid"  name="id" value="'.$redir.'">
						   <input type="hidden" id="ikeys" name="key"  value="193">
						   <button type="button" id="btn-del-obj" class="btn btn-md btn-secondary btn-form display-4" data-mess="'.LANG_DEL_OBJECT_MESS.'">'.LANG_DELETE.'</button>';
				}
				else
					echo  '<input type="hidden" name="key" value="3621" id="ikeys">';
				?>
					<button type="button" name="cancel" id="cancel_btn" class="btn btn-warning btn-form display-4"><?=LANG_CANCEL?></button>
				</span>
			</form>
			<p><br></p>
			<p>* <?=LANG_REQUIRED_FILEDS?></p>
		</div>
	</section>
	<?php include "footer.php";?>
	<?php include "scripts.php"; ?>
	<script src="js/post_new_places.js"></script>
</body>
</html>
