<?php
/*
 * Подключение языков
 * © Эрис
*/
define('LANGUAGE_DIR', $_SERVER['DOCUMENT_ROOT']."/lang/", false);

if(empty($_SESSION['lang'])){
	$lang = 'ru';
}else{
	$lang = $_SESSION['lang'];
}

include_once(LANGUAGE_DIR . $lang . '.php');
?>