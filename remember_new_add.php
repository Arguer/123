<?php
session_start();
/*
 * Восстановление пароля
 * © Эрис
*/
include "connection.php";
include "localization.php";

if(isset($_POST['id_user']))
  $id_new = $_POST['id_user'];
if(isset($_POST['key']))
  $key = $_POST['key'];
if(isset($_POST['password']))
  $password = trim($_POST['password']);
if(isset($_POST['password2']))
  $password2 = trim($_POST['password2']);

if($id_new && $key){

  	$result = array();

    $password_ln = mb_strlen($password,'UTF-8'); //длина пароля
    $pass_err = 0;

  	// поверка на совпадение паролей
  	if($password != $password2){
  		$pass_err = 1;
  	}

    if(!$password){
  		$result['status'] = 'error';
  		$result['name'] = 'password';
  		$result['info'] = LANG_ERR_PASS;
    }elseif($password_ln < 3){
  		$result['status'] = 'error';
  		$result['name'] = 'password';
  		$result['info'] = LANG_ERR_PASS_MIN_LN;
  	}elseif($password_ln > 50){
  		$result['status'] = 'error';
  		$result['name'] = 'password';
  		$result['info'] = LANG_ERR_PASS_LN;
    }elseif (!$password2) {
      $result['status'] = 'error';
  		$result['name'] = 'password2';
  		$result['info'] = LANG_ERR_PASS_CONFIRM;
    }elseif($pass_err == 1){
  		$result['status'] = 'error';
  		$result['name'] = 'password';
  		$result['name2'] = 'password2';
  		$result['info'] = LANG_ERR_PASS_DUBLICAT;
    }else{

      //если версия php выше 5.5.0
      if (version_compare(PHP_VERSION, '5.5.0') >= 0) {

    		$hash = password_hash($password, PASSWORD_DEFAULT);

    		$password = $hash;

    	}

      $unique_admin_result = $mysqli->query("SELECT * FROM admins WHERE id_admin='".$id_new."'");

      if ($unique_admin_result->num_rows > 0){

				if ($mysqli->query("UPDATE admins SET password = '".$password."' WHERE id_admin = ".$id_new) == TRUE){
					$result['status'] = 'success';
					$result['info'] = LANG_PASS_SUCCESS;

          //удаление ключа после успешного изменения пароля
          $mysqli->query('DELETE FROM tmp_key_admin WHERE id_admin = '.$id_new);

				}else{
					$result['status'] = 'error';
					$result['info'] = LANG_ERR_PASS_CHANGE;
				}

			}else{

				$result['status'] = 'error';
				$result['info'] = LANG_ERROR;

			}

    }

}

// преобразуем в JSON-формат
echo json_encode($result);
?>
