<?php
session_start();
/*
 * Страница вывода данных устройства
 * © Эрис
*/
include "localization.php";
include "connection.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы

	if (!isset($_SESSION['user']))
	{
		header('Location: /login.php');
		exit;
	}

	if(isset($_GET['id']))
		$id_dev = (int) intval($_GET['id']);

	if(isset($_GET['page']))
		$page = (int) intval($_GET['page']);

	$arr_filter = array();

	//$action = '/more.php?id='.$id_dev;

	if(isset($_POST['filter_send'])){ //отправка формы

		// время от
		if(isset($_POST['from'])){
			if(!empty($_POST['from'])){
				$_SESSION['filter']['from'] = $_POST['from'];
			}else{
				unset($_SESSION['filter']['from']);
			}
		}

		// время до
		if(isset($_POST['to'])){
			if(!empty($_POST['to'])){
				$_SESSION['filter']['to'] = $_POST['to'];
			}else{
				unset($_SESSION['filter']['to']);
			}
		}

		// заряд от
		if(isset($_POST['battery_from'])){
			if(!empty($_POST['battery_from'])){
				$_SESSION['filter']['battery_from'] = $_POST['battery_from'];
			}else{
				unset($_SESSION['filter']['battery_from']);
			}
		}

		// заряд до
		if(isset($_POST['battery_to'])){
			if(!empty($_POST['battery_to'])){
				$_SESSION['filter']['battery_to'] = $_POST['battery_to'];
			}else{
				unset($_SESSION['filter']['battery_to']);
			}
		}

		// статус
		if(isset($_POST['status'])){
			if(!empty($_POST['status'])){
				$_SESSION['filter']['status'] = $_POST['status'];
			}else{
				unset($_SESSION['filter']['status']);
			}
		}

		if(!$page){
			$action = '/more.php?id='.$id_dev.'#map_nav_btn';
		}elseif($page && $page != 0){
			$action = '/more.php?id='.$id_dev.'&page=0#map_nav_btn';
		}else{
			$action = '/more.php?id='.$id_dev.'&page='.$page.'#map_nav_btn';
		}

		header('Location: '.$action);

	} // конец обработки формы

	// сброс фильтра
	if(isset($_POST['filter_reset'])){
		unset($_SESSION['filter']);
		$from = $to = $battery_from = $battery_to = $status = '';
		header('Location: /more.php?id='.$id_dev.'#map_nav_btn');
	}

	/* переменные формы фильтра */

	// время от
	if(isset($_SESSION['filter']['from'])){
		$from = $_SESSION['filter']['from'];
		$dev_query .= " AND date >= '".$from."'";
	}

	// время до
	if(isset($_SESSION['filter']['to'])){
		$to = $_SESSION['filter']['to'];
		$dev_query .= " AND date <= '".$to."'";
	}

	// заряд от
	if(isset($_SESSION['filter']['battery_from'])){
		$battery_from = $_SESSION['filter']['battery_from'];
		$dev_query .= " AND field1 >= '".$battery_from."'";
	}

	// заряд до
	if(isset($_SESSION['filter']['battery_to'])){
		$battery_to = $_SESSION['filter']['battery_to'];
		$dev_query .= " AND field1 <= '".$battery_to."'";
	}

	// статус
	if(isset($_SESSION['filter']['status'])){
		$status = $_SESSION['filter']['status'];
		$dev_query .= " AND state LIKE '%".$status."%'";
	}

	//print_r($_SESSION['filter']);

?>
<html>
<head>
	<?php echo $head->getHead(LANG_DEVICE_TITLE, LANG_DEVICE_TITLE); ?>
	<link rel="stylesheet" href="assets/theme/css/datepicker.min.css">
	<script src="assets/web/assets/jquery/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.0.min.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<link rel="stylesheet" href="assets/theme/css/leaflet.css" />
  <script src="js/leaflet/leaflet.js"></script>
</head>
<body>
<?php include "headpanel.php";

	$str_filter = implode(',', $arr_filter);

	$id_place = 0;

	// массив статусов
	$status_arr = array(
		'OK' => 'OK',
		'Низкий заряд' => LANG_STATUS_1,
		'Критический заряд' => LANG_STATUS_2,
		'Порог 1 - Электрохим. 1' => LANG_STATUS_3,
		'Порог 2 - Электрохим. 1' => LANG_STATUS_4,
		'Порог 1 - Электрохим. 2' => LANG_STATUS_5,
		'Порог 1 - Кислород' => LANG_STATUS_6,
		'Порог 2 - Кислород' => LANG_STATUS_7,
		'Порог 1 - Пеллистор/мипекс' => LANG_STATUS_8,
		'Порог 2 - Пеллистор/мипекс' => LANG_STATUS_9,
		'Ошибка при чтении параметров из флеша' => LANG_STATUS_10,
		'Время не установлено' => LANG_STATUS_11,
		'Битая конфиг. таблица сенсора' => LANG_STATUS_12,
		'Ошибка конфигурации LMP' => LANG_STATUS_13,
		'Ошибка АЦП элх. сенсора' => LANG_STATUS_14,
		'I2C не работает' => LANG_STATUS_15,
		'Превышение диапазона' => LANG_STATUS_16,
		'Ошибка неисправности И2Ц' => LANG_STATUS_17,
		'Archive' => 'Archive',
		'Нажата кнопка!' => LANG_STATUS_18
	);

	//функция получения концентрации
	function conc_select($valcan)
	{
		$start = strpos($valcan, "<b>");	//Ищем тег
		$end = strpos($valcan, "</");	    //Ищем начало следующего тега
		$res = substr($valcan, $start+3, $end - $start - 3 );				//Выводим числовую часть
		$res = str_replace(",",".", $res);
		return $res;
	}

	//функция получения подписи
	function title_select($valcan)
	{
    $result_f = substr(strrchr($valcan, ">"), 1);
		$result_f = str_replace("\00","", $result_f);
		return $result_f;
	}

	$res_table = $mysqli->query("SELECT * FROM eventlog WHERE id_device = ".$id_dev." AND gps != 'null' ".$dev_query." ORDER BY id_event DESC");
	$size_of_table = $res_table->num_rows;

	$res_title = $mysqli->query("SELECT * FROM eventlog WHERE id_device = ".$id_dev." ORDER BY id_event DESC");
	//$size_of_table = $res_title->num_rows;
	$title_resul_sql = $res_title->fetch_assoc();

	$title_th = "['".LANG_TIME."','".title_select($title_resul_sql['channel1'])."','".title_select($title_resul_sql['channel2'])."','".title_select($title_resul_sql['channel3'])."','".title_select($title_resul_sql['channel4'])
	."']\n";

	$res = $mysqli->query("SELECT d.*, DATE_FORMAT(e.date, '%H:%i:%s') as date, e.id_event, e.id_device, e.id_place, e.gps, e.channel1, e.channel2, e.channel3, e.channel4, e.field1, e.state
							FROM eventlog e INNER JOIN devices d ON e.id_device = d.id_device WHERE d.id_device=".$id_dev." AND e.gps != 'null' AND e.gps != 0".$dev_query." ORDER BY e.id_event DESC LIMIT ".($page * 30).", 30 ", MYSQLI_USE_RESULT);

	//%d.%m.%y

	$s = 0;
	$chart = "";
	$write_gps = 0;

	while ($value = $res->fetch_assoc()) {
		//$strings[$s] .= ",['".substr($value['date'],11).
		$strings[$s] .= ",['".$value['date'].
		"',".conc_select($value['channel1']).",".conc_select($value['channel2']).",".conc_select($value['channel3']).",".conc_select($value['channel4'])."]\n";
		if ($write_gps == 0)
		{
		  $last_gps = $value['gps'];
		  $desc = $value['description'];
		  $id_place = $value['id_place'];
		  $write_gps = 1;
		}
		$s++;
		//echo $value['date'].'; ';
	}

	for ($x = $s; $x >= 0; $x--)
	{
		$chart .= $strings[$x];
	}

	//echo "
	//";

?>
<section class="section-table cid-r13PedKtlK" id="table1-4">
  <div class="container container-table">
      <h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2 mbr-dev-title"><?=LANG_DEVICE_TITLE?></h2>
      <h3 class="mbr-section-subtitle mbr-fonts-style align-center pb-5 mbr-light display-5"><?=LANG_DEV_DATA?></h3>
	  <div id="chart_div" style="width: 100%; height: 500px;"></div>
	  <div id='map' class='map'></div>
	  <div class ="navigation" id="map_nav_btn">
		  <div class="navbar-buttons ">
			<a id = "clear" class="btn btn-sm btn-primary display-3"><?=LANG_CLEAR?></a>
			<a id = "refresh" class="btn btn-sm btn-primary display-3"><?=LANG_RELOAD?></a>
			<a href="uploads/eventlog2.csv" download class="btn btn-sm btn-primary display-3"><?=LANG_DOWNLOAD?></a> 
		  </div>
	  </div><br>
      <div class="table-wrapper table-wrapper-more">
        <div class="container">
          <div class="row">
            <div class="filter-section">
				<a href="#" class="filter-toggle">[<span class="smb" data-title-plus="+" data-title-minus="-"></span>] <?=LANG_FILTER?></a>
				<form method="post" id="form_filter" action="">
                <div class="dataTables_filter" style="">
					<div class="filter-item filter-date">
						<span><?=LANG_DATE_FROM?>: <input type="text" name="ifrom" id="date_ifrom" class="filter-inp form-control datepicker-here" size="13" autocomplete="off" value="<?php echo $from; ?>">
						<input type="hidden" name="from" id="date_from" value="<?php echo $from; ?>">
						</span>
						<span><?=LANG_TO?>: <input type="text" name="ito" id="date_ito" class="filter-inp form-control datepicker-here" size="13" autocomplete="off" value="<?php echo $to; ?>">
						<input type="hidden" name="to" id="date_to" value="<?php echo $to; ?>">
						</span>
					</div>
					<div class="filter-item filter-battery">
						<span><?=LANG_BATTERY_FROM?>: <input type="text" name="battery_from" class="filter-inp form-control" size="3" value="<?php echo $battery_from; ?>"></span>
						<span><?=LANG_TO?>: <input type="text" name="battery_to" class="filter-inp form-control" size="3" value="<?php echo $battery_to; ?>"></span>
					</div>
					<div class="filter-item filter-status">
						<div class="dropdown">
						  <button class="btn dropdown-toggle" type="button" id="dropdown_status" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="">
							<?=LANG_STATUS?>...
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdown_status">
							<a class="dropdown-item dropdown-item-default" data-value="" href="#" <?php if(!isset($_SESSION['filter']['status'])): ?>style="display:none;"<?php endif ?>><?=LANG_STATUS?>...</a>
							<?php foreach($status_arr as $key=>$val): ?>
								<a class="dropdown-item" data-value="<?php echo $key; ?>" href="#"><?php echo $val; ?></a>
							<?php endforeach; ?>
						  </div>
						  <input type="hidden" name="status" class="inp-dropdown" value="<?php echo $status; ?>">
						</div>
					</div>
					<input type="hidden" name="idd" value="<?php echo $id_dev; ?>">
					<input type="hidden" name="pagen" value="<?php echo $page; ?>">
					<div class="filter-item filter-button">
						<button class="btn btn-sm btn-primary" id="filter_send" type="submit" name="filter_send"><?=LANG_FILTER_APPLY?></button>
						<button class="btn btn-sm btn-second" id="filter_reset" type="submit" name="filter_reset"><?=LANG_RESET?></button>
					</div>
                </div>
				</form>
            </div>
          </div>
        </div>
        <div class="container scroll" id="more_content">
<?php

	echo '<table class="table isSearch table-panel" cellspacing="0">
            <thead>
              <tr class="table-heads ">
								<th class="head-item mbr-fonts-style display-7">'.LANG_DESCRIPTION.'</th>
								<th class="head-item mbr-fonts-style display-7">'.LANG_FACTORY_NUMBER.'</th>
								<th class="head-item mbr-fonts-style display-7">'.LANG_TIME.'</th>
								<th class="head-item mbr-fonts-style display-7">'.LANG_CHANEL.' 1</th>
								<th class="head-item mbr-fonts-style display-7">'.LANG_CHANEL.' 2</th>
								<th class="head-item mbr-fonts-style display-7">'.LANG_CHANEL.' 3</th>
								<th class="head-item mbr-fonts-style display-7">'.LANG_CHANEL.' 4</th>
								<th class="head-item mbr-fonts-style display-7">'.LANG_CHARGE.'</th>
								<th class="head-item mbr-fonts-style display-7">'.LANG_STATUS.'</th>
						  </tr>
            </thead>
          <tbody>';

	$res_query = $mysqli->query("SELECT d.*, DATE_FORMAT(e.date, '%d.%m.%y %H:%i:%s') as date, e.id_event, e.id_device, e.id_place, e.gps, e.channel1, e.channel2, e.channel3, e.channel4, e.field1, e.state
							FROM eventlog e INNER JOIN devices d ON e.id_device = d.id_device WHERE d.id_device=".$id_dev." ".$dev_query." ORDER BY e.id_event DESC LIMIT ".($page * 30).", 30 ", MYSQLI_USE_RESULT);

	while ($value = $res_query->fetch_assoc()) {
		$strings[$s] .= ",['".substr($value['date'], 11)."',".conc_select($value['channel1']).",".conc_select($value['channel2']).",".conc_select($value['channel3']).",".conc_select($value['channel4'])."]\n";
		echo '<tr>';
		echo ' <td class="body-item mbr-fonts-style display-7" data-label="'.LANG_DESCRIPTION.'">'.$value['description'].'</td><td class="body-item mbr-fonts-style display-7" data-label="'.LANG_FACTORY_NUMBER.'">'.$value['znumber'].'</td> ';
		echo ' <td class="body-item mbr-fonts-style display-7" data-label="'.LANG_TIME.'">'.$value['date'].'</td><td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANEL.' 1">'.$value['channel1'].'</td>       ';
		echo ' <td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANEL.' 2">'.$value['channel2'].'</td><td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANEL.' 3">'.$value['channel3'].'</td>   ';
		echo ' <td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHANEL.' 4">'.$value['channel4'].'</td><td class="body-item mbr-fonts-style display-7" data-label="'.LANG_CHARGE.'">'.$value['field1'].'</td>      ';
		echo ' <td class="body-item mbr-fonts-style display-7" data-label="'.LANG_STATUS.'">'.$value['state'].'</td>';
		echo ' </tr>';
	}

	echo '</table>';

	/* ** Навигация ** */

	echo '<div class="navigation">';

	echo '<span class="nav_count">'.LANG_COUNT_RECORDS.': '.$size_of_table.'</span>';

	$count_pages = ($size_of_table / 30);
	echo LANG_PAGES.": ";
	if ($page != 0)
	{
		echo '<a href="/more.php?id='.$id_dev.'&page='.($page - 1).'#map_nav_btn" data-page="'.($page - 1).'"><< </a>';
		echo '<a href="/more.php?id='.$id_dev.'&page=0#map_nav_btn" data-page="0">1</a>';
	}
	else
		echo '<b>1</b>';

	if ($count_pages < 4)
	{
		for($z = 1; $z < 4; $z++)
		{
			if ($z <= $count_pages)
			{
				if ($z == $page) echo ', <b>'.($z + 1).'</b> ';
				else
				echo '<a href="/more.php?id='.$id_dev.'&page='.$z.'#map_nav_btn" data-page="'.$z.'">, '.($z + 1).'</a>';
			}
		}
	}
	else
	{
		if ($page > 4) echo '<a href="/more.php?id='.$id_dev.'&page=0#map_nav_btn" data-page="0">... </a>';
		for($z = -3; $z < 4; $z++)
		{
			if ((($z + $page) < $count_pages) && ($z + $page > 0))
			{
				if ($z == 0) echo ',<b> '.($z + $page + 1).'</b>';
				else
				echo '<a href="/more.php?id='.$id_dev.'&page='.($z + $page).'#map_nav_btn" data-page="'.($z + $page).'">, '.($z + $page + 1).'</a>';
			}
		}
		if ($count_pages - 4 > $page) echo '<a href="/more.php?id='.$id_dev.'&page='.(ceil($count_pages) - 1).'#map_nav_btn" data-page="'.(ceil($count_pages) - 1).'"> ... '.ceil($count_pages).'</a>';
		if ($page < ($count_pages - 1)) echo '<a href="/more.php?id='.$id_dev.'&page='.($page + 1).'#map_nav_btn" data-page="'.($page + 1).'"> >></a> ';
	}

	echo '</div>';

	echo '<div class="pagination"></div>';

	echo '<div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-7" href="/panel.php?id='.$id_place.'">&nbsp&nbsp&nbsp&nbsp'.LANG_BACK.'&nbsp&nbsp&nbsp&nbsp&nbsp<br></a></div>';

?>
        </div>
      </div>
    </div>
</section>
<?php include "footer.php";?>
	<script>
	document.getElementById('refresh').onclick = function() {
		location.reload();
	};
	</script>
	<?php //include "scripts.php"; ?>
	<!--script src="assets/web/assets/jquery/jquery.min.js"></script-->
	<script src="assets/popper/popper.min.js"></script>
	<script src="assets/tether/tether.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/smoothscroll/smooth-scroll.js"></script>
	<script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
	<script src="assets/datatables/jquery.data-tables.min.js"></script>
	<script src="assets/datatables/data-tables.bootstrap4.min.js"></script>
	<script src="js/datepicker.min.js"></script>
	<script src="js/more.js"></script>
	<script src="js/settings.js"></script>
	<script src="js/dropdown.js"></script>
	<script src="js/lang.js"></script>
	<script type="text/javascript">

		//инициализация datepicker
		$('input[name=ifrom].datepicker-here').datepicker({
			language: 'ru',
			timepicker:true,
			dateFormat: 'yyyy-mm-dd',
			timeFormat: 'hh:ii',
			onSelect: function(){
				//заполнение скрытых полей при изменении даты
				var f_val = document.getElementById('date_ifrom').value;
				document.getElementById('date_from').value = f_val;
			},
			language: '<?php echo isset($_SESSION['lang']) ? $_SESSION['lang'] : 'ru'; ?>'
		});
		$('input[name=ito].datepicker-here').datepicker({
			language: 'ru',
			timepicker:true,
			dateFormat: 'yyyy-mm-dd',
			timeFormat: 'hh:ii',
			onSelect: function(){
				//заполнение скрытых полей при изменении даты
				var t_val = document.getElementById('date_ito').value;
				document.getElementById('date_to').value = t_val;
			},
			language: '<?php echo isset($_SESSION['lang']) ? $_SESSION['lang'] : 'ru'; ?>'
		});

		//заполнение скрытых полей
		var inp_from = $('#form_filter').find('input[name=ifrom].filter-inp').val(),
			inp_to = $('#form_filter').find('input[name=ito].filter-inp').val(),
			inp_stat = $('#form_filter').find('input[name=status]').val();

		$('#form_filter').find('input[name=ifrom].filter-inp').attr('placeholder',inp_from);
		$('#form_filter').find('input[name=ito].filter-inp').attr('placeholder',inp_to);
		if(inp_stat){
			$('#form_filter').find('#dropdown_status').text(inp_stat);
		}

	</script>
	<script>
	"use strict"
	var last_location = new L.LatLng(<?php echo $last_gps; ?>),
			map = new L.Map('map').setView(last_location, 17),
			tileServer = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
			tileAttribution = 'Map data: <a href="http://openstreetmap.org">OSM</a>';

	L.tileLayer(tileServer, {attribution: tileAttribution,  maxZoom: 18}).addTo(map);

	function init(){
		var marker = L.marker(last_location).addTo(map);

		var polylinePoints = [
		<?php
		$gps_arr = "";

	  $res2 = $mysqli->query("SELECT gps, state FROM (SELECT * FROM eventlog WHERE id_device=".$id_dev." ORDER BY id_event DESC LIMIT ".($page * 30).", 30) A  ORDER BY id_event ASC", MYSQLI_USE_RESULT);

	  while ($value = $res2->fetch_assoc()) {
			if($value['state'] == "OK" || $value['state'] == "Время не установлено\n"){
				$gps_arr .= $value['gps'];
				$gps_arr .= ", ";
				echo "new L.LatLng(".$value['gps']."),";
			}
	  }

		?>
		];

		var polylineOptions = {
               color: 'blue',
               weight: 5,
               opacity: 0.5
             };

   	var polyline = new L.Polyline(polylinePoints, polylineOptions);

   	map.addLayer(polyline);

		var polylinePointsR = [

			<?php
			$res3 = $mysqli->query("SELECT gps, state FROM (SELECT * FROM eventlog WHERE id_device=".$id_dev." AND gps != 'null' AND gps != 0 ORDER BY id_event DESC LIMIT ".($page * 30).", 30) A  ORDER BY id_event ASC", MYSQLI_USE_RESULT);

			while ($value_r = $res3->fetch_assoc()) {
				if($value_r['state'] != "OK" && $value_r['state'] != "Время не установлено\n"){
					echo "new L.LatLng(".$value_r['gps']."),";
				}
		  }
			?>

		];

		var polylineOptionsR = {
               color: 'red',
               weight: 5,
               opacity: 0.5
             };

   	var polyline_r = new L.Polyline(polylinePointsR, polylineOptionsR);

   	map.addLayer(polyline_r);

	}

	init();

	</script>
	<script type="text/javascript">

      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

	   	var chart;
	   	var data;

      function drawChart() {

        data = google.visualization.arrayToDataTable([<?php echo $title_th.$chart; ?>]);

        var options = {
          title: '<?=LANG_GRAPH?>',
          hAxis: {title: '<?=LANG_TIME?>',  titleTextStyle: {color: '#333'}, textStyle: {fontSize:14} },
          vAxis: {minValue: 0}
        };

        chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);

				google.visualization.events.addListener(chart, 'select', selectHandler);

      }

	   	var gps_coord = [<?php echo $gps_arr; ?>];
	   	var placemark;
			var placemarkGroup = L.layerGroup().addTo(map);
			//var placemark = new L.marker([gps_coord[selectedItem.row * 2], gps_coord[selectedItem.row*2 + 1]]);

	  	function selectHandler() {

				var selectedItem = chart.getSelection()[0];

				if(selectedItem){
					placemark = new L.marker([gps_coord[selectedItem.row * 2], gps_coord[selectedItem.row*2 + 1]]).addTo(placemarkGroup);
				}

			}

			document.getElementById('clear').onclick = function() {

				placemarkGroup.clearLayers();

			};
			

    </script>
	

</body>
</html>
