$('.dropdown-item').on('click', function(e){
	e.preventDefault();
	$('.dropdown-item').removeClass('hover-item');
	var text_val = $(this).text(),
		data_val = $(this).attr('data-value');
	$(this).parent('.dropdown-menu').parent('.dropdown').find('.inp-dropdown').val(data_val);
	$(this).parent('.dropdown-menu').parent('.dropdown').find('.dropdown-toggle').text(text_val);
	$(this).parent('.dropdown-menu').parent('.dropdown').find('.dropdown-toggle').attr('title', text_val);
	$(this).addClass('hover-item');
	$('.dropdown-item-default').show();
	if($(this).hasClass('dropdown-item-default')){
		$('.dropdown-item-default').hide();
	}
});

$('.filter-toggle').click(function(e){
	e.preventDefault();
	$('#form_filter').slideToggle();
	$(this).find('.smb').toggleClass('ndash');
});