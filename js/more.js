function more_load(form, id_dev, page, redir = false){
	
	$.ajax({
		url: "more_table.php",
		cache: false,
		type: "POST",
		async:true,
		data: {data_dev:form, id_dev:id_dev, page:page}
	}).done(function(html){
		if(redir == true){
			$("#more_content").html(html);
			location.href="/more.php?id="+id_dev+"&page=0#map_nav_btn";
		}else{
			$("#more_content").html(html);
		}
	});
	
}