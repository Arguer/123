/*
 * Переключение языков
 * © Эрис
*/
$(document).ready(function(){
	
	$('.lang-list').find('.lang-item').click(function(){
		$('.lang-wrap').toggle();
	});
	
	$(".lang-wrap").find(".lang-item").on('click', function(e){
		e.preventDefault();
		var lang = $(this).data("lang");
		$.ajax({
			url: "/lang_action.php",
			type:"post",
			dataType: "json",
			data: {lng:lang},
			success: function(data) {
				if(data.status == 'success'){
					location.reload();
				}else if(data.status == 'error'){
					alert(data.error);
				}
			},
			error: function (xhr, ajaxOptions, thrownError){
				alert(thrownError); //выводим ошибку
			}			
		});
	});
	
});
