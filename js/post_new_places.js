// Обработка отправки формы
// редактирование/добавление подразделения

$('#send_form').on('submit', function(e){
	e.preventDefault();
	var form = $(this),
		ikey = $('#ikeys').val();
	
	if(ikey == '193'){
		
		// обработка редактирования подразделения
		
		// очищаем сообщения об ошибках
		$('.mbr-block-title').empty();
		
		form.find('input').css('border-color','#d2d2d2');
		
		// обработка ajax
		$.ajax({
			type: "post",
			url: "edit.php",
			dataType: 'json',
			data: form.serialize(),
			success: function(data) {
				if(data.status == 'success'){
					alert(data.info);
					form.find('input').css('border-color','#d2d2d2');
					location.href='object.php';
				}else if(data.status == 'error'){
					$('.mbr-block-title').append('<h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5 message-title error-title">' + data.info + '</h3>');
					if(data.name){
						form.find('[name="' + data.name + '"]').css('border-color','#f00');
					}
					if(data.name2){
						form.find('[name="' + data.name2 + '"]').css('border-color','#f00');
					}
				}
			},
			error: function (xhr, ajaxOptions, thrownError){
				alert(thrownError); //выводим ошибку
			}
		});
	
	}else if(ikey == '3621'){
		
		// обработка добавления подразделения
		
		// очищаем сообщения об ошибках
		$('.mbr-block-title').empty();
		
		form.find('input').css('border-color','#d2d2d2');
		
		// обработка ajax
		$.ajax({
			type: "post",
			url: "add_places.php",
			dataType: 'json',
			data: form.serialize(),
			success: function(data) {
				if(data.status == 'success'){
					alert(data.info);
					form.find('input').css('border-color','#d2d2d2');
					location.href='object.php';
				}else if(data.status == 'error'){
					$('.mbr-block-title').append('<h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5 message-title error-title">' + data.info + '</h3>');
					if(data.name){
						form.find('[name="' + data.name + '"]').css('border-color','#f00');
					}
					if(data.name2){
						form.find('[name="' + data.name2 + '"]').css('border-color','#f00');
					}
				}
			},
			error: function (xhr, ajaxOptions, thrownError){
				alert(thrownError); //выводим ошибку
			}
		});
		
	}
	
});


//удаление подразделения
$('#btn-del-obj').click(function(){
		
	var ckey = $('#ikeys'),
		data_form = $('#send_form').serialize(),
		mess = $(this).attr('data-mess');
	
	if(confirm(mess)){
	
		// обработка ajax
		$.ajax({
			type: "post",
			url: "del_object.php",
			dataType: 'json',
			data: data_form,
			success: function(data) {
				alert(data.info);
				location.href='object.php';
			},
			error: function (xhr, ajaxOptions, thrownError){
				alert(thrownError); //выводим ошибку
			}
		});
	
	}
		
});

// нажатие кнопки "Отмена"
$('#cancel_btn').click(function(){
	location.href='object.php';
});

$(document).ready(function(){
	
	// обработка чекбоксов в боковой панели
	
	var	panel = $('.panel-view'),
		title_checked = $('.title-all-checkbox').attr('title-checked'),
		title_none = $('.title-all-checkbox').attr('title-none');;
	
	$('.checkbox-inp').each(function(){
		
		//обрабатываем нажатие чекбокса
		$(this).on('change', function(){
			
			if($(this).prop('checked')){
				//отображаем кнопку подробного просмотра
				panel.show();
			}
			
			//выделение всех чекбоксов
			if($(this).is('#check_all')) {
				if($(this).prop('checked')){
					/*var check = $(this).parent().parent().parent().parent().parent().find('.checkbox-obj');
					check.prop("checked", !check.prop("checked"));*/
					$('.title-all-checkbox').text(title_checked);
					$('input.checkbox-obj').each(function(){
						$('input.checkbox-obj').prop('checked','checked');
					});
				}else{
					$('.title-all-checkbox').text(title_none);
					$('input.checkbox-obj').each(function(){
						$('input.checkbox-obj').prop('checked',false);
					});
				}
			}
			
			//собираем массив из id
			var arr = $('input.checkbox-obj:checked').map(function(){ return '&id[]='+this.value; }).get();

			//если не выделен ни один чекбокс
			if($('input.checkbox-obj:checked').length > 0){
				panel.show();
				//удаляем амперсанд из первого элемента
				var first = arr[0].replace('&','');
				arr[0] = first;
				//конвертим массив в строку
				var arr_r = arr.join("");
				//добавляем значение атрибута ссылки с параметрами в кнопку
				panel.prop('href','panel.php?'+arr_r);
			}else{
				//скрываем кнопку
				panel.hide();
				panel.prop('href','#');
			}
			
		});
		
	});
	
	if($('input.checkbox-obj:checked').length > 0){
		$('.title-all-checkbox').text(title_checked);
	}else{
		$('.title-all-checkbox').text(title_none);
	}
	
});
