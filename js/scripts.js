$(document).ready(function(){

    $('.sidebar_btn').click(function() {
		
		if($(this).hasClass('active')){
			
			if($(window).width() > 991){
				$('.section-side-panel').css({
					'transform':'translate(-370px, 0px)',
					'transition-duration':'.3s'
				});
				$('.section-wrapper').css({
					'transform':'translate(0px, 0px)',
					'width':'100%',
					'transition-duration':'.3s'
				});
				$('.ymaps-2-1-73-map').css({
					'width':'100%',
					'transition-duration':'.3s'
				});
			}else{
				$('.section-side-panel').css({
					'transform':'translate(-100%, 0px)',
					'transition-duration':'.3s'
				});
			}
			
		}else{
			
			if($(window).width() > 991){
				$('.section-side-panel').css({
					'transform':'translate(0px, 0px)',
					'transition-duration':'.3s'
				});
				$('.section-wrapper').css({
					'transform':'translate(370px, 0px)',
					'width':'-=370',
					'transition-duration':'.3s'
				});
				$('.ymaps-2-1-73-map').css({
					'width':'-=370',
					'transition-duration':'.3s'
				});
			}else{
				$('.section-side-panel').css({
					'transform':'translate(0px, 0px)',
					'transition-duration':'.3s'
				});
			}
			
		}
		
		$(this).toggleClass('active');
		return false;
		
	});
	
	
	$('.place-name').click(function(){
		
		$(this).closest('.place-item').find('.device-list').slideToggle(200);
		$(this).closest('.place-name').find('.fa-arrow').toggleClass('fa-angle-down');
		$(this).closest('.place-item').toggleClass('place-item-open');
		
	});
	
	$('.place-item').each(function(){
		
		if($(this).find('.device-list').children().length == 0){
			$(this).find('.fa-arrow').hide();
			$(this).find('.device-list').remove();
		}
		
	});
	
});
	
