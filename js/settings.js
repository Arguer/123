$.fn.datepicker.language['ru'] =  {
	days: ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
	daysShort: ['Вос','Пон','Вто','Сре','Чет','Пят','Суб'],
	daysMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
	months: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
	monthsShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
	today: 'Сегодня',
	clear: 'Очистить',
	dateFormat: 'dd.mm.yyyy',
	timeFormat: 'hh:ii',
	firstDay: 1
};

$.fn.datepicker.language['en'] =  {
	days: ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
	daysShort: ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'],
	daysMin: ['Su','Mo','Tu','We','Th','Fr','Sa'],
	months: ['January','February','March','April','May','June','July','August','September','October','November','December'],
	monthsShort: ['Jan.','Feb.','Mar.','Apr.','May','June','July','Aug.','Sept.','Oct.','Nov.','Dec.'],
	today: 'Today',
	clear: 'Clear',
	dateFormat: 'dd.mm.yyyy',
	timeFormat: 'hh:ii',
	firstDay: 1
};