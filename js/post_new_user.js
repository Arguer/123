/*
 * Добавление/изменение аккаунта
 * © Эрис
*/

$(document).ready(function(){
	
	// обработка добавления/изменения аккаунта
	$('#user_form').on('submit', function(e){
		
		e.preventDefault();
		
		var form = $(this),
			data_form = form.serialize();
		
		// очищаем сообщения об ошибках
		$('.mbr-block-title').empty();
		
		// обработка ajax
		$.ajax({
			type: "post",
			url: "add_user.php",
			dataType: 'json',
			data: data_form,
			success: function(data) {
				//alert(data);
				// если запрос успешно обработан
				form.find('input, textarea').css('border-color','#e8e8e8');
				if(data.status == 'error'){
					// если json вернул ошибки
					$('.mbr-block-title').append('<h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5 message-title error-title">' + data.info + '</h3>');
					form.find('[name="' + data.name + '"]').css('border-color','#f00');
					if(data.name2)
						form.find('[name="' + data.name2 + '"]').css('border-color','#f00');
				}else if(data.status == 'success'){
					// если данные успешно обработаны
					$('.mbr-block-title').append('<h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5 message-title success-title">' + data.info + '</h3>');
					form.find('input, textarea').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
					alert(data.info);
					location.href='users.php';
				}
			},
			error: function (xhr, ajaxOptions, thrownError){
                alert(thrownError); //выводим ошибку
            }
		});
		
	});
	
	// удаление аккаунта
	$('#btn_del_user').click(function(){
		
		var ckey = $('#ckey'),
			data_form = $('#user_form').serialize(),
			mess = $(this).attr('data-mess');
		
		if(confirm(mess)){
		
			// обработка ajax
			$.ajax({
				type: "post",
				url: "del_user.php",
				dataType: 'json',
				data: data_form,
				success: function(data) {
					alert(data.info);
					location.href='users.php';
				},
				error: function (xhr, ajaxOptions, thrownError){
					alert(thrownError); //выводим ошибку
				}
			});
		
		}
		
	});
	
	// выбор из списка компаний
	$('.dropdown-company').find('.dropdown-item').click(function(e){
		e.preventDefault();
		let text_val = $(this).text(),
			data_val = $(this).attr('data-value'),
			data_id = $(this).attr('data-id');
		$('.dropdown-company').find('.dropdown-item').removeClass('hover-item');
		$(this).parent('.dropdown-menu').parent('.dropdown').find('.inp-dropdown').val(data_id);
		$(this).parent('.dropdown-menu').parent('.dropdown').find('.dropdown-toggle').text(text_val);
		$(this).parent('.dropdown-menu').parent('.dropdown').find('.dropdown-toggle').attr('title', text_val);
		$(this).addClass('hover-item');
	});

	// выбор из списка ролей
	$('.dropdown-user_role').find('.dropdown-item').click(function(e){
		e.preventDefault();
		let text_val = $(this).text(),
			data_val = $(this).attr('data-value'),
			data_id = $(this).attr('data-id');
		$('.dropdown-user_role').find('.dropdown-item').removeClass('hover-item');
		$(this).parent('.dropdown-menu').parent('.dropdown').find('.inp-dropdown').val(data_id);
		$(this).parent('.dropdown-menu').parent('.dropdown').find('.dropdown-toggle').text(text_val);
		$(this).parent('.dropdown-menu').parent('.dropdown').find('.dropdown-toggle').attr('title', text_val);
		$(this).addClass('hover-item');
	});
	
	// нажатие кнопки "Отмена"
	$('#btn_cancel_user').click(function(){
		location.href='users.php';
	});
	
});