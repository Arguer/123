$(document).ready(function(){
	
	$('#company_form').on('submit', function(e){
		
		e.preventDefault();
		
		var cname = $('#cname'),
			caddr = $('#caddr'),
			cdesc = $('#cdesc'),
			cadm = $('#cadm'),
			ckey = $('#ckey'),
			form = $(this),
			data_form = form.serialize();
		
		// проверка ключа
		if(ckey.val() == '1532') {
			
		// добавление компании
		
		// очищаем сообщения об ошибках
		$('.mbr-block-title').empty();
		
		// обработка ajax
		$.ajax({
			type: "post",
			url: "add_company.php",
			dataType: 'json',
			data: data_form,
			success: function(data) {
				//alert(data);
				// если запрос успешно обработан
				form.find('input, textarea').css('border-color','#e8e8e8');
				if(data.status == 'error'){
					// если json вернул ошибки
					$('.mbr-block-title').append('<h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5 message-title error-title">' + data.info + '</h3>');
					form.find('[name="' + data.name + '"]').css('border-color','#f00');
				}else if(data.status == 'success'){
					// если данные успешно обработаны
					$('.mbr-block-title').append('<h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5 message-title success-title">' + data.info + '</h3>');
					form.find('input, textarea').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
					alert(data.info);
					location.href='company.php';
				}
			},
			error: function (xhr, ajaxOptions, thrownError){
                alert(thrownError); //выводим ошибку
            }
		});
			
		}else if (ckey.val() == '199') {
			
		// редактирование компании
			
		// обработка ajax
		$.ajax({
			type: "post",
			url: "edit_company.php",
			dataType: 'json',
			data: data_form,
			success: function(data) {
				//alert(data);
				// если запрос успешно обработан
				form.find('input, textarea').css('border-color','#e8e8e8');
				if(data.status == 'error'){
					// если json вернул ошибки
					$('.mbr-block-title').append('<h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5 message-title error-title">' + data.info + '</h3>');
					form.find('[name="' + data.name + '"]').css('border-color','#f00');
				}else if(data.status == 'success'){
					// если данные успешно обработаны
					$('.mbr-block-title').append('<h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5 message-title success-title">' + data.info + '</h3>');
					form.find('input, textarea').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
					alert(data.info);
					location.href='company.php';
				}
			},
			error: function (xhr, ajaxOptions, thrownError){
                alert(thrownError); //выводим ошибку
            }
		});
		
		}

	});
	
	
	// удаление компании
	$('#button-delete').click(function(){
		
		var ckey = $('#ckey'),
			data_form = $('#company_form').serialize(),
			mess = $(this).attr('data-mess');
		
		if(confirm(mess)){
		
			// обработка ajax
			$.ajax({
				type: "post",
				url: "del_company.php",
				dataType: 'json',
				data: data_form,
				success: function(data) {
					alert(data.info);
					location.href='company.php';
				},
				error: function (xhr, ajaxOptions, thrownError){
					alert(thrownError); //выводим ошибку
				}
			});
		
		}
		
	});
	
	// нажатие кнопки "Отмена"
	$('#cancel_btn_comp').click(function(){
		location.href='company.php';
	});
	
});