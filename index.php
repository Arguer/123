<?php
session_start();
/*
 * Главная страница
 * © Эрис
*/
include "localization.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы
?>
<!DOCTYPE html>
<html>
	<head>
	<?php echo $head->getHead(LANG_TITLE_SITE); ?>
	</head>
	<body>
	<?php echo(password_hash("admin",PASSWORD_DEFAULT)) ?> 
	<?php include "headpanel.php"; ?>
	<section class="cid-qTkA127IK8 mbr-fullscreen mbr-parallax-background page-index" id="header2-1">
		<div class="mbr-fullscreen-back"></div>
		<div class="container align-center">
			<div class="row justify-content-md-center">
				<div class="mbr-white col-lg-10 col-md-12 col-sm-12">
					<h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1"><?=LANG_TITLE_HOME?></h1>
					<div class="mbr-section-btn index-section-btn"><a class="btn btn-md btn-secondary display-4" href="object.php"><?=LANG_BTN_HOME_1?></a>
						<a class="btn btn-md btn-white-outline display-4 btn-site-company" href="http://eriskip.com/"><?=LANG_BTN_HOME_2?></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php include "footer.php"; ?>
	<?php include "scripts.php"; ?>

	</body>
</html>