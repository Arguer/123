<?php
session_start();
/*
 * Формирование данных устройства в формате JSON
 * © Эрис
*/
if (!isset($_SESSION['user'])) {
	header('Location: /login.php');
	exit;
}

include "connection.php";

$id_place = $_POST['arrp'];
//$id_place = array(1,7,13,14,16);

if(is_array($id_place)){
	//если массив
	$arr_place = implode(",", $id_place);
}else{
	//если не является массивом
	$arr_place = $id_place;
}

//echo json_encode($id_place);

$current = '{
			"type": "FeatureCollection",
			"features": [
				';


$res_devices = $mysqli->query("SELECT d.*, ev1.*
							   FROM devices d
							   INNER JOIN eventlog ev1 ON d.id_device = ev1.id_device
							   AND ev1.id_place IN(".$arr_place.") GROUP BY ev1.id_device");

$arr_device = array();

while ($val_dev = $res_devices->fetch_assoc()){

	$arr_device[] = $val_dev['id_device'];

}


if(is_array($arr_device)){

	foreach($arr_device as $key=>$val){

		$res = $mysqli->query("SELECT d.*, e.*
								FROM devices d INNER JOIN eventlog e ON e.id_device = d.id_device AND e.id_device = ".$val." AND e.gps != 'null' AND e.gps != 0 AND e.gps IS NOT NULL ORDER BY e.id_event DESC LIMIT 1", MYSQLI_USE_RESULT);

		while ($value = $res->fetch_assoc()) {
					$dateNow = date("y-m-d H:i:s");
					if ((strtotime($dateNow) - strtotime($value['date'])) < 900){
						if ($value['state'] == "OK" || $value['state'] == "Время не установлено\n"){
							$color_link = 'class=\'link-color-green\'';
						}else{
							$color_link = 'class=\'link-color-red\'';
						}
					}else{
						$color_link = 'class=\'link-color-gray\'';
					}

					$current .= '{
					"type": "Feature",
					"id": "'.$value['id_device'].'",
					"geometry": {
						"type": "Point",
						"coordinates": ['.$value['gps'].']
					},
					"properties": {
						"balloonContent": "<a href=\'more.php?id='.$value['id_device'].'\' '.$color_link.' target=\'_blank\'>'.$value['description'].'</a>",
						"hintContent": "'.$value['description'].'",
						"color": "'.$color_link.'",';
						if ((strtotime($dateNow) - strtotime($value['date'])) < 900)
						{
							if ($value['state'] == "OK" or $value['state'] == "Время не установлено\n"){
								$current .= '"clusterCaption":"<span style=\'color:#00AA00;margin-right:5px;\'>'.$value['description'].'</span><i class=\'fa fa-map-marker\' aria-hidden=\'true\' style=\'color:#00AA00;left:10px;\'></i>",';
								$current .= '"color":"green"';
							}else{
								$current .= '"clusterCaption":"<span style=\'color:#AA0000;margin-right:5px;\'>'.$value['description'].'</span><i class=\'fa fa-exclamation\' aria-hidden=\'true\' style=\'color:#AA0000;\'></i>",';
								$current .= '"color":"red"';
							}
						}else{
							$current .= '"clusterCaption":"'.$value['description'].'",';
							$current .= '"color":"gray"';
						}

					$current .= '},
					"options": {';
					$dateNow = date("y-m-d H:i:s");

					if ((strtotime($dateNow) - strtotime($value['date'])) < 900)									//если от последнего коннекта прошло не больше 15 минут
					{
							if ($value['state'] == "OK" or $value['state'] == "Время не установлено\n")
								$current .= '"preset": "islands#greenCircleDotIconWithCaption"';
							else
								$current .= '"preset": "islands#redCircleDotIconWithCaption"';
					}
					else
							$current .= '"preset": "islands#grayCircleDotIconWithCaption"';

					$current .= '
					}
				   },';

		}

	}

}else{

	$res = $mysqli->query("SELECT d.*, e.*
								FROM devices d INNER JOIN eventlog e ON e.id_device = d.id_device AND e.id_device = ".$id_place." AND e.gps != 'null' AND e.gps != 0 AND e.gps IS NOT NULL ORDER BY e.id_event DESC LIMIT 1", MYSQLI_USE_RESULT);

		while ($value = $res->fetch_assoc()) {

					$current .= '{
					"type": "Feature",
					"geometry": {
						"type": "Point",
						"coordinates": ['.$value['gps'].']
					},
					"properties": {
						"balloonContent": "<a href=\'more.php?id='.$value['id_device'].'\'>'.$value['description'].'</a>",
						"hintContent": "'.$value['description'].'",
						"clusterCaption": "'.$value['description'].'"
					},
					"options": {
						"preset": "islands#icon",
					"iconColor":';
					$dateNow = date("y-m-d H:i:s");
					if ((strtotime($dateNow) - strtotime($value['date'])) < 900)									//если от последнего коннекта прошло не больше 15 минут
					{
							if ($value['state'] == "OK" or $value['state'] == "Время не установлено\n")
								$current .= '	"#00AA00"';
							else
								$current .= '	"#AA0000"';
					}
					else
							$current .= '	"#888888"';

					$current .= '
					}
				   },';

		}

}

$current = 	mb_substr($current, 0, -1);
$current .= "],";

$current .= '"messFeatures": [';

foreach($arr_device as $key=>$val){

	$res_mess = $mysqli->query("SELECT d.*, e.*
							FROM devices d INNER JOIN eventlog e ON e.id_device = d.id_device AND e.id_device = ".$val." ORDER BY e.id_event DESC LIMIT 1", MYSQLI_USE_RESULT);

	while ($val_mess = $res_mess->fetch_assoc()) {

		$dateNow = date("y-m-d H:i:s");
		if ((strtotime($dateNow) - strtotime($val_mess['date'])) < 900){
			if ($val_mess['state'] == "OK" || $val_mess['state'] == "Время не установлено\n"){
				$color_ln = 'class=\'link-color-green\'';
			}else{
				$color_ln = 'class=\'link-color-red\'';
			}
		}else{
			$color_ln = 'class=\'link-color-gray\'';
		}

		$current .= '{
									"messProp":{';

		$mess_cont = '"messContent":"<a href=\'more.php?id='.$val_mess['id_device'].'\' '.$color_ln.' target=\'_blank\'>'.$val_mess['description'].'</a>",';

		//$current .= $mess_cont.' "mess":"no"';

		if ((strtotime($dateNow) - strtotime($val_mess['date'])) < 900){
			if ($val_mess['state'] == "OK" || $val_mess['state'] == "Время не установлено\n"){

				$current .= $mess_cont.' "mess":"no"';
			}else{
				$current .= $mess_cont.' "mess":"yes"';
			}
		}else{
			$current .= $mess_cont.' "mess":"no"';
		}

		$current .= '}},';

	}

}

$current = 	mb_substr($current, 0, -1);

$current .= ']}';

//echo $current;

echo json_encode($current);

?>
