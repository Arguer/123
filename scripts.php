<?php
// очищение сессии фильтра при переходе на другие страницы, кроме more.php
// поместили здесь, т.к. данный файл не подключен на странице с фильтром
unset($_SESSION['filter']);
?>
<script src="assets/web/assets/jquery/jquery.min.js"></script>
<script src="assets/popper/popper.min.js"></script>
<script src="assets/tether/tether.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<!--script src="assets/dropdown/js/script.min.js"></script-->
<script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
<script src="assets/parallax/jarallax.min.js"></script>
<script src="assets/smoothscroll/smooth-scroll.js"></script>
<script src="assets/theme/js/script.js"></script>
<script src="js/lang.js"></script>