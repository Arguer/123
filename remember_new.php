<?php
session_start();
/*
 * Восстановление пароля
 * © Эрис
*/
include "connection.php";
include "localization.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы

if(isset($_GET['id_user']))
  $id_user = $_GET['id_user'];
if(isset($_GET['key']))
  $key = $_GET['key'];

$table_list = $mysqli->query("SELECT * FROM tmp_key_admin WHERE id_admin = ".$id_user." AND key_admin = '".$key."'");

if ($table_list->num_rows == 0){
  header('Location: /err404.php');
}

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $head->getHead(LANG_PANEL_TITLE, LANG_CONTENT_1); ?>
</head>
<body>
	<?php include "headpanel.php";?>
	<section class="section-table cid-r13PedKtlK page-remember" id="table1-4">
		<div class="container container-table table-login">
			<h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2"><?=LANG_NEW_PASS?></h2>
      <form method="post" class="remember-form remember-new-form" id="new_pass_form" action="" autocomplete="off">
				<input type="password" name="password" id="new_pass" value="" placeholder="<?=LANG_PASS?>" required>
        <input type="password" name="password2" id="new_pass2" value="" placeholder="<?=LANG_PASS_CONF?>" required>
        <input type="hidden" name="id_user" value="<?php echo $id_user; ?>">
        <input type="hidden" name="key" value="<?php echo $key; ?>">
				<input type="submit" name="send_new_pass" id="send_new_pass" class="btn btn-primary" value="<?=LANG_RUN?>">
			</form>
    </div>
    <p class="errormsg"><img src="assets/images/errlogin.png"></p>
  </section>
  <?php include "footer.php"; ?>
  <?php include "scripts.php"; ?>
  <script>
    $('#new_pass_form').find('#send_new_pass').click(function(e){
      e.preventDefault();
      var form = $('#new_pass_form');
      $('.err-text').remove();
      $('.success-text').remove();
      // обработка ajax
      $.ajax({
        type: "post",
        url: "remember_new_add.php",
        dataType: "json",
        data: form.serialize(),
        success: function(data) {
          // если запрос успешно обработан
          form.find('input, textarea').removeClass('err-field');
          if(data.status == 'error'){
            // если json вернул ошибки
            form.before('<div class="err-text">' + data.info + '</div>');
            if(data.name)
              form.find('[name="' + data.name + '"]').addClass('err-field');
            if(data.name2)
              form.find('[name="' + data.name2 + '"]').addClass('err-field');
          }else if(data.status == 'success'){
            // если данные успешно обработаны
            alert(data.info);
            location.href='/login.php';
          }
        },
        error: function (xhr, ajaxOptions, thrownError){
          form.before('<div class="err-text">' + thrownError + '</div>');
        }
      });
      });
    </script>
</body>
</html>
