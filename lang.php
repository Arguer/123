<?php
/*
 * Переключение языков
 * © Эрис
*/
$arr_lang = array('ru', 'en');
?>
<div class="lang-header">
	<div class="lang-list">
		<?php
			foreach($arr_lang as $ln=>$k){
				if($k == $lang){
					echo '<div class="lang-item">
							<span class="flag '.$k.'" style="background-image:url(/assets/images/'.$k.'.png)"></span>
							<input type="hidden" name="lang" value="'.$k.'">
							<span class="arrow-lang"></span>
						  </div>';
				}
			}
		?>
	</div>
	<div class="lang-wrap" style="display: none;">
		<?php
			foreach($arr_lang as $ln=>$k){
				if($k != $lang){
					echo  '<div class="lang-item" data-lang="'.$k.'">
								<span class="flag '.$k.'" style="background-image:url(/assets/images/'.$k.'.png)"></span>
						   </div>';
				}
			}
		?>
	</div>
</div>