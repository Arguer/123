<?php
session_start();
/*
 * "Шапка" сайта
 * © Эрис
*/
?>
 <section class="menu cid-qTkzRZLJNu" once="menu" id="menu1-0">
    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
		<?php if($_SERVER['SCRIPT_NAME'] == '/panel.php'): ?>
		<div class="sidebar_btn sidebar_nav">
			<svg class="ham hamRotate180 ham5" viewBox="0 0 100 100" width="55">
			  <path class="line top"
					d="m 30,33 h 40 c 0,0 8.5,-0.68551 8.5,10.375 0,8.292653 -6.122707,9.002293 -8.5,6.625 l -11.071429,-11.071429" />
			  <path class="line middle"
					d="m 70,50 h -40" />
			  <path class="line bottom"
					d="m 30,67 h 40 c 0,0 8.5,0.68551 8.5,-10.375 0,-8.292653 -6.122707,-9.002293 -8.5,-6.625 l -11.071429,11.071429" />
			</svg>
		</div>
		<?php endif ?>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-icon"></span>
			<span class="navbar-icon"></span>
			<span class="navbar-icon"></span>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="/">
                        <img src="assets/images/-192x45.png" alt="Eris" title="" style="height: 2.5rem;">
                    </a>
                </span>
                <span class="navbar-caption-wrap"><a class="navbar-caption text-white display-4" href="http://eriskip.com/"></a></span>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <?php
			if (!isset($_SESSION['user'])){
				echo '<div class="header-right not-logged-header">';
				echo '<div class = "mbr-white"> <form action="/login.php" method="post">
					<span class="logform-title">'.LANG_LOGIN.': </span><input type="text" name="login" class="loginform"/>
					<span class="logform-title">'.LANG_PASS.': </span><input class="loginform" type="password" name="pass" />
					<input type="submit" class="btn btn-primary" value="'.LANG_ENTER.'" name="log_in" />
          <a href="/remember.php" class="rmb-link-header">Напомнить пароль</a>
					</form></div>';
				include 'lang.php';
				echo '</div>';
			} else {
				echo '<div class="navbar-wrap">';
			    include 'nav_bar.php';
				echo '</div>';
				echo '<div class="header-right">';
				echo'<div class = "mbr-white"> '.LANG_YOU_LOGGED.': <b><i>'.$_SESSION['log'].'  </b></i></div><form action="logout.php" method="post">
					<input type="submit" class="btn btn-primary" value="'.LANG_LOGOUT.'" name="log_in" />
					</form>';
				include 'lang.php';
				echo '</div>';
			}
			?>
        </div>
    </nav>
</section>
