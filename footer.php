<section class="cid-qTkAaeaxX5 footer" id="footer1-2">
    <div class="container">
        <div class="media-container-row content text-white">
            <div class="col-12 col-md-3">
                <div class="media-wrap">
                    <a href="http://eriskip.com/">
                        <img src="assets/images/-192x45.png" alt="Eris" class="logo-footer" title="" style="height: 3.5rem;">
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    <?=LANG_ADDR?></h5>
                <p class="mbr-text">
                    617762, <?=LANG_ADDR_STATE?>,
					<br><?=LANG_ADDR_CITY?>
                    <br><?=LANG_ADDR_STREET?>, 8/25
                </p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    <?=LANG_CONTACTS?>
                </h5>
                <p class="mbr-text">
                    Email: <a href="mailto:info@eriskip.ru">info@eriskip.ru</a>
                    <br><?=LANG_PHONE?>: <a href="tel:+73424165511">+7 (34241) 6-55-11</a>
                </p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    <?=LANG_LINKS?></h5>
                <p class="mbr-text">
                    <a class="text-primary" href="http://eriskip.com/"><?=LANG_SITE_COMPANY?></a>
                    <br><a class="text-primary" href="http://eriskip.com/ru/about-us/contacts"><?=LANG_FEEDBACK?></a>
					<?php if(isset($_SESSION['user'])): ?>
					<br><a class="text-primary" href="/help.php"><?=LANG_HELP?></a>
					<?php endif ?>
                </p>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        2019 © <?=LANG_COMPANY?> "<?=LANG_ERIS?>"</p>
                </div>
                <div class="col-md-6">
                    <div class="social-list align-right">

                        <div class="soc-item">
                            <a href="https://www.youtube.com/channel/UCKFHB135fBpA1AWOrn5CmUQ" target="_blank">
                                <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
