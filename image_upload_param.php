<?php
if($_POST['img_width']) 
	$img_width = $_POST['img_width'];
else
	$img_width = '';

if($_POST['img_height']) 
	$img_height = $_POST['img_height'];
else
	$img_height = '';

if($_POST['img_lat1']) 
	$lat1 = $_POST['img_lat1'];
else
	$lat1 = '';

if($_POST['img_lat2']) 
	$lat2 = $_POST['img_lat2'];
else
	$lat2 = '';

if($_POST['img_lng1']) 
	$lng1 = $_POST['img_lng1'];
else
	$lng1 = '';

if($_POST['img_lng2']) 
	$lng2 = $_POST['img_lng2'];
else
	$lng2 = '';

if($_POST['lat_dist']) 
	$lat_dist = $_POST['lat_dist'];
else
	$lat_dist = '';

if($_POST['lng_dist']) 
	$lng_dist = $_POST['lng_dist'];
else
	$lng_dist = '';

$json_data = '{
	"imgWidth":"'.$img_width.'",
	"imgHeight":"'.$img_height.'",
	"lat1":"'.$lat1.'",
	"lat2":"'.$lat2.'",
	"lng1":"'.$lng1.'",
	"lng2":"'.$lng2.'",
	"latDist":"'.$lat_dist.'",
	"lngDist":"'.$lng_dist.'"
}';
	
	file_put_contents('uploads/data_image_param.json', $json_data);
?>