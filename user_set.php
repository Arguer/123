<?php
	session_start();
/*
 * Страница добавления/редактирования аккаунта
 * © Эрис
*/
	include "localization.php";
	include "connection.php";
	require_once "class/head.class.php";
	$head = new HeadPage(); //класс HEAD страницы

	if (isset($_SESSION['user'])){
		$id_user = $_SESSION['user'];
	}

	$table = $mysqli->query("SELECT * FROM admin_role WHERE id_admin = ".$id_user."");

	while($val = $table->fetch_assoc()){
		$id_role = $val['id_role'];
	}

	if (!isset($_SESSION['user']) || $id_role != 1)
	{
	  header('Location: /login.php');
	  exit;
	}

?>
<!DOCTYPE>
<html>
<head>
	<?php echo $head->getHead(); ?>
</head>
<body>
<?php include "headpanel.php"; ?>
<?php

	$edit_user = intval($_GET['user']);

	if($edit_user){

		$res = $mysqli->query("SELECT a.*, c.id_company, c.name, ar.id_admin, ar.id_role FROM admins a INNER JOIN company c INNER JOIN admin_role ar ON a.id_company = c.id_company AND a.id_admin = ar.id_admin WHERE a.id_admin = ".$edit_user."", MYSQLI_USE_RESULT);
		while ($value = $res->fetch_assoc())
		{

				$flogin = $value['login'];
				$femail = $value['email'];
				$fpass = $value['password'];
				$fname = $value['name'];
				$fid_company = $value['id_company'];
				$fid_role = $value['id_role'];

		}

		//$res_role = $mysqli->query

	}

	$comp_res = $mysqli->query("SELECT * FROM company", MYSQLI_USE_RESULT);

?>
	<section class="mbr-section form1 cid-r3l5xi9p39 mbr-parallax-background" id="form1-10">
    <div class="container">
		<div class="row justify-content-center">
			<div class="title col-12 col-lg-8">
				<br>
				<h2 class="mbr-section-title align-center mbr-fonts-style display-2">
				<?php echo $edit_user ? LANG_USER_CHANGE : LANG_USER_ADD; ?>
				</h2>
			</div>
		</div>
		<div class="mbr-block-title"></div>
		<form class="mbr-form company-form" id="user_form" action="users.php" method="post" autocomplete="off">
			<div class="row row-sm-offset">
				<div class="col-md-4 multi-horizontal" data-for="login">
					<div class="form-group">
						<label class="form-control-label mbr-fonts-style display-7"><?=LANG_USER_LOGIN?></label>
						<input type="text" class="form-control" name="login" maxlength="150" id="ulogin" value="<?php echo $flogin; ?>">
					</div>
				</div>
			</div>
			<div class="row row-sm-offset">
				<div class="col-md-4 multi-horizontal" data-for="uemail">
					<div class="form-group">
						<label class="form-control-label mbr-fonts-style display-7"><?=LANG_USER_EMAIL?></label>
						<input type="email" class="form-control" name="email" maxlength="150" id="uemail" value="<?php echo $femail; ?>">
					</div>
				</div>
			</div>
			<div class="row row-sm-offset">
				<div class="col-md-4 multi-horizontal" data-for="password">
					<div class="form-group">
						<label class="form-control-label mbr-fonts-style display-7"><?=LANG_PASS?></label>
						<input type="password" class="form-control" name="password"  id="upass"  value="<?php //echo $fpass; ?>">
					</div>
				</div>
			</div>
			<div class="row row-sm-offset">
				<div class="col-md-4 multi-horizontal" data-for="password">
					<div class="form-group">
						<label class="form-control-label mbr-fonts-style display-7"><?=LANG_PASS_CONF?></label>
						<input type="password" class="form-control" name="password2"  id="upass2"  value="<?php //echo $fpass; ?>">
					</div>
				</div>
			</div>
			<div class="form-group" data-for="company">
				<div class="dropdown dropdown-company">
				  <button class="btn dropdown-toggle" type="button" id="dropdown_company" name="dropdown_company" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="">
					<?php echo $fname ? $fname : LANG_COMPANY.'...'; ?>
				  </button>
				  <div class="dropdown-menu" aria-labelledby="dropdown_company">
					<?php while($value = $comp_res->fetch_assoc()): ?>
						<a class="dropdown-item" data-id="<?php echo $value['id_company']; ?>" data-value="<?php echo $value['name']; ?>" href="#"><?php echo $value['name']; ?></a>
					<?php endwhile; ?>
				  </div>
				  <input type="hidden" name="company_user" class="inp-dropdown" value="<?php echo $fid_company; ?>">
				</div>
			</div>
			<div class="form-group" data-for="user_role">
				<div class="dropdown dropdown-user_role">
				  <button class="btn dropdown-toggle" type="button" id="dropdown_user_role" name="dropdown_user_role" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="">
					  	<?php 
						  if($fid_role) {
							if($fid_role==2)
								echo LANG_ADMIN;
							elseif($fid_role==3)
								echo LANG_USER;
						  }else{
							echo LANG_ROLE.'...';
						  }
						?>
				  </button>
				  <div class="dropdown-menu" aria-labelledby="dropdown_user_role">
						<a class="dropdown-item" data-id="2" data-value="<?=LANG_ADMIN?>" href="#"><?=LANG_ADMIN?></a>
						<a class="dropdown-item" data-id="3" data-value="<?=LANG_USER?>" href="#"><?=LANG_USER?></a>
				  </div>
				  <input type="hidden" name="user_role" class="inp-dropdown" value="<?php echo $fid_role; ?>">
				</div>
				<div class="help-title" title="<?=LANG_HELP_TITLE?>">?</div>
			</div>
			<span class="input-group-btn">
				<button type="submit" class="btn btn-primary btn-form display-4"><?php echo $edit_user ? LANG_SAVE_CHANGE : LANG_CREATE; ?></button>
				<?php if($edit_user): ?><?php if($edit_user != $id_user): ?><button type="button" id="btn_del_user" class="btn btn-md btn-secondary btn-form display-4" data-mess="<?=LANG_DEL_USER_CONF?>"><?=LANG_DELETE?></button><?php endif ?><?php endif ?>
				<button type="button" name="cancel" id="btn_cancel_user" class="btn btn-warning btn-form display-4"><?=LANG_CANCEL?></button>
			</span>
			<input type="hidden" name="uid" id="uid" value="<?php echo $edit_user; ?>">
			<?php if($edit_user): ?>
			<input type="hidden" name="key" id="ckey" value="321">
			<?php else: ?>
			<input type="hidden" name="key" id="ckey" value="132">
			<?php endif ?>
		</form>
	</div>
	</section>
	<?php include "footer.php";?>
	<?php include "scripts.php"; ?>
	<script src="js/post_new_user.js"></script>
</body>
</html>
