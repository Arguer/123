<?php
session_start();
/*
 * Страница подразделений
 * © Эрис
*/
include "localization.php";
require_once "class/head.class.php";
$head = new HeadPage(); //класс HEAD страницы
?>
<html>
	<head>
		<?php echo $head->getHead(LANG_OBJECTS, LANG_OBJECTS_CONTENT); ?>
	</head>
	<body>
	<?php include "headpanel.php";
		if (!isset($_SESSION['user'])){
		  header('Location: /login.php');
		  exit;
		}

		include "connection.php";
	
		$edit_place = intval($_GET['edt_place']);

		$id_user = $_SESSION['user'];
		
		$res_role = $mysqli->query("SELECT * FROM admin_role WHERE id_admin = ".$id_user , MYSQLI_USE_RESULT);
		
		while ($val_role = $res_role->fetch_assoc()){
			$admin_role = $val_role['id_role'];
		}
	?>
	<section class="section-table cid-r13PedKtlK" id="table1-4">
		<div class="tab-pad80">
		<div class="container container-table">
			<h2 class="mbr-section-title mbr-fonts-style align-center pb-3 display-2"><?=LANG_OBJECTS_CONTENT?></h2>
			<div class="container scroll">
			<table class="table isSearch table-hover table-object" cellspacing="0">
			<thead>
			  <tr class="table-heads ">
				  <th class="head-item head-item-checkbox mbr-fonts-style display-7">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input checkbox-inp" id="check_all">
						<label class="custom-control-label" for="check_all"></label>
					</div>
					<span title-checked="<?=LANG_UNSELECT_ALL?>" title-none="<?=LANG_SELECT_ALL?>" class="title-all-checkbox"></span>
				  </th>
				  <th class="head-item mbr-fonts-style display-7"><?=LANG_NAME_OBJ?></th>
				  <th class="head-item mbr-fonts-style display-7"><?=LANG_LOGIN?></th>
				  <th class="head-item mbr-fonts-style display-7"><?=LANG_DESCRIPTION?></th>
				  <?php if($admin_role == 1 || $admin_role == 2): ?><th class="head-item mbr-fonts-style display-7"><?=LANG_CHANGE?></th><?php endif ?>
			  </tr>
			</thead>
			<tbody>
	<?php
	
	if($admin_role == 1){
		$res = $mysqli->query("SELECT * FROM places" , MYSQLI_USE_RESULT);
	}elseif($admin_role == 2){
		$res = $mysqli->query("SELECT * FROM admin_places INNER JOIN places ON admin_places.id_place = places.id_place WHERE admin_places.id_admin = ".$id_user , MYSQLI_USE_RESULT);
	}elseif($admin_role == 3){
		$res = $mysqli->query("SELECT * FROM admins INNER JOIN places ON admins.id_company = places.id_company WHERE admins.id_admin = ".$id_user , MYSQLI_USE_RESULT);
	}
	
	while ($value = $res->fetch_assoc())
	{
		echo '<tr>
				<td class="body-item item-checkbox mbr-fonts-style display-7">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input checkbox-obj checkbox-inp" name="check" id="check_'.$value['id_place'].'" value="'.$value['id_place'].'">
						<label class="custom-control-label" for="check_'.$value['id_place'].'"></label>
					</div>
				</td>
				<td class="body-item item-name mbr-fonts-style display-7">
					<span class="td-title-xs">'.LANG_NAME_OBJ.':</span>
					<a href="/panel.php?id='.$value['id_place'].'">'.$value['name'].'</a>
				</td>
				<td class="body-item item-login mbr-fonts-style display-7"><span class="td-title-xs">'.LANG_LOGIN.':</span>'.$value['login'].'</td> ';
		echo '  <td class="body-item item-description mbr-fonts-style display-7"><span class="td-title-xs">'.LANG_DESCRIPTION.':</span>'.$value['description'].'</td>';
		if($admin_role == 1 || $admin_role == 2) echo '<td class="body-item item-edit mbr-fonts-style display-7"><a href="/add_object.php?redir='.$value['id_place'].'" class="edit-link">'.LANG_EDIT.'</td>';
		echo ' </tr>';
		if ($value['id_place'] == $edit_place)
		{
			$fname = $value['name'];
			$flogin = $value['login'];
			$fpassword = $value['password'];
			$fdescript = $value['description'];
		}
	}
		
	echo '</tbody></table>';
   
	?>
			</div>
			<div class="panel-view-btn input-group-btn">
				<a href="#" class="panel-view btn btn-primary btn-form display-4" style="display:none;"><?=LANG_DETAIL_VIEW?></a>
				<?php if($admin_role == 1 || $admin_role == 2): ?><a href="/add_object.php" class="add-object btn btn-primary btn-form display-4">[+] <?=LANG_ADD_OBJ?></a><?php endif ?>
			</div>
		</div>
		</div>		
	</section>
	<?php include "footer.php";?>
	<?php include "scripts.php"; ?>
	<script src="js/post_new_places.js"></script>
	</body>
</html>

